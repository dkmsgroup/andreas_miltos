package eu.coherentpaas.mongodb.wrapper.test;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;

/**
 *
 * @author idezol
 */
public class Runner {
    
    public static void main(String [] args) throws UnknownHostException, TransactionManagerException, IOException, CQEException, InterruptedException, DataStoreException {
        //In2Test.initDB();
        In2Test test = new In2Test();
        test.test();
        //In2Test.dropDb();
    }
}
