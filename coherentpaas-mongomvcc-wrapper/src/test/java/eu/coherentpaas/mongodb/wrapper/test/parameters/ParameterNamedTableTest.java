package eu.coherentpaas.mongodb.wrapper.test.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBTransformationQuery;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ParameterNamedTableTest {
    private static final Logger Log = LoggerFactory.getLogger(ParameterNamedTableTest.class);
    
    public static void main(String [] args)  {
        try {
            ParameterNamedTableTest.initDB();
            ParameterNamedTableTest test = new ParameterNamedTableTest();
            test.test();
            ParameterNamedTableTest.dropDb();
        } catch(Exception ex) {
            Log.error("{}: {}", ex.getClass().getName(), ex.getMessage());
        }
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(App.TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("revs_cities", null);
            Random generator = new Random();
            List<DBObject> list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                        .append("rec_id", generator.nextInt(1000));
                if(i<70)
                    obj.append("city", "athens");
                else
                    obj.append("city", "berlin");
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            Log.info("Records to nested table added.");
            Log.info("Now will add records to master table");
            
            database.startTransaction();
            collection = database.createCollection("publications", null);
            list.clear();
            for(int i=0; i<100; i++) {
                BasicDBObject obj = new BasicDBObject("key_id", generator.nextInt(1000))
                        .append("pub_id", (i+1))
                        .append("name", "name" + (i+1))
                        .append("title", "title" + (i+1));
                list.add(obj);
            }
            collection.insert(list);
            database.commit();
            
            
            database.startTransaction();
            database.commit();
            
            Log.info("Records added to both databases");
        }
    }
    
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test 
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStreamNested = ClassLoader.getSystemResourceAsStream("Nested_Nested.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlanNested = mapper.readValue(inStreamNested, QueryPlan.class);
        
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlanNested);
        
        MongoCQEDatastore ddsNested = new MongoCQEDatastore();        
        ddsNested.start(properties);
        Connection connectionNested = ddsNested.getConnection(); 
        Statement statementNested = connectionNested.createStatement();
        statementNested.prepare(queryPlanNested);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("Nested_TopInNamedTable.json");
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.useNamedTable("t0", statementNested);
        statement.setString("cityname", "athens");
      
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();       
        
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        Assert.assertEquals(70, result.length);
        
        LTMClient.getInstance().getConnection().commit();
        
        statementNested.close();
        connectionNested.close();
        ddsNested.close();
        statement.close();
        connection.close();
        dds.close();
        
    }
    
    @Test 
    public void test1() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStreamNested = ClassLoader.getSystemResourceAsStream("Nested_Nested.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlanNested = mapper.readValue(inStreamNested, QueryPlan.class);
        
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlanNested);
        
        MongoCQEDatastore ddsNested = new MongoCQEDatastore();        
        ddsNested.start(properties);
        Connection connectionNested = ddsNested.getConnection(); 
        Statement statementNested = connectionNested.createStatement();
        statementNested.prepare(queryPlanNested);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("Nested_TopInNamedTable_ConstParam.json");
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.useNamedTable("t0", statementNested);
        statement.setString("cityname", "athens");
      
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();       
        
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        Assert.assertEquals(70, result.length);
        
        LTMClient.getInstance().getConnection().commit();
        
        statementNested.close();
        connectionNested.close();
        ddsNested.close();
        statement.close();
        connection.close();
        dds.close();
        
    }
}
