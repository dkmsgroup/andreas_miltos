package eu.coherentpaas.mongodb.wrapper.test;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;

public class Bibsearch2Test {
	private static final Logger Log = LoggerFactory
			.getLogger(Bibsearch2Test.class);

	@BeforeClass
	public static void initDB() throws Exception {
            Log.info("Starting minicluster, in case it is not yet started");
            TMMiniCluster.startMiniCluster(true, 0);
            
            Loader.main(new String[] { TestConfiguration.HOSTNAME,
                            Integer.toString(TestConfiguration.PORT),
                            TestConfiguration.DATABASENAME,
                            "src/main/resources/papers.csv", "documents" });
	}

	@AfterClass
	public static void dropDb() throws UnknownHostException,
			TransactionManagerException {
		MongoClient mongoClient = MongoClientFactory.getInstance()
				.createClient(TestConfiguration.HOSTNAME,
						TestConfiguration.PORT);
		Log.info("datastoreID: " + mongoClient.getDataStoreID());
		Log.info("now will drop the database");
		mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
		Log.info("database dropped");
	}

	@Test
	public void test() throws IOException, CQEException, InterruptedException,
			TransactionManagerException, DataStoreException {
		Log.info("Setting properties file");
		Properties properties = new Properties();
		properties.put(ConfigurationParameters.HOSTNAME,
				TestConfiguration.HOSTNAME);
		properties.put(ConfigurationParameters.PORT,
				String.valueOf(TestConfiguration.PORT));
		properties.put(ConfigurationParameters.DATABASENAME,
				TestConfiguration.DATABASENAME);
		properties.put(ConfigurationParameters.SYNCHRONIZE,1);
		Log.info("Properties have been set");

		InputStream inStream = ClassLoader
				.getSystemResourceAsStream("papers.json");
		ObjectMapper mapper = new ObjectMapper();
		QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
		/*
		MongoDBQueryParam query = (new MongoDBInterprenter())
				.getMongoDBQueryParam(queryPlan);
		Log.info(query.toString());*/

		MongoCQEDatastore dds = new MongoCQEDatastore();
                //LTMClient.register(dds);
                dds.start(properties);
                
                LTMClient.getInstance().getConnection().startTransaction();
		Connection connection = dds.getConnection();
		Statement statement = connection.createStatement();

		statement.prepare(queryPlan);
		//TxnCtx txcCtx = new TxnCtx(0, 1);
		TxnCtx txcCtx = LTMClient.getInstance().getConnection().getTxnCtx();
		
		ResultSet resultSet = statement.execute(txcCtx);

		Object[][] result = resultSet.next();
		// printResults(result);
		Assert.assertEquals(5, result.length);
		
		resultSet.close();
                
                //txcCtx.abort();
                LTMClient.getInstance().getConnection().commit();

		Log.info("total records: " + result.length);
	}

}
