package eu.coherentpaas.mongodb.wrapper.test.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.test.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertNamedTableTest {
    private static final Logger Log = LoggerFactory.getLogger(InsertNamedTableTest.class);
    
    public static void main(String [] args) throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, InterruptedException, Exception {
        InsertNamedTableTest test = new InsertNamedTableTest();
        InsertNamedTableTest.initDB();
        test.test();
        InsertNamedTableTest.dropDb();
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("reviwers", null);
            Random generator = new Random();
            List<DBObject> list = new ArrayList<>();
            for(int i=0; i<200; i++) {
                BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                        .append("name", RandomStringUtils.random(5))
                        .append("pub_id", (i+1))
                        .append("random_id", generator.nextInt(1000));
                if(i<70)
                    obj.append("city", "athens");
                else
                    obj.append("city", "berlin");
                
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            database.startTransaction();
            database.commit();
            Log.info("Records to nested table added.");
        }
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStreamNested = ClassLoader.getSystemResourceAsStream("insert_namedtable_nested_query.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlanNested = mapper.readValue(inStreamNested, QueryPlan.class);
        
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlanNested);
        
        MongoCQEDatastore ddsNested = new MongoCQEDatastore();        
        ddsNested.start(properties);
        Connection connectionNested = ddsNested.getConnection(); 
        Statement statementNested = connectionNested.createStatement();
        statementNested.prepare(queryPlanNested);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("insert_namedtable.json");
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.useNamedTable("t1", statementNested);
        statement.setString("x", "yolo");
        statement.setInt("input_id", 70); 
      
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();       
        
        statement.execute(txnCtx);
        
        
        LTMClient.getInstance().getConnection().commitSync();
        statementNested.close();
        connectionNested.close();
        ddsNested.close();
        statement.close();
        connection.close();
        dds.close();
        
        inStream = ClassLoader.getSystemResourceAsStream("insert_namedtable_validator.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        
        Assert.assertEquals(130, result.length);
        
        for(Object [] row : result) {
            String city = (String) row[1];
            Assert.assertEquals("berlin", city);
        }
        
        LTMClient.getInstance().getConnection().commit();
           
        
        
    }
}
