package eu.coherentpaas.mongodb.wrapper.test;

/**
 *
 * @author idezol
 */
public class TestConfiguration {
    
    //public static final String HOSTNAME = "83.212.84.244";
    public static final String HOSTNAME = "127.0.0.1";
    public static final int PORT = 27017;
    public static final String DATABASENAME = "publications";
    public static final String SYNCHRONIZE = "0";
}
