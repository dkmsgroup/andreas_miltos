package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class GroupByTest {
    private static final Logger Log = LoggerFactory.getLogger(CountTest.class);
    private static final String COLUMN_TYPE = "type";
    private static final String COLUMN_QUANTITY = "quantity";
    private static final String COLUMN = "column";
    private static final String COLUMN_VAR = "var";
    
    public static void main(String [] args) {
        try {
            GroupByTest.initDB();
            GroupByTest test = new GroupByTest();
            test.test();
            GroupByTest.dropDb();
        } catch(Exception ex) {
            Log.error("{}: {}", ex.getClass().getName(), ex.getMessage());
        }
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("reviews", null);
            List<DBObject> list = new ArrayList<>();
            BasicDBObject dbObject = new BasicDBObject();
            int count = 0;
            dbObject.append(COLUMN_QUANTITY, 5).append(COLUMN_TYPE, "A").append(COLUMN_VAR, ++count).append(COLUMN, "F");
            list.add(dbObject);
            dbObject = new BasicDBObject();
            dbObject.append(COLUMN_QUANTITY, 10).append(COLUMN_TYPE, "A").append(COLUMN_VAR, ++count).append(COLUMN, "H");
            list.add(dbObject);
            dbObject = new BasicDBObject();
            dbObject.append(COLUMN_QUANTITY, 12).append(COLUMN_TYPE, "B").append(COLUMN_VAR, ++count).append(COLUMN, "F");
            list.add(dbObject);
            dbObject = new BasicDBObject();
            dbObject.append(COLUMN_QUANTITY, 13).append(COLUMN_TYPE, "B").append(COLUMN_VAR, ++count).append(COLUMN, "H");
            list.add(dbObject);
            dbObject = new BasicDBObject();
            dbObject.append(COLUMN_QUANTITY, 14).append(COLUMN_TYPE, "B").append(COLUMN_VAR, ++count).append(COLUMN, "F");
            list.add(dbObject);
            dbObject = new BasicDBObject();
            dbObject.append(COLUMN_QUANTITY, 3).append(COLUMN_TYPE, "V").append(COLUMN_VAR, ++count).append(COLUMN, "H");
            list.add(dbObject);
            
            
            collection.insert(list);
            database.commit();
            
            database.startTransaction();
            database.commit();
            
            Log.info("committed");
        }
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException {
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("groupby.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        LTMClient.getInstance().getConnection().startTransaction();
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        //LTMClient.register(dds);
        dds.start(properties);
        
        Connection connection = dds.getConnection();
        Statement statement = connection.createStatement();
        
        statement.prepare(queryPlan);
        
        TxnCtx txcCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txcCtx);
        
        Object [][] result = resultSet.next();
        printResults(result);
        Assert.assertEquals(3, result.length);
        Assert.assertEquals("V", result[0][0]);
        Assert.assertEquals("B", result[1][0]);
        Assert.assertEquals("A", result[2][0]);
        Assert.assertEquals(3, ((Integer) result[0][1]).intValue());
        Assert.assertEquals(39, ((Integer) result[1][1]).intValue());
        Assert.assertEquals(15, ((Integer) result[2][1]).intValue());
        
        inStream = ClassLoader.getSystemResourceAsStream("groupby_where_clause.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        statement.prepare(queryPlan);
        resultSet = statement.execute(txcCtx);
        result = resultSet.next();
        printResults(result);
        Assert.assertEquals(2, result.length);
        Assert.assertEquals("B", result[0][0]);
        Assert.assertEquals("A", result[1][0]);
        Assert.assertEquals(26, ((Integer) result[0][1]).intValue());
        Assert.assertEquals(5, ((Integer) result[1][1]).intValue());
        
        resultSet.close();
        
        //txcCtx.abort();
        LTMClient.getInstance().getConnection().commit();
        
    }
    
    private static void printResults(Object [][] result) {
        
        for(int i=0; i<result.length; i++) {
            Object[] row = result[i];
            String line = (i+1) + ": ";
            for (Object row1 : row) {
                line += row1.toString() + " ";
            }
            System.out.println(line);
        }
    }
    
}
