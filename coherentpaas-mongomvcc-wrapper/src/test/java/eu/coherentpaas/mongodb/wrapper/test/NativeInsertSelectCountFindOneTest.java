package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class NativeInsertSelectCountFindOneTest {
    private static final Logger Log = LoggerFactory.getLogger(NativeInsertSelectCountFindOneTest.class);
    
    @BeforeClass
    public static void testInsert() throws UnknownHostException, TransactionManagerException, IOException, CQEException, InterruptedException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/insert_multiple.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("city", "athens");
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        statement.execute(txnCtx);
        
        LTMClient.getInstance().getConnection().commitSync();
        
        statement.close();
        connection.close();
        dds.close();
        
        
    }
    
    @Test
    public void testSimple() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/simple_select.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("city", "athens");
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Log.info("Lenght: " + result.length);
        Assert.assertEquals(8, result.length);
        
        statement.close();
        connection.close();
        dds.close();
    }
    
    @Test
    public void testCount() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/count.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("city", "athens");
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Assert.assertEquals(1, result.length);
        Assert.assertEquals(8L, (long) result[0][0]);
        
        statement.close();
        connection.close();
        dds.close();
    }
    
    @Test
    public void testCountParams() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/count_params.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setInt("id", 5);
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Assert.assertEquals(1, result.length);
        Assert.assertEquals(3L, (long) result[0][0]);
        
        statement.close();
        connection.close();
        dds.close();
    }
    
    @Test
    public void testFindOne() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/findone.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setInt("id", 5);
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Assert.assertEquals(1, result.length);
        Assert.assertEquals("name5", (String) result[0][0]);
        
        statement.close();
        connection.close();
        dds.close();
    }
}
