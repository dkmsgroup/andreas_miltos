package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class CountTest {
    private static final Logger Log = LoggerFactory.getLogger(CountTest.class);
    private static final String COLUMN = "column";

    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("reviews", null);
            List<DBObject> list = new ArrayList<>();
            for(int i=0; i<100; i++) {
                BasicDBObject obj = new BasicDBObject("pub_id", (i+1))
                        .append("title", RandomStringUtils.randomAlphanumeric(50))
                        .append("reviewer", "name" + (i+1));
                if(i<10) {
                    obj.append("date", "2013-02-01");
                    obj.append(COLUMN, "F");
                }
                else {
                    obj.append("date", "2014-02-01");
                    obj.append(COLUMN, "D");
                }
                
                Log.info((i+1) + ": inserting: " + obj);
                list.add(obj);
            }
            
            collection.insert(list);
            database.commit();
            
            database.startTransaction();
            database.commit();
            
            Log.info("committed");
        }
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException {
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("count.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        LTMClient.getInstance().getConnection().startTransaction();
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        //LTMClient.register(dds);
        dds.start(properties);
        
        Connection connection = dds.getConnection();
        Statement statement = connection.createStatement();
        
        statement.prepare(queryPlan);
        
        TxnCtx txcCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txcCtx);
        
        Object [][] result = resultSet.next();
        Assert.assertEquals(1, result.length);
        Object obj = result[0][0];
        Long longValue = (Long) obj;
        
        Assert.assertEquals(100L, longValue.longValue());
        Log.info("Total records count: {}", 100);
        
        inStream = ClassLoader.getSystemResourceAsStream("count_where_clause.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        statement.prepare(queryPlan);
        resultSet = statement.execute(txcCtx);
        result = resultSet.next();
        Assert.assertEquals(1, result.length);
        obj = result[0][0];
        longValue = (Long) obj;
        Assert.assertEquals(10L, longValue.longValue());
        Log.info("Total records count with filter: {}", 10);
        
        resultSet.close();
        
        //txcCtx.abort();
        LTMClient.getInstance().getConnection().commit();
        
    }
}
