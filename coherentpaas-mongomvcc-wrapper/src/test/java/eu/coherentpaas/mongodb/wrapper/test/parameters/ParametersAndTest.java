package eu.coherentpaas.mongodb.wrapper.test.parameters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.App;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.Random;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ParametersAndTest {
    private static final Logger Log = LoggerFactory.getLogger(ParametersAndTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        try (DB database = mongoClient.getDB(App.TestConfiguration.DATABASENAME)) {
            database.startTransaction();
            DBCollection collection = database.createCollection("publications", null);
            Random generator = new Random();
            for(int j=0; j<4; j++ ) {
                for(int i=0; i<25; i++) {
                    BasicDBObject obj = new BasicDBObject("pub_id", (((j)*25)+(i+1)))
                            .append("title", "title" + (j+1))
                            .append("name", "name" + (i+1))
                            .append("value", generator.nextInt(100)+1);
                    Log.info((i+1) + ": inserting: " + obj);
                    collection.insert(obj);
                }
            }
            database.commit();
            
            database.startTransaction();
            database.commit();
            
            Log.info("committed");
        }
    }
    
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(App.TestConfiguration.HOSTNAME, App.TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(App.TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    
    @Test
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, App.TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(App.TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, App.TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("parameterized_and.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        
        
        statement.prepare(queryPlan);
        Type [] signature = statement.getSignature();
        int count = 0;
        for(Type type : signature) {
            Log.info("{}: {}", ++count, type.name());
        }
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        
        ResultSet resultSet;
        try {
            statement.execute(txnCtx); 
            Assert.fail("Parameter has not been set and no exception was thrown");
        } catch(CQEException | InterruptedException ex) {
            Log.info("yes: " + ex.getMessage());
        }
        
        statement.setInt("id", 10);
        statement.setString("name", "name1");
        statement.setString("title", "title2");
        resultSet = statement.execute(txnCtx); 
        
        Object [][] result = resultSet.next();
        Assert.assertEquals(1, result.length);
        
        LTMClient.getInstance().getConnection().commit();
        
    }
    
}
