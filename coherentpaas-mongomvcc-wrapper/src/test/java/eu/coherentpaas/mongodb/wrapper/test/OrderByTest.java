package eu.coherentpaas.mongodb.wrapper.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.ConfigurationParameters;
import eu.coherentpaas.mongodb.wrapper.MongoCQEDatastore;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.Random;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class OrderByTest {
    private static final Logger Log = LoggerFactory.getLogger(OrderByTest.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        DB database = mongoClient.getDB(TestConfiguration.DATABASENAME);
        database.startTransaction();
        DBCollection collection = database.createCollection("reviews", null);
        Random generator = new Random();
        for(int j=0; j<4; j++ ) {
            for(int i=0; i<25; i++) {            
                BasicDBObject obj = new BasicDBObject("pub_id", (i+1))
                        .append("title", RandomStringUtils.randomAlphanumeric(50))
                        .append("reviewer", "name" + (i+1))
                        .append("value", generator.nextInt(100)+1);
                if(i<10) 
                    obj.append("date", "2013-02-01");
                else
                    obj.append("date", "2014-02-01");
                Log.info((i+1) + ": inserting: " + obj);
                collection.insert(obj);
            }
        }
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("committed");
        database.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("orderby_expression.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        //LTMClient.register(dds);
        dds.start(properties);
        
        LTMClient.getInstance().getConnection().startTransaction();
        Connection connection = dds.getConnection();
        Statement statement = connection.createStatement();
        
        statement.prepare(queryPlan);
        
        TxnCtx txcCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txcCtx);
        
        Integer curValue = Integer.MAX_VALUE;
        Object [][] result = resultSet.next();
        Assert.assertEquals(36, result.length);
        for(int i=0; i<result.length; i++) {
            Object[] row = result[i];
            String line = (i+1) + ": ";
            for (Object row1 : row) {
                line += row1.toString() + " ";
            }
            
            Integer value = (Integer) row[2];
            if(value>curValue)
                Assert.fail("value: " + value + " is greater than: " + curValue + ". ORDER BY FAILED");
            
            curValue = value;
        }
        
        //Assert.assertEquals(10, result.length);
        resultSet.close();
        
        //txcCtx.abort();
        LTMClient.getInstance().getConnection().commit();
        Log.info("total records: " + result.length);
    }
}
