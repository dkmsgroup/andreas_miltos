package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class InvalidParameterTypeException extends Exception {
    public InvalidParameterTypeException(String message) {
       super(message);
    }
    
}
