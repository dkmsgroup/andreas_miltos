package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;
import java.sql.Timestamp;

/**
 *
 * @author idezol
 */
public class MongoDBParamTimeStamp extends MongoDBParam {
    protected Timestamp value;
    
    public MongoDBParamTimeStamp(String name) {
        super(name, Type.TIMESTAMP);
    }
    
    @Override
    public void setValue(Object value) {
        if(!(value instanceof Timestamp)) 
            throw new IllegalArgumentException("value must be type of " + Type.TIMESTAMP.name());
        else 
            this.value = (Timestamp) value;
    }

    @Override
    public Timestamp getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamTimeStamp{" + super.toString() + " value=" + value + '}';
    }
}
