package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamLong extends MongoDBParam {
    protected Long value;

    public MongoDBParamLong(String name) {
        super(name, Type.LONG);
    }

    @Override
    public Long getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Long)) 
            throw new IllegalArgumentException("value must be type of " + Type.LONG.name());
        else 
            this.value = (Long) value;
    }
    
    @Override
    public String toString() {
        return "MongoDBParamLong{" + super.toString() + " value=" + value + '}';
    } 
    
}
