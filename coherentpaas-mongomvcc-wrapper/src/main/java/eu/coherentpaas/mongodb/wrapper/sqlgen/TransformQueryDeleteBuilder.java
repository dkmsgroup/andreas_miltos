package eu.coherentpaas.mongodb.wrapper.sqlgen;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Delete;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.Project;
import eu.coherentpaas.cqe.plan.Select;
import eu.coherentpaas.cqe.plan.TableRef;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryDeleteFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransformQueryDeleteBuilder {
    private static final Logger Log = LoggerFactory.getLogger(TransformQueryDeleteBuilder.class);
    private String collectionName;
    private NamedTableExpression namedTable = null;
    
    public MongoDBQueryDeleteFactory build(Delete deleteOperation) throws CQEException {       
        if(!((deleteOperation.getOperands()!=null)&&(deleteOperation.getOperands().length==1)))
            throwException("delete operation must be followed by an operation to delete");
                    
        Operation operation = deleteOperation.getOperands()[0];
        if(!(operation instanceof Project))             
            throwException("delete operation must be followed by a valid Project operation to delete");
        
        Project projectOperation = (Project) operation;
        if(!((projectOperation.getOperands()!=null)&&(projectOperation.getOperands().length==1)))
            throwException("project operation must be followed by an operation to get data");
        if(!(projectOperation.getOperands()[0] instanceof Select))
            throwException("project operation must be followed by a valid select operation to get data");
        
        Select selectOperation = (Select) projectOperation.getOperands()[0];
        this.collectionName = ((TableRef) selectOperation.getOperands()[0]).getName();
        Expression expression = selectOperation.getFilter();
        
        MongoDBExpressionVisitor mongoDBExpressionVisitor = new MongoDBExpressionVisitor();
        BasicDBObject queryObject = (BasicDBObject) mongoDBExpressionVisitor.interpret(expression);
        if(!mongoDBExpressionVisitor.getNamedTables().isEmpty())
            this.namedTable = mongoDBExpressionVisitor.getNamedTables().get(0);
        
        
        MongoDBQueryDeleteFactory factory = new MongoDBQueryDeleteFactory(queryObject);
        factory.setCollectionName(collectionName);
        if(namedTable!=null)
            factory.addNamedTable(namedTable);
        
        
        return factory;
    }
    
    private void throwException(String message) throws CQEException {
        MalformedQueryException ex = new MalformedQueryException(message);
        throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
    }
}
