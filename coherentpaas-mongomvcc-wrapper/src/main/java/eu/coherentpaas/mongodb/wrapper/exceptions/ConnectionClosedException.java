package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class ConnectionClosedException extends Exception {
    public ConnectionClosedException(String message) {
        super(message);
    }
}
