package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DBWrapper;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.cqe.datastore.Parameterized;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.utils.WrapperSupportedMethod;
import eu.coherentpaas.mongodb.wrapper.events.StatementExecutionStartedEvent;
import eu.coherentpaas.mongodb.wrapper.events.StatementExecutionStartedListener;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidParameterTypeException;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.mongodb.wrapper.exceptions.StatementClosedException;
import eu.coherentpaas.mongodb.wrapper.exceptions.StatementExpiredException;
import eu.coherentpaas.mongodb.wrapper.exceptions.StatementNotSetException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.AggregationElement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.CountElement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.GroupByElement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBSetDependencies;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBNativeQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryAggregation;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryDelete;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryInsert;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuerySelect;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryUpdate;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBSorterType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.utils.ParsingUtils;
import eu.coherentpaas.mongomvcc.driver.WrapperTransactionContext;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoCQEStatement implements Statement  {
    
    private static final Logger Log = LoggerFactory.getLogger(MongoCQEStatement.class);
    private MongoCQEConnection connection;
    private DBWrapper mongoMVCCDatabase;
    private DBCollection mongoMVCCCollection;
    private MongoDBQuery query;
    //protected ResultSet resultSet;
    private final AtomicBoolean open = new AtomicBoolean(true);
    private volatile AtomicBoolean executing = new AtomicBoolean(false);
    
    
    //listeners for statement execution started event
    private final List<StatementExecutionStartedListener> statementExecutionStartedListeners = new ArrayList<>();
    public synchronized void addStatementExecutionStartedListener(StatementExecutionStartedListener listener) {
        this.statementExecutionStartedListeners.add(listener);
    }
    public synchronized void removeStatementExecutionStartedListener(StatementExecutionStartedListener listener) {
        this.statementExecutionStartedListeners.remove(listener);
    }
    
    public MongoCQEStatement(DBWrapper mongoMVCCDatabase, MongoCQEConnection connection) {
        this.mongoMVCCDatabase = mongoMVCCDatabase;
        this.connection = connection;
    }

    protected boolean isOpen() {
        return this.open.get();
    }
    
    //check if some else is doing something with the queryDBObject or DBCollection
    //one cannnot execute while the other is preparing a query, which will change the parameters of the execution
    //if can execute, then lock the object
    private synchronized boolean canExecute() {
        if(!this.executing.get()) {
            this.executing.set(true);
            return true;
        }
        return false;
    }
    
    private synchronized void releaseExecuteLock() {
        this.executing.set(false);
    }
    
    
    //fires the StatementExecutionStartedEvent, so that the Connection can be informed 
    //and make other open statements to close their result sets
    //*** a connection has multiple statements, but a statement has only one result set associated with it
    private void fireStatementExecutionStarted() {
        for(StatementExecutionStartedListener listener : this.statementExecutionStartedListeners) {
            StatementExecutionStartedEvent event = new StatementExecutionStartedEvent(this);
            listener.statementExecutionStartedDetected(event);
        }
    }
    
    /**
     * Prepares the statement to be executed afterwards.
     * 
     * 
     * JUST A MOCK IMPLEMENTATION YET
     * 
     * @param query a query plan
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void prepare(QueryPlan query) throws CQEException {
        _checkClosed();
        
        try {
            //check if can prepare (maybe someone is executing on the same time, and if yes, then lock
            if(canExecute()) {
                
                
                /*
                this.query = new MongoDBTransformationQuery();
                this.query.setOperationType(MongoOperationType.SELECT);
                this.query.setCollectionName("users");
                this.query.setQueryDbObject(new BasicDBObject());
                this.query.setProjectDbObject(new BasicDBObject());
                 * 
                 */
                
                this.query = (new MongoDBInterprenter()).getMongoDBQueryParam(query);
                
            } else {
                //if object is locked, then wait and retry
                Thread.sleep(100);
                prepare(query);
            }
        } catch (InterruptedException ex) {
            Log.warn("prepare statement was interrupted due to the interruption of a wait-block waiting for another process to finish its concurrent execution of a pre0submitted query");
            Thread.currentThread().interrupt();
        } finally {
            //always release the lock
            releaseExecuteLock();
        }
    }
    
    @Override
    public ResultSet execute(TxnCtx txnctx) throws CQEException, InterruptedException {       
        try {
            //check if can prepare (maybe someone is executing on the same time, and if yes, then lock
            if(!canExecute()) { 
                StatementExpiredException ex = new StatementExpiredException("Prepare statement has changed before the exeution.");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }

            _checkClosed();
            _checkIfQueryIsSet();
            
            //inform other statements to close their result sets
            fireStatementExecutionStarted();

            if(txnctx!=null) {//explicitly set the transaction context, override the one that was initially set upon starting


                //never increment versions through the LTMClient, let the load method of 
                //the Transactional driver to init data
                /*
                if(this.connection.getMongoCQEDatastore().isIncrementVersion()) {
                    try {
                        LTMClient.getInstance().getConnection().commit();
                        LTMClient.getInstance().getConnection().startTransaction();
                        txnctx = LTMClient.getInstance().getConnection().getTxnCtx();
                    } catch(DataStoreException ex) {
                        throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                    }
                }
                */


                WrapperTransactionContext wrapperTxnCtx = new WrapperTransactionContext(txnctx);     
                try {
                    this.mongoMVCCDatabase.startTransaction(wrapperTxnCtx);      
                    txnctx.associate(this.mongoMVCCDatabase.getTransactionalDSClient());
                } catch (CoherentPaaSException ex) {
                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                }
            }
            
            ResultSet resultSet = null;

            if(query instanceof MongoDBNativeQuery) {
                //this.resultSet = executeNativeQuery((MongoDBNativeQuery) query);
                resultSet = executeNativeQuery((MongoDBNativeQuery) query);
            } else if(query instanceof MongoDBQuerySelect) {
                //this.resultSet = executeSelectClause(txnctx, (MongoDBQuerySelect) query);
                resultSet = executeSelectClause(txnctx, (MongoDBQuerySelect) query);
            } else if (query instanceof MongoDBQueryInsert) {
                executeInsertClause(txnctx, (MongoDBQueryInsert) query);
                //this.resultSet = new MongoCQEResultSet(null, false, null, null);
                resultSet =  new MongoCQEResultSet(null, false, null, null);
            } else if (query instanceof MongoDBQueryUpdate) {
                executeUpdateClause(txnctx, (MongoDBQueryUpdate) query);
                //this.resultSet = new MongoCQEResultSet(null, false, null, null);
                resultSet = new MongoCQEResultSet(null, false, null, null);
            } else if (query instanceof MongoDBQueryDelete) {
                executeDeleteClause(txnctx, (MongoDBQueryDelete) query);
                //this.resultSet = new MongoCQEResultSet(null, false, null, null);
                resultSet = new MongoCQEResultSet(null, false, null, null);
            } else if (query instanceof MongoDBQueryAggregation) {
                //this.resultSet = executeAggregationClause(txnctx, (MongoDBQueryAggregation) query);
                resultSet = executeAggregationClause(txnctx, (MongoDBQueryAggregation) query);
                
            } else {
                InvalidQueryOperationException ex = new InvalidQueryOperationException("MongoDBQuery of not known type: " + query.getClass().getName());
                throw ex;          
            }
            return resultSet;
             
        } catch(CQEException | InvalidQueryOperationException ex) {
            Log.error(ex.getClass().getName() + " Exception thrown while executing the command. " + ex.getMessage());
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        } finally {
            //always release the lock
            releaseExecuteLock();
        }
        
    }
    
    
    private MongoCQEResultSet executeInsertClause(TxnCtx txnCtx, MongoDBQueryInsert insertQuery) throws CQEException, InterruptedException {
        BasicDBObject dbObject = null;
        List<DBObject> list = null;
        if((this.query.getParameters().isEmpty())&&(this.query.getNamedTables().isEmpty()))
            dbObject = insertQuery.getInsertDbObject();
        else {
            MongoDBSetDependencies bSetDependencies = new MongoDBSetDependencies(txnCtx, insertQuery.getParameters());
            if(this.query.getNamedTables().isEmpty())
                dbObject = bSetDependencies.setParameters(insertQuery.getInsertDbObject());
            else
                list = bSetDependencies.getListOfObjects(insertQuery.getInsertDbObject(), insertQuery.getNamedTables().get(0));
        }
        
        try {
            Log.debug("will add the following document: {}", dbObject);
            this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(insertQuery.getCollectionName(), null);
            if(dbObject!=null)
                this.mongoMVCCCollection.insert(dbObject);
            else if((list!=null)&&(!list.isEmpty()))
                this.mongoMVCCCollection.insert(list);
        } catch(Exception ex) {
            Log.error("Error executing query. {}: {}", ex.getClass().getName(), ex.getMessage());
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);                        
        }
        return null;
    }
    
    private MongoCQEResultSet executeUpdateClause(TxnCtx txnCtx, MongoDBQueryUpdate updateQuery) throws CQEException, InterruptedException {
        //first assign parameters to the query
        BasicDBObject queryClause;
        BasicDBObject setParametersClause;
        if((this.query.getParameters().isEmpty())&&(this.query.getNamedTables().isEmpty())) {
            queryClause = updateQuery.getUpdateQueryDbObject();
            setParametersClause = updateQuery.getSetParameters();
        }
        else {
            MongoDBSetDependencies bSetDependencies = new MongoDBSetDependencies(txnCtx, this.query.getParameters());
            queryClause = bSetDependencies.setParameters(updateQuery.getUpdateQueryDbObject());
            setParametersClause = bSetDependencies.setParameters(updateQuery.getSetParameters());
        }
        
        try {
            Log.debug("Will update based on the following query: {}", queryClause.toJson());
            Log.debug("Will set values according to the following {}", setParametersClause.toJson());
            this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(updateQuery.getCollectionName(), null);
            this.mongoMVCCCollection.update(queryClause, setParametersClause);
        } catch(Exception ex) {
            Log.error("Error executing query. {}: {}", ex.getClass().getName(), ex.getMessage());
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);                        
        }
        return null;
    }
    
    private MongoCQEResultSet executeDeleteClause(TxnCtx txnCtx, MongoDBQueryDelete deleteQuery) throws CQEException, InterruptedException {
        //first assign parameters to the query
        BasicDBObject queryClause;
        if((this.query.getParameters().isEmpty())&&(this.query.getNamedTables().isEmpty()))
            queryClause = deleteQuery.getDeleteQueryDbObject();
        else {
            MongoDBSetDependencies bSetDependencies = new MongoDBSetDependencies(txnCtx, this.query.getParameters());
            queryClause = bSetDependencies.setParameters(deleteQuery.getDeleteQueryDbObject());
        }
        
        try {
            Log.debug("Will delete based on the following: {}", queryClause.toJson());
            this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(deleteQuery.getCollectionName(), null);
            this.mongoMVCCCollection.remove(queryClause);
        } catch(Exception ex) {
            Log.error("Error executing query. {}: {}", ex.getClass().getName(), ex.getMessage());
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);                        
        }
        return null;
    }
    
    private MongoCQEResultSet executeAggregationClause(TxnCtx txnCtx, MongoDBQueryAggregation aggregationQuery) throws CQEException, InterruptedException {
        //first assign parameters to the query
        BasicDBObject queryClause;
        if((this.query.getParameters().isEmpty())&&(this.query.getNamedTables().isEmpty()))
            queryClause = aggregationQuery.getMatchDbObject();
        else {
            MongoDBSetDependencies bSetDependencies = new MongoDBSetDependencies(txnCtx, this.query.getParameters());
            queryClause = bSetDependencies.setParameters(aggregationQuery.getMatchDbObject());
        }
        
        Log.info("MongoWrapper will execute the aggregation using the following pipeline [match]-[groups]-[project]: {} - {} - {}", queryClause.toJson(), aggregationQuery.getGroupByElements(), aggregationQuery.getProjectDbObject().toJson());
        this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(aggregationQuery.getCollectionName(), null);
        
        //check if aggregation is Count
        if(isCountAggregation(aggregationQuery.getGroupByElements())) {
            long countResult = this.mongoMVCCCollection.count(queryClause);
            return new MongoCQEResultSetLong(countResult);
            
        } else {
            //do normal aggregation
            List<DBObject> pipeline = getAggregationPipeline(queryClause, aggregationQuery);
            AggregationOutput aggregationOutput = this.mongoMVCCCollection.aggregate(pipeline);
            return new MongoCQEResultSetAggregationOutput(aggregationOutput, aggregationQuery.getSignature(), aggregationQuery.getColumnNames());
        }
        
    }
     
    private MongoCQEResultSet executeSelectClause(TxnCtx txnCtx, MongoDBQuerySelect selectQuery) throws CQEException, InterruptedException {
        //first assign parameters to the query
        BasicDBObject queryClause;
        if((this.query.getParameters().isEmpty())&&(this.query.getNamedTables().isEmpty()))
            queryClause = selectQuery.getQueryDbObject();
        else {
            MongoDBSetDependencies bSetDependencies = new MongoDBSetDependencies(txnCtx, this.query.getParameters());
            queryClause = bSetDependencies.setParameters(selectQuery.getQueryDbObject());
        }
        
        Log.debug("MongoWrapper will execute and project the following: selection: {} - projection: {}", queryClause.toJson(), selectQuery.getProjectDbObject().toJson());
        this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(selectQuery.getCollectionName(), null);
        DBCursor mongoCursor = (DBCursor) this.mongoMVCCCollection.find(queryClause, selectQuery.getProjectDbObject());
        if((selectQuery.getMongoDBSorterParam()!=null)&&(!selectQuery.getMongoDBSorterParam().getSorting().isEmpty())) {
            LinkedHashMap<String, MongoDBSorterType> sorting = selectQuery.getMongoDBSorterParam().getSorting();
            Iterator<Entry<String, MongoDBSorterType>> it = sorting.entrySet().iterator();
            BasicDBObject sortElement = new BasicDBObject();
            while(it.hasNext()) {
                Entry<String, MongoDBSorterType> entry = it.next();
                if(entry.getValue()==MongoDBSorterType.ASC)
                    sortElement.append(entry.getKey(), 1);
                if(entry.getValue()==MongoDBSorterType.DESC)
                    sortElement.append(entry.getKey(), -1);

            }
            mongoCursor = (DBCursor) mongoCursor.sort(sortElement);                    
        }

        if(selectQuery.getLimit()!=null)
            mongoCursor = (DBCursor) mongoCursor.limit(selectQuery.getLimit());
        
        return new MongoCQEResultSet(mongoCursor, ((MongoDBQuerySelect) this.query).getIncludeIDInResult(), selectQuery.getSignature(), selectQuery.getColumnNames());
    }
    
    @SuppressWarnings({"BroadCatchBlock", "TooBroadCatch", "UseSpecificCatch"})
    private MongoCQEResultSet executeNativeQuery(MongoDBNativeQuery nativeQuery) throws CQEException, InterruptedException {
        nativeQuery = NativeQueryExecutorHelper.assignParamValues(nativeQuery);
        this.mongoMVCCCollection = (DBCollection) this.mongoMVCCDatabase.createCollection(nativeQuery.getCollectionName(), null);
        boolean methodFound = false;
        MongoCQEResultSet result = null;
        //for(Method method : this.mongoMVCCCollection.getClass().getDeclaredMethods()) {
        for(Method method : MongoCollectionMethods.getInstance().getMethods()) {
            if(method.isAnnotationPresent(WrapperSupportedMethod.class)) {
                WrapperSupportedMethod annotation = method.getAnnotation(WrapperSupportedMethod.class);
                if(annotation.name().equalsIgnoreCase(nativeQuery.getOperationName())) {
                    
                    if(annotation.name().equalsIgnoreCase("insert")) {
                        //for insertions do other logic ... and execute
                        methodFound = true;
                        List<DBObject> list = new LinkedList<>();
                        for(String argument : nativeQuery.getArgs()) 
                            list.add(ParsingUtils.getDBObjectFromString(argument));
                        try {
                            this.mongoMVCCCollection.insert(list);
                        } catch(Exception ex) {
                            Log.error("Error executing insert from native query. {}: {}", ex.getClass().getName(), ex.getMessage());
                            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                        }
                        break;
                        
                    } else {
                        if(Integer.valueOf(annotation.arguments()).intValue()==nativeQuery.getArgs().size()) {
                            //found valid method ... execute
                            methodFound = true;
                            List<DBObject> arguments;
                            
                            if(nativeQuery.getOperationName().equalsIgnoreCase("aggregate")) {
                                //special treatment for aggregations which expects a single argument which is a list
                                if(nativeQuery.getArgs().size()!=1) {
                                    InvalidQueryOperationException ex = new InvalidQueryOperationException("Aggregations must have a single argument which is a list. Operation has " + nativeQuery.getArgs().size() + " arguments");
                                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                                }
                                
                                arguments = ParsingUtils.getListDBObjectFromString(nativeQuery.getArgs().get(0));
                                
                                try {
                                    Object invokeResponse = method.invoke(this.mongoMVCCCollection, arguments);
                                    result = NativeQueryExecutorHelper.createResponse(invokeResponse, annotation.responseType(), nativeQuery.isIncludeIDInResult(), nativeQuery.getSignature(), nativeQuery.getColumnNames());
                                } catch(Exception ex) {
                                    Log.error("Error invoking operation {} from native query. {}: {}", nativeQuery.getOperationName(), ex.getClass().getName(), ex.getMessage());
                                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                                }
                                
                            } else {
                                arguments = new ArrayList<>(nativeQuery.getArgs().size());
                                for(String argument : nativeQuery.getArgs()) 
                                    arguments.add(ParsingUtils.getDBObjectFromString(argument));

                                if(nativeQuery.getOperationName().equalsIgnoreCase("find")) {
                                    if(arguments.size()==2) 
                                        arguments.get(1).put(MongoMVCCFields.DATA_ID, 1);
                                } else if(nativeQuery.getOperationName().equalsIgnoreCase("findOne")) {
                                    if(arguments.size()>1) 
                                        arguments.get(1).put(MongoMVCCFields.DATA_ID, 1);
                                }
                                
                                try {
                                    Object invokeResponse = method.invoke(this.mongoMVCCCollection, arguments.toArray());
                                    result = NativeQueryExecutorHelper.createResponse(invokeResponse, annotation.responseType(), nativeQuery.isIncludeIDInResult(), nativeQuery.getSignature(), nativeQuery.getColumnNames());
                                } catch(Exception ex) {
                                    Log.error("Error invoking operation {} from native query. {}: {}", nativeQuery.getOperationName(), ex.getClass().getName(), ex.getMessage());
                                    throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
                                }
                            }
                            
                        
                            
                            
                            break;
                        }
                    }
                }
            }
                
        }
        
        if(!methodFound) {
            InvalidQueryOperationException ex = new InvalidQueryOperationException("Method " + nativeQuery.getOperationName() + " is not valid or not supported");
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        }
        
        return result;
    }

    
    /**
     * Discard this instance.
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void close() throws CQEException {
        Log.info("Statement is closing");
        synchronized(this) {
            if(!this.open.get())
                return;
            else {
                this.open.set(true);
            }
            this.mongoMVCCCollection = null;
            this.query = null;
            
            /*
            if(this.resultSet!=null) {
                this.resultSet.close();
                this.resultSet = null;
            }
            */
            
            this.connection.removeStatement(this);
            this.mongoMVCCDatabase= null;
            this.connection = null;
        }
    }
    
    
    //maybe this should be removed????
    protected void _close(boolean removeConnectionStatement) throws CQEException  {
   
        Log.info("Statement is closing");
        synchronized(this) {
            if(!this.open.get())
                return;
            else {
                this.open.set(false);
            }
            this.mongoMVCCCollection = null;
            this.query = null;
            
            /*
            if(this.resultSet!=null) {
                this.resultSet.close();
                this.resultSet = null;
            }
            */
            //used internally
            if(removeConnectionStatement)
                this.connection.removeStatement(this);
            this.mongoMVCCDatabase = null;
            this.connection = null;
        }
    }
    
    
    /**
     * Checks if there is a COUNT aggregation operation
     * 
     * @param operands list of aggregation operands of the query
     * @return 
     */
    private boolean isCountAggregation(List<GroupByElement> operands) {
        if(operands==null)
            return false;
        
        for(GroupByElement groupByElement : operands)
            if(groupByElement instanceof CountElement)
                return true;
        
        return false;
    }
    
    /**
     * 
     * Constructs the pipeline to be used from the MongoDB Aggregation framework
     * 
     * @param matchQuery the match query, after interpreted to replace the parameters with specific values
     * @param query the MongoDBQueryAggregation query
     * @return the pipeline
     */
    private List<DBObject> getAggregationPipeline(BasicDBObject matchQuery, MongoDBQueryAggregation query) {
        List<DBObject> pipeline = new LinkedList<>();
        
        //first add the '$match' operation
        if((matchQuery!=null)&&(!matchQuery.isEmpty())) {
            BasicDBObject matchPipeline = new BasicDBObject("$match", matchQuery);
            Log.debug("matchPipeline: {}", matchPipeline.toJson());
            pipeline.add(matchPipeline);
        }
        
        //then add the '$groupBy' operation
        //1. add the "_id" part of the GroupBy Elements
        BasicDBObject idPart = new BasicDBObject();
        for(GroupByElement groupByElement : query.getGroupByElements()) {
            //exclude the aggregation functions 
            if(!(groupByElement instanceof AggregationElement)) {
                idPart.append(groupByElement.getName(), "$" + groupByElement.getReferencedField());
            }
        }
        
        BasicDBObject groupByPipelineValue = new BasicDBObject("_id", idPart);
        
        
        //2. add the aggregationParts
        for(GroupByElement aggregationElement : query.getGroupByElements()) {
            //include only aggregation functions here
            if(aggregationElement instanceof AggregationElement) {
                groupByPipelineValue.append(aggregationElement.getName(), new BasicDBObject("$" + ((AggregationElement) aggregationElement).getFunction(), "$" + aggregationElement.getReferencedField()));
            }
        }
                
        //3. now build the '$groupBy' pipilene
        BasicDBObject groupByPipeline = new BasicDBObject("$group", groupByPipelineValue);
        Log.debug("groupByPipeline: {}", groupByPipeline.toJson());
        pipeline.add(groupByPipeline);
        
        
        //ADD THE SORT
        if((query.getMongoDBSorterParam()!=null)&&(!query.getMongoDBSorterParam().getSorting().isEmpty())) {
            BasicDBObject sorterValue = new BasicDBObject();
            Iterator<Entry<String, MongoDBSorterType>> it = query.getMongoDBSorterParam().getSorting().entrySet().iterator();
            while(it.hasNext()) {
                Entry<String, MongoDBSorterType> entry = it.next();
                if(entry.getValue()==MongoDBSorterType.ASC)
                    sorterValue.append(entry.getKey(), 1);
                else
                    sorterValue.append(entry.getKey(), -1);
            }
            
            BasicDBObject sorterPipeline = new BasicDBObject("$sort", sorterValue);
            Log.debug("sorterPipeline: {}", sorterPipeline.toJson());
            pipeline.add(sorterPipeline);
        }
        
        //AND FINALLY ADD THE PROJECTION
        BasicDBObject projectPipeline = new BasicDBObject("$project", query.getProjectDbObject());
        Log.debug("projectPipeline: {}", projectPipeline.toJson());
        pipeline.add(projectPipeline);
        
        
        return pipeline;
    }

    /**
     * Informs that a named table will be used. This table is looked up
     * in the table store. At this time, data is not still available in
     * the table store. The wrapper will have to ask the result set to
     * know. 
     * 
     * NOT SUPPORTED YET
     * 
     * @param string
     * @param p
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void useNamedTable(String string, Parameterized p) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean isFound = false;
        for(NamedTableExpression namedTable : this.query.getNamedTables()) {
            if(namedTable.getName().equalsIgnoreCase(string)) {
                namedTable.setPreparedStatement(p);
                namedTable.setSignature(p.getSignature());
                isFound = true;
                break;
            } 
        }
        
        if(!isFound)
            Log.warn("Could not find named table for name: {}", string);
    }
    

    

    /**
     * Sets the value of a dynamic parameter. The parameter must have
     * been previously declared with {@link #setOutputType(int, Type)}
     * as {@link Type#INT}.
     * 
     * @param string
     * @param i
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void setInt(String string, int i) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();        
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.INT) {
                    param.setValue(new Integer(i));
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.INT);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }
    
    /**
     * Sets the value of a dynamic parameter. The parameter must have
     * been previously declared with {@link #setOutputType(int, Type)}
     * as {@link Type#STRING}.
     * 
     * 
     * @param string
     * @param string1
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void setString(String string, String string1) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.STRING) {
                    param.setValue(string1);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.STRING);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }
    
    @Override
    public void setDate(String string, Date date) throws CQEException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setLong(String string, long l) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.LONG) {
                    param.setValue(new Long(l));
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.LONG);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setTimestamp(String string, Timestamp tmstmp) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.TIMESTAMP) {
                    param.setValue(tmstmp);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.TIMESTAMP);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setArray(String string, ArrayList<?> al) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.ARRAY) {
                    param.setValue(al);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.ARRAY);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            }  
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setDictionary(String string, HashMap<?, ?> hm) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.DICTIONARY) {
                    param.setValue(hm);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.DICTIONARY);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setFloat(String string, Float f) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.FLOAT) {
                    param.setValue(f);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.FLOAT);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setDouble(String string, Double d) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.DOUBLE) {
                    param.setValue(d);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.DOUBLE);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public void setBinary(String string, Blob blob) throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        boolean parameterFound = false;
        for(MongoDBParam param : this.query.getParameters()) {
            if(param.getName().equalsIgnoreCase(string)) {
                if(param.getDataType()==Type.BINARY) {
                    param.setValue(blob);
                    parameterFound = true;
                    break;
                } else {
                    InvalidParameterTypeException ex = new InvalidParameterTypeException("Parameter " + string + " must be of type " + Type.BINARY);
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
                }
            } 
        }
        
        if(!parameterFound) 
            Log.warn("Parameter " + string + " does not exist in the query");
    }

    @Override
    public Type[] getSignature() throws CQEException {
        _checkClosed();
        _checkIfQueryIsSet();
        
        return this.query.getSignature();
    }
    
    private void _checkIfQueryIsSet() throws CQEException {
        if(this.query==null) {
            StatementNotSetException ex = new StatementNotSetException("Query statement has not been set. It has a null value");
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
    }

    
    private void _checkClosed() throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                Exception ex = new StatementClosedException("Statement is closed");
                throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }
        }
    }

   
    
}
