package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.exceptions.ResultSetClosedException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoCQEResultSetAggregationOutput extends MongoCQEResultSet {
    private final Iterator<DBObject> iterator;
    
    public MongoCQEResultSetAggregationOutput(AggregationOutput aggregationOutput, Type [] signature, String [] columnNames) {
        super(null, true, signature, columnNames);
        Iterable<DBObject> itarable = aggregationOutput.results();
        if(itarable!=null)
            this.iterator = itarable.iterator();
        else
            this.iterator = null;
    }
    
    /**
     * Obtain an arbitrary number of rows according to the configuration
     * If no more rows are included, it closes the result Set
     * 
     * @return null if no more rows available
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Object[][] next() throws CQEException {        
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        if(iterator==null)
            return null;
        
        List<Object[]> rows = new ArrayList<>();
        while((rowsCount<defaultNextRows) && (iterator.hasNext())) {
            DBObject dbObject = iterator.next();
            rows.add(transformRow((BasicDBObject) dbObject));
            rowsCount++;
        }
 
        
        //reset the counter to zero
        rowsCount=0;
        
        return rows.toArray(new Object[rows.size()][]);
    }
    
    /**
     * Obtain at most max number of rows and then closes the resultSet
     * 
     * @param max the maximum number of rows to fetch
     * @return null if no more rows available
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Object[][] next(int max) throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        
        if(iterator==null)
            return null;
        
        List<Object[]> rows = new ArrayList<>();
        while((rowsCount<max) && (iterator.hasNext())) {
            BasicDBObject dbObject = (BasicDBObject) mongoCursor.next();
            rows.add(transformRow(dbObject));
            rowsCount++;
        }
        
        
        return rows.toArray(new Object[rows.size()][]);
    }
    
    /**
     * Transforms a BasicDBObject to a Object []
     * @param dbObject
     * @return 
     */
    @Override
    protected Object[] transformRow(BasicDBObject dbObject) {
        Object [] row = new Object[this.columnNames.length];
        
        
        Object object = dbObject.get("_id");
        BasicDBObject _idObject = null;
        if((object!=null)&&(object instanceof BasicDBObject)) {
            _idObject = (BasicDBObject) object;
        }
        
        for(int i=0; i<this.columnNames.length; i++) {
            Object field = _idObject.get(this.columnNames[i]); //first try into the '_id' subdocument
            if(field==null) //if not there, try on the result (coming from aggregation functionality)
                field = dbObject.get(this.columnNames[i]);
            if(field!=null) {
                row[i] = getValueFromSignature(field, this.signature[i]);
            }
        
        }
        
        return row;
    }
}
