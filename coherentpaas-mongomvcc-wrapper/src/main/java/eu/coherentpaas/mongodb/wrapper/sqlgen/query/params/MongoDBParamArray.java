package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;
import java.util.ArrayList;

/**
 *
 * @author idezol
 */
public class MongoDBParamArray extends MongoDBParam {
    protected ArrayList<?> value;
    
    public MongoDBParamArray(String name) {
        super(name, Type.ARRAY);
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof ArrayList)) 
            throw new IllegalArgumentException("value must be type of " + Type.ARRAY.name());
        else 
            this.value = (ArrayList) value;
    }

    @Override
    public ArrayList getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamArray{" + super.toString() + " value=" + value + '}';
    }
}
