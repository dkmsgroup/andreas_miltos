package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class MalformedQueryException extends Exception {
    public MalformedQueryException(String message) {
        super(message);
    }
}
