package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoDBQueryFactoryUtils {
    private MongoDBQueryFactoryUtils() {}
    
    protected static List<MongoDBParam> addUniqueParam(List<MongoDBParam> parameters, MongoDBParam parameter) {
        if(parameters==null)
            parameters = new LinkedList<>();
        
        Iterator<MongoDBParam> it = parameters.iterator(); 
        while(it.hasNext()) {
            MongoDBParam element = it.next();
            if(element.getName().equalsIgnoreCase(parameter.getName())) {
                it.remove();
                break; //only one element with the same name can exist in the list
            }   
        }
        
        parameters.add(parameter);
        return parameters;
    }
    
    protected static List<NamedTableExpression> addUniqueNamedTableExpr(List<NamedTableExpression> namedTables, NamedTableExpression namedTable) {
        if(namedTables==null)
            namedTables = new LinkedList<>();
        
        Iterator<NamedTableExpression> it = namedTables.iterator();
        while(it.hasNext()) {
            NamedTableExpression element = it.next();
            if(element.getName().equalsIgnoreCase(namedTable.getName())) {
                it.remove();
                break; //only one element with the same name can exist in the list
            }
        }
        
        namedTables.add(namedTable);
        return namedTables;
    }
}
