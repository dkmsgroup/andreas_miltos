package eu.coherentpaas.mongodb.wrapper.events;

/**
 * Listener for the StatementClosedEvent
 * 
 * @author idezol
 */
public interface StatementExecutionStartedListener {
    
    public void statementExecutionStartedDetected(StatementExecutionStartedEvent event);
}
