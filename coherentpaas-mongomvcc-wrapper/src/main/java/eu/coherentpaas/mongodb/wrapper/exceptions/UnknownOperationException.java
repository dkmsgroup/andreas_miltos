package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class UnknownOperationException extends Exception {
    public UnknownOperationException(String message) {
        super(message);
    }
}
