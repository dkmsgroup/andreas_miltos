package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class StatementClosedException extends Exception {    
    public StatementClosedException(String message) {
        super(message);
    }    
}
