package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author idezol
 */
public class MongoDBQueryUpdateFactory implements MongoDBQueryFactory {
    private BasicDBObject updateQueryObject;
    private BasicDBObject setParametersQueryObject;
    private String collectionName;
    private List<MongoDBParam> parameters = new LinkedList<>();
    private List<NamedTableExpression> namedTables = new LinkedList<>();
    private Type[] signature;
    protected String [] projectionColumnNames;
    

    public MongoDBQueryUpdateFactory(BasicDBObject updateQueryObject, BasicDBObject setParametersQueryObject) {
        this.updateQueryObject = updateQueryObject;
        this.setParametersQueryObject = setParametersQueryObject;
    }
    
    @Override
    public MongoDBTransformationQuery createInstance() throws CQEException {
        MongoDBQueryUpdate updateQuery = new MongoDBQueryUpdate(collectionName, MongoOperationType.UPDATE, signature, projectionColumnNames,  parameters, namedTables, updateQueryObject, setParametersQueryObject);
        return updateQuery;
    }

    @Override
    public void setSignature(Type[] signature) {
        this.signature = signature;
    }

    @Override
    public void setProjectionColumnNames(String[] projectionColumnNames) {
        this.projectionColumnNames = projectionColumnNames;
    }
    
    @Override
    public void addParam(MongoDBParam parameter) {
        this.parameters = MongoDBQueryFactoryUtils.addUniqueParam(parameters, parameter);
    }

    @Override
    public void addNamedTable(NamedTableExpression namedTable) {
        this.namedTables.add(namedTable);
    }

    @Override
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public BasicDBObject getUpdateQueryObject() {
        return updateQueryObject;
    }

    public void setUpdateQueryObject(BasicDBObject updateQueryObject) {
        this.updateQueryObject = updateQueryObject;
    }

    public BasicDBObject getSetParametersQueryObject() {
        return setParametersQueryObject;
    }

    public void setSetParametersQueryObject(BasicDBObject setParametersQueryObject) {
        this.setParametersQueryObject = setParametersQueryObject;
    }

    public void setNamedTables(List<NamedTableExpression> namedTables) {
        this.namedTables = namedTables;
    }
    
    
}
