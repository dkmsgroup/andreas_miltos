package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class PortNumberException extends Exception {
    
    public PortNumberException(String message) {
        super(message);
    }
}
