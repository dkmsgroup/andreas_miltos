package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import java.util.LinkedHashMap;

/**
 *
 * @author idezol
 */
public class MongoDBSorterParam {
    
    private final LinkedHashMap<String, MongoDBSorterType> sorting = new LinkedHashMap<>();

    public LinkedHashMap<String, MongoDBSorterType> getSorting() {
        return sorting;
    }
    
    public void addNext(String element, MongoDBSorterType type) {
        this.sorting.put(element, type);
    }
    
   
}
