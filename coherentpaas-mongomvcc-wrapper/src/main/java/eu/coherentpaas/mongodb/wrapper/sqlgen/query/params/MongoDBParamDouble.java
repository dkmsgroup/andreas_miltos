package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamDouble extends MongoDBParam {
    protected Double value;

    public MongoDBParamDouble(String name) {
        super(name, Type.DOUBLE);
    }

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Double)) 
            throw new IllegalArgumentException("value must be type of " + Type.DOUBLE.name());
        else 
            this.value = (Double) value;
    }
    
    
    @Override
    public String toString() {
        return "MongoDBParamDouble{" + super.toString() + " value=" + value + '}';
    }
    
}
