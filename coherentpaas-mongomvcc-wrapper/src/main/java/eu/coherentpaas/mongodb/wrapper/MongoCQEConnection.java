package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.DBWrapper;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.events.DatabaseTransactionCommitEvent;
import eu.coherentpaas.mongodb.events.DatabaseTransactionCommitListener;
import eu.coherentpaas.mongodb.events.DatabaseTransactionRollbackEvent;
import eu.coherentpaas.mongodb.events.DatabaseTransactionRollbackListener;
import eu.coherentpaas.mongodb.wrapper.exceptions.ConnectionClosedException;
import eu.coherentpaas.mongodb.wrapper.events.StatementExecutionStartedEvent;
import eu.coherentpaas.mongodb.wrapper.events.StatementExecutionStartedListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MongoDB's implementation of the CQE.Connection is an extended MongoMVCCDatabase
 * This implementations "wraps around" the MongoMVCCDatabase instance in order to be used by the CQE
 * All the init/finalize actions that need to be done, are already implemented by the MongoMVCCDatabase class
 *
 * @author idezol
 */
public class MongoCQEConnection implements Connection, StatementExecutionStartedListener, DatabaseTransactionRollbackListener, DatabaseTransactionCommitListener {
    private static final Logger Log = LoggerFactory.getLogger(MongoCQEConnection.class);
    private final DBWrapper mongoMVCCDatabase;
    protected final MongoCQEDatastore mongoCQEDatastore;
    private volatile AtomicBoolean open = new AtomicBoolean(true);
    private volatile AtomicBoolean tranasctionOpen = new AtomicBoolean(false);
    
    //holds all currently open statements
    List<MongoCQEStatement> statements = Collections.synchronizedList(new ArrayList<MongoCQEStatement>());
    protected void removeStatement(MongoCQEStatement statement) {
        statements.remove(statement);
    }
    
    
    public MongoCQEConnection(DBWrapper mongoMVCCDatabase, MongoCQEDatastore datastore) throws CQEException {
        //just assigned the MongoMVCCDatabase instance
        this.mongoMVCCDatabase = mongoMVCCDatabase;
        this.mongoCQEDatastore = datastore;
        
        //by default, when a CQE connection is constructed, a transaction context has been already associated
        this.tranasctionOpen.set(true);
    }

    protected MongoCQEDatastore getMongoCQEDatastore() {
        return mongoCQEDatastore;
    }
    
    

    
    public synchronized boolean isOpen() {
        return this.open.get();
    }

    

    
    /**
     * Create a statement. Puts it to its list and start listening so as to be informed if the statement closes
     * 
     * @return a statement object
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Statement createStatement() throws CQEException {
        synchronized(this) {
            //need to check is connection is open
            //syncrhonized block over the 'open' variable
            if(!this.open.get()) {
                ConnectionClosedException ex = new ConnectionClosedException("Connection is closed");
                throw new CQEException(CQEException.Severity.Connection, ex.getMessage(), ex);
            }
        }
        
        MongoCQEStatement statement = new MongoCQEStatement(mongoMVCCDatabase, this);
        statement.addStatementExecutionStartedListener(this);
        this.statements.add(statement);
        return statement;
    }

    
    //Close the CQEConnection and let the MongoDB instance to handle everything and inform
    //the MongoClient which is registered with the TM
    //Also closes all opened statements
    //Application developer has to pre-check that the transaction has already been commited/rollback so for the TM to release the resources
    //and then informs the MongoCQEDatastore to remove the connection from its list
    //
    //NEED TO FIX: BUG EXPLAINED INLINE
    @Override
    public void close() throws CQEException {
        synchronized(this) {
            if(!this.open.get())
                return;
            else {
                this.open.set(false);
            }
        }
        
        
        //close connection's transaction, otherwise, when new connections are opened on the same thread, a Transaction Exception will be thrown by the LTMClient
        //LTMClient keeps a transaction's context still asssociate with the current thread
        /*
        synchronized(this) {
            if(this.tranasctionOpen.get()) {
                try {
                    TxnCnx transactionConnection = LTMClient.getInstance().getConnection();
                    if(transactionConnection!=null) {
                        Log.warn("Closing the connection will also rollback all transaction's context");
                        transactionConnection.rollback();
                    }
                } catch(TransactionManagerException ex) {
                    TransactionClosingException newEx = new TransactionClosingException(ex, "Cannot close connection's current associate transaction");
                    throw new CQEException(CQEException.Severity.Connection, newEx.getMessage(), newEx);
                }                
            }
        }
        */        
        for(MongoCQEStatement statement : this.statements) {
            //AN EXCEPTION WILL BE THROWN IF A STATEMENT IS CONCURRENTLY TRYING TO CLOSE
            //THE for-loop WILL FAIL
            statement._close(false);
        }

        this.mongoCQEDatastore.removeConnection(this);
    }

    
    
    //a statement has started executing, so must inform the other statements to close their result sets
    //THIS IS A REQUIREMENT FOR COHERENTPAAS:
    //Connections may have multiple Statements -- only ONE can execute. When it executes, all ResultSets must close
    @Override
    public void statementExecutionStartedDetected(StatementExecutionStartedEvent event) {
        
        
        for(MongoCQEStatement statement : this.statements) {
            //if(!statement.equals((MongoCQEStatement) event.getSource())) {
            /*
                try {
                    if(statement.resultSet!=null)
                        statement.resultSet.close();
                } catch(CQEException ex) {
                    //not much to do
                } 
             */
            //}
        }
        
    }

    
   

    @Override
    public void databaseTransactionRollbackDetected(DatabaseTransactionRollbackEvent event) {
        try {
            synchronized(this) {
                this.tranasctionOpen.set(false);
            }
            this.close();
        } catch (CQEException ex) {
            Log.error(ex.getClass().getName() + " - " + ex.getMessage());
        }
    }

    @Override
    public void databaseTransactionCommitDetected(DatabaseTransactionCommitEvent event) {
        try {
            synchronized(this) {
                this.tranasctionOpen.set(false);
            }
            this.close();
        } catch (CQEException ex) {
            Log.error(ex.getClass().getName() + " - " + ex.getMessage());
        }
    }

}
