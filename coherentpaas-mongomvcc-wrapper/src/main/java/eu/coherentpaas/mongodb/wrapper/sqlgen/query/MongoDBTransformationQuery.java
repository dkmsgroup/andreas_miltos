package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import javax.annotation.concurrent.Immutable;

/**
 *
 * @author idezol
 */
@Immutable
public abstract class MongoDBTransformationQuery extends MongoDBQuery {    
   
    protected final MongoOperationType operationType;
    
    public MongoDBTransformationQuery(String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables) {
        super(collectionName, signature, columnNames, parameters, namedTables);
        this.operationType = operationType;
    }

    public MongoOperationType getOperationType() {
        return operationType;
    }
    
    
    
}
