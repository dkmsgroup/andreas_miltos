package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;
import java.util.Dictionary;

/**
 *
 * @author idezol
 */
public class MongoDBParamDictionary extends MongoDBParam {
    protected Dictionary value;
    
    public MongoDBParamDictionary(String name) {
        super(name, Type.DICTIONARY);
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Dictionary)) 
            throw new IllegalArgumentException("value must be type of " + Type.DICTIONARY.name());
        else 
            this.value = (Dictionary) value;
    }

    @Override
    public Dictionary getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamDictionary{" + super.toString() + " value=" + value + '}';
    }
}
