package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

/**
 *
 * @author idezol
 */
public enum MongoDBSorterType {
    
    ASC,
    DESC
}
