package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;


/**
 *
 * @author idezol
 */
public class MongoDBParamReference extends MongoDBParam {
    protected MongoDBParam value;
    
    public MongoDBParamReference(String name) {
        super(name, Type.UNKNOWN);
    }

    @Override
    public MongoDBParam getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof MongoDBParam)) 
            throw new IllegalArgumentException("value must be type of " + MongoDBParam.class.getName());
        else 
            this.value = (MongoDBParam) value;
    }

    
    
    @Override
    public String toString() {
        return "MongoDBParamReference{" + super.toString() + " value=" + value + '}';
    }
    
    
}
