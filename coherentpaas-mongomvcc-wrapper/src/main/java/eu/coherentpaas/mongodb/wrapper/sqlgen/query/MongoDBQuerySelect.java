package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import java.util.Objects;
import javax.annotation.concurrent.Immutable;

/**
 *
 * @author idezol
 */
@Immutable
public class MongoDBQuerySelect extends MongoDBTransformationQuery {
    protected final BasicDBObject queryDbObject;
    protected final BasicDBObject projectDbObject;
    protected final MongoDBSorterParam mongoDBSorterParam;
    protected final Integer limit;
    protected final boolean includeIDInResult;


    public MongoDBQuerySelect(BasicDBObject queryDbObject, BasicDBObject projectDbObject, MongoDBSorterParam mongoDBSorterParam, Integer limit, String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables, boolean includeIDInResult) {
        super(collectionName, operationType, signature, columnNames, parameters, namedTables);
        this.queryDbObject = queryDbObject;
        this.projectDbObject = projectDbObject;
        this.mongoDBSorterParam = mongoDBSorterParam;
        this.limit = limit;
        this.includeIDInResult = includeIDInResult;
    }

    public BasicDBObject getQueryDbObject() {
        return queryDbObject;
    }

    public BasicDBObject getProjectDbObject() {
        return projectDbObject;
    }

    public MongoDBSorterParam getMongoDBSorterParam() {
        return mongoDBSorterParam;
    }

    public Integer getLimit() {
        return limit;
    }

    public boolean getIncludeIDInResult() {
        return includeIDInResult;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.queryDbObject);
        hash = 59 * hash + Objects.hashCode(this.projectDbObject);
        hash = 59 * hash + Objects.hashCode(this.mongoDBSorterParam);
        hash = 59 * hash + Objects.hashCode(this.limit);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MongoDBQuerySelect other = (MongoDBQuerySelect) obj;
        if (!Objects.equals(this.queryDbObject, other.queryDbObject)) {
            return false;
        }
        if (!Objects.equals(this.projectDbObject, other.projectDbObject)) {
            return false;
        }
        if (!Objects.equals(this.mongoDBSorterParam, other.mongoDBSorterParam)) {
            return false;
        }
        return Objects.equals(this.limit, other.limit);
    }

    @Override
    public String toString() {
        return "MongoDBQuerySelect{" + "queryDbObject=" + queryDbObject + ", projectDbObject=" + projectDbObject + ", mongoDBSorterParam=" + mongoDBSorterParam + ", limit=" + limit + '}';
    }
    
}
