package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.NativeQueryPlan;
import eu.coherentpaas.cqe.QueryPlan.Parameter;
import eu.coherentpaas.cqe.TransformQueryPlan;
import eu.coherentpaas.cqe.plan.ColRef;
import eu.coherentpaas.cqe.plan.Delete;
import eu.coherentpaas.cqe.plan.Insert;
import eu.coherentpaas.cqe.plan.Limit;
import eu.coherentpaas.cqe.plan.NamedExpr;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.Project;
import eu.coherentpaas.cqe.plan.Update;
import eu.coherentpaas.mongodb.wrapper.exceptions.InvalidQueryOperationException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryDeleteFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryInsertFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuerySelectFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryUpdateFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamArray;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamBinary;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamBoolean;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamDictionary;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamDouble;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamFloat;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamInt;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamLong;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamString;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamTimeStamp;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamUnknown;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class QueryBuilder {
    private static final Logger Log = LoggerFactory.getLogger(QueryBuilder.class);
    
    public static MongoDBQuery build(NativeQueryPlan plan) throws CQEException {
        NativeQueryBuilder builder = new NativeQueryBuilder();
        MongoDBQueryFactory mongoDBQueryFactory = builder.build(plan);
        
        //add signature
        mongoDBQueryFactory.setSignature(plan.getSignature());
        
        //add projection columns
        mongoDBQueryFactory.setProjectionColumnNames(parseNativeProjectionColumns(plan.getColumns()));
        
        //add parameters
        mongoDBQueryFactory = parseParameters(mongoDBQueryFactory, plan.getParameters());
        
        return mongoDBQueryFactory.createInstance();
        
    }
    
    private static String [] parseNativeProjectionColumns(String [][] columns) {
        String [] projectionColumns = new String[columns.length];
        for(int i=0; i<columns.length; i++) {
            projectionColumns[i] = columns[i][0];
        }
        return projectionColumns;
    }
    
    public static MongoDBQuery build(TransformQueryPlan plan) throws CQEException {
        MongoDBQueryFactory mongoDBQueryFactory = null;
        
        Operation operation = ((TransformQueryPlan) plan).getPlan();
        
        
        if((operation instanceof Limit) || (operation instanceof Project))
            mongoDBQueryFactory = createSelectQuery(operation);
        else if (operation instanceof Insert) {
            mongoDBQueryFactory = createInsertQuery(operation);
        } else if(operation instanceof Delete) {
            mongoDBQueryFactory = createDeleteQuery(operation);
        } else if(operation instanceof Update) {
            mongoDBQueryFactory = createUpdateQuery(operation);
        } else {
            InvalidQueryOperationException ex = new InvalidQueryOperationException("Query is not of a known type. It is: " + operation.getClass().getName());
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
        
        if(mongoDBQueryFactory==null) {
            Log.warn("No valid query ... will return null");
            return null;
        }
        
        //add signature
        mongoDBQueryFactory.setSignature(plan.getSignature());
        
        
        //add parameters
        mongoDBQueryFactory = parseParameters(mongoDBQueryFactory, plan.getParameters());
        
        return mongoDBQueryFactory.createInstance();
    }
    
    
    private static MongoDBQuerySelectFactory createSelectQuery(Operation operation) throws CQEException {
        
        TransformQuerySelectBuilder builder = new TransformQuerySelectBuilder();
        MongoDBQuerySelectFactory result = builder.build(operation);
        
        if(operation instanceof Project)
            result.setProjectionColumnNames(getProjectionColumns(((Project) operation).getColumns()));
        else {
            Limit limitOperation = (Limit) operation;
            Operation limitOperand = limitOperation.getOperands()[0];
            result.setProjectionColumnNames(getProjectionColumns(((Project) limitOperand).getColumns()));
        }
        
        return result;
    }
    
    private static String [] getProjectionColumns(NamedExpr[] namedExpressions) {
        String [] columns = new String[namedExpressions.length];
        for(int i=0; i<namedExpressions.length; i++) {
            NamedExpr namedExpr = namedExpressions[i];
            if(namedExpr.getValue() instanceof ColRef) {
                ColRef colRef = (ColRef) namedExpr.getValue();
                columns[i] = colRef.getColref()[0];
            } else {
                columns[i] = namedExpr.getName();
            }
        }
       
        return columns;
    }
    
    private static MongoDBQueryInsertFactory createInsertQuery(Operation operation) throws CQEException {
        TransformQueryInsertBuilder builder = new TransformQueryInsertBuilder();
        MongoDBQueryInsertFactory result = builder.build(operation);
        return result;
    }
    
    private static MongoDBQueryDeleteFactory createDeleteQuery(Operation operation) throws CQEException {
        TransformQueryDeleteBuilder builder = new TransformQueryDeleteBuilder  ();
        MongoDBQueryDeleteFactory result = builder.build((Delete) operation);
        return result;
    }
    
    private static MongoDBQueryFactory createUpdateQuery(Operation operation) throws CQEException {
        TransformQueryUpdateBuilder builder = new TransformQueryUpdateBuilder();
        MongoDBQueryUpdateFactory result = builder.build((Update) operation);
        return result;
    }
    
    private static MongoDBQueryFactory parseParameters(MongoDBQueryFactory factory, Parameter [] parameters) throws CQEException {
        if(parameters==null)
            return factory;
        
        for(Parameter parameter : parameters) {
            switch(parameter.getType()) {
                case ARRAY:
                    factory.addParam(new MongoDBParamArray(parameter.getName()));
                    break;
                case BINARY:
                    factory.addParam(new MongoDBParamBinary(parameter.getName()));
                    break;
                case BOOL:
                    factory.addParam(new MongoDBParamBoolean(parameter.getName()));
                    break;
                case DICTIONARY:
                    factory.addParam(new MongoDBParamDictionary(parameter.getName()));
                    break;
                case DOUBLE:
                    factory.addParam(new MongoDBParamDouble(parameter.getName()));
                    break;
                case FLOAT:
                    factory.addParam(new MongoDBParamFloat(parameter.getName()));
                    break;
                case INT:
                    factory.addParam(new MongoDBParamInt(parameter.getName()));
                    break;
                case LONG:
                    factory.addParam(new MongoDBParamLong(parameter.getName()));
                    break;
                case STRING:
                    factory.addParam(new MongoDBParamString(parameter.getName()));
                    break;
                case TIMESTAMP:
                    factory.addParam(new MongoDBParamTimeStamp(parameter.getName()));
                    break;
                case UNKNOWN:
                    factory.addParam(new MongoDBParamUnknown(parameter.getName()));
                    break;
                default:
                    InvalidQueryOperationException ex = new InvalidQueryOperationException("parameter of not known type: " + parameter.getType());
                    throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            }
        }
        
        return factory;
    }
    
    
    
}
