package eu.coherentpaas.mongodb.wrapper;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.mongodb.wrapper.exceptions.ResultSetClosedException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 *
 * @author idezol
 */
public class MongoCQEResultSetLong extends MongoCQEResultSet {
    private final long value;
    private final AtomicBoolean hasReturedResults;

    protected MongoCQEResultSetLong(long value) {
        super(null, false, null, null);
        this.value = value;
        this.hasReturedResults = new AtomicBoolean(false);
    }
    
    @Override
    public int getRowCount() throws CQEException {
        return 1;
    }
    
    @Override
    public Object[][] next() throws CQEException {
        synchronized(this) {
            if(!this.open.get()) {
                ResultSetClosedException ex = new ResultSetClosedException("Cannot retrieve more data. ResultSet is closed");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
            }
        }
        
      
        //return no results
        if(this.hasReturedResults.get()) {
            List<Object[]> emptyRows = new ArrayList<>();
            return emptyRows.toArray(new Object[emptyRows.size()][]);
        }
        
        List<Object[]> rows = new ArrayList<>();
        Object [] row = new Object[1];
        row[0] = value;
        rows.add(row);
        this.hasReturedResults.set(true);
        return rows.toArray(new Object[rows.size()][]);
    }
    
    @Override
    public Object[][] next(int max) throws CQEException {
        return next();
    }
    
}
