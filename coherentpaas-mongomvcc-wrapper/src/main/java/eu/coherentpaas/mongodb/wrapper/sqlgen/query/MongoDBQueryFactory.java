package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;

/**
 *
 * @author idezol
 */
public interface MongoDBQueryFactory {
    public MongoDBQuery createInstance() throws CQEException;
    public void setSignature(Type [] signature);
    public void setProjectionColumnNames(String [] columns);
    public void addParam(MongoDBParam parameter);
    public void addNamedTable(NamedTableExpression namedTable);
    public void setCollectionName(String collectionName);
}
