package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import java.util.Objects;

/**
 *
 * @author idezol
 */
public class AggregationElement extends GroupByElement{
    private final String function;

    public AggregationElement(String function, String name, String referencedField) {
        super(name, referencedField);
        this.function = function;
    }

    public String getFunction() {
        return function;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.function);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AggregationElement other = (AggregationElement) obj;
        return Objects.equals(this.function, other.function);
    }

    @Override
    public String toString() {
        return "AggregationElement{" + "function=" + function + "} " + super.toString();
    }
    
    
    protected static void checkValidAggregationOperation(String operation) throws CQEException {
        
        if( !
                ((operation.equalsIgnoreCase("avg")) ||
                (operation.equalsIgnoreCase("min")) ||
                (operation.equalsIgnoreCase("max")) ||
                (operation.equalsIgnoreCase("count")) ||
                (operation.equalsIgnoreCase("sum"))) 
          ) {
            MalformedQueryException ex = new MalformedQueryException("Aggregation operation must be one of the [avg, sum, min, max, count]. This is: " + operation);
            CQEException cqeEx = new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
            throw cqeEx;
        }
    }
}
