package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class TransactionClosingException extends Exception{

    public TransactionClosingException(Throwable ex, String message) {
        super(message, ex);
    }
}
