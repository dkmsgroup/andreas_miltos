package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author idezol
 */
public class MongoDBQueryUpdate extends MongoDBTransformationQuery {
    protected final BasicDBObject updateQueryDbObject;
    protected final BasicDBObject setParameters;

    public MongoDBQueryUpdate(String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables, BasicDBObject updateQueryDbObject, BasicDBObject setParametersDbObject) {
        super(collectionName, operationType, signature, columnNames, parameters, namedTables);
        this.updateQueryDbObject = updateQueryDbObject;
        this.setParameters = setParametersDbObject;
    }

    public BasicDBObject getUpdateQueryDbObject() {
        return updateQueryDbObject;
    }

    public BasicDBObject getSetParameters() {
        return setParameters;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.updateQueryDbObject);
        hash = 89 * hash + Objects.hashCode(this.setParameters);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MongoDBQueryUpdate other = (MongoDBQueryUpdate) obj;
        if (!Objects.equals(this.updateQueryDbObject, other.updateQueryDbObject)) {
            return false;
        }
        return Objects.equals(this.setParameters, other.setParameters);
    }

    @Override
    public String toString() {
        return "MongoDBQueryUpdate{" + "updateQueryDbObject=" + updateQueryDbObject + ", setParameters=" + setParameters + '}';
    }
 
    
}
