package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;

/**
 *
 * @author idezol
 */
public abstract class MongoDBQuery {
    protected final String collectionName;
    protected final Type[] signature;
    protected final String [] columnNames;
    protected final List<MongoDBParam> parameters;
    protected final List<NamedTableExpression> namedTables;

    public MongoDBQuery(String collectionName, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables) {
        this.collectionName = collectionName;
        this.signature = signature;
        this.columnNames = columnNames;
        this.parameters = parameters;
        this.namedTables = namedTables;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public Type[] getSignature() {
        return signature;
    }

    public List<MongoDBParam> getParameters() {
        return parameters;
    }

    public List<NamedTableExpression> getNamedTables() {
        return namedTables;
    }

    public String[] getColumnNames() {
        return columnNames;
    }

}
