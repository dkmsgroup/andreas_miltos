package eu.coherentpaas.mongodb.wrapper.sqlgen.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Call;
import eu.coherentpaas.cqe.plan.Const;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.NamedExpr;
import eu.coherentpaas.cqe.plan.Param;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamArray;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamBinary;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamBoolean;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamDictionary;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamDouble;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamFloat;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamInt;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamLong;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamReference;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamString;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamTimeStamp;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamUnknown;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.rowset.serial.SerialBlob;
import org.mortbay.log.Log;

/**
 *
 * @author idezol
 */
public class ParsingUtils {
    
    public static Object getValueByConst(Const expr) throws SQLException {
        Object obj = expr.getValue();
        switch(expr.getDatatype()) {
            case INT:
                return Integer.valueOf(obj.toString());
            case STRING:
                return (String) obj;
            case ARRAY:
                return (ArrayList) obj;
            case BINARY:
                return (Blob) new SerialBlob(((String) obj).getBytes());
            case BOOL:
                return Boolean.valueOf(obj.toString());
            case DICTIONARY:
                return (Dictionary) obj;
            case DOUBLE:
                return Double.valueOf(obj.toString());
            case FLOAT:
                return Float.valueOf(obj.toString());
            case LONG:
                return Long.valueOf(obj.toString());
            case TIMESTAMP:
                Log.warn("Check timestamp const validation");
                return new Timestamp(Long.valueOf(obj.toString()));
            case UNKNOWN:
                return obj;
            default:
                break;
        }
        
        return obj;
    }
    
    public static MongoDBParam interpretParameter(String paramName, Const expr) {
        MongoDBParam param = null;
        switch(expr.getDatatype()) {
            case INT:
                param = new MongoDBParamInt(paramName);
                param.setValue(expr.getValue());
                return param;
            case STRING:
                param = new MongoDBParamString(paramName);
                param.setValue(expr.getValue());
                return param;
            case ARRAY:
                param = new MongoDBParamArray(paramName);
                param.setValue(expr.getValue());
                return param;
            case BINARY:
                param = new MongoDBParamBinary(paramName);
                param.setValue(expr.getValue());
                return param;
            case BOOL:
                param = new MongoDBParamBoolean(paramName);
                param.setValue(expr.getValue());
                return param;
            case DICTIONARY:
                param = new MongoDBParamDictionary(paramName);
                param.setValue(expr.getValue());
                return param;
            case DOUBLE:
                param = new MongoDBParamDouble(paramName);
                param.setValue(expr.getValue());
                return param;
            case FLOAT:
                param = new MongoDBParamFloat(paramName);
                param.setValue(expr.getValue());
                return param;
            case LONG:
                param = new MongoDBParamLong(paramName);
                param.setValue(expr.getValue());
                return param;
            case TIMESTAMP:
                Log.warn("Check timestamp const validation");
                param = new MongoDBParamTimeStamp(paramName);
                param.setValue(expr.getValue());
                return param;
            case UNKNOWN:
                param = new MongoDBParamUnknown(paramName);
                param.setValue(expr.getValue());
                return param;
            default:
                break;
        }
        
        return param;
    }
    
    public static NamedTableExpression parseNamedTable(Call call) {
        NamedTableExpression namedTableExpression = new NamedTableExpression(call.getSub());
        if(call.getParams()!=null) {
            for(NamedExpr namedExpr : call.getParams()) {
                String paramName = namedExpr.getName();
                Expression paramValue = namedExpr.getValue();
                if(paramValue instanceof Const) {
                    namedTableExpression.addParam(interpretParameter(paramName, (Const) paramValue));
                }
                if(paramValue instanceof Param) {
                    MongoDBParamReference paramReference = new MongoDBParamReference(paramName);
                    paramReference.setValue(new MongoDBParamUnknown(((Param) paramValue).getName()));
                    namedTableExpression.addParam(paramReference);
                }
            }
        }
        return namedTableExpression;
    }
    
    public static BasicDBObject getDBObjectFromString(String str) throws CQEException {
        BasicDBObject dbObject;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String, Object> map = objectMapper.readValue(str, HashMap.class);
            dbObject = new BasicDBObject(map);
        } catch(IOException ex) {
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
        return dbObject;
    }
    
    public static List<DBObject> getListDBObjectFromString(String str) throws CQEException {
        List<DBObject> list;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            list = objectMapper.readValue(str, new TypeReference<List<BasicDBObject>>(){});
        } catch(IOException ex) {
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
        return list;
    }
}
