package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamString extends MongoDBParam {
    protected String value;

    public MongoDBParamString(String name) {
        super(name, Type.STRING);
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof String)) 
            throw new IllegalArgumentException("value must be type of " + Type.STRING.name());
        else 
            this.value = (String) value;
    }

    @Override
    public String getValue() {
        return value;
    }

    
    @Override
    public String toString() {
        return "MongoDBParamString{" + super.toString() + " value=" + value + '}';
    }

}
