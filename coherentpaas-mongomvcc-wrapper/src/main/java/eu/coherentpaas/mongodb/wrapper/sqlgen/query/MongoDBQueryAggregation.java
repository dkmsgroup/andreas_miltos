package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.GroupByElement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author idezol
 */
public class MongoDBQueryAggregation extends MongoDBTransformationQuery {
    protected final BasicDBObject matchDbObject;
    protected final BasicDBObject projectDbObject;
    protected final MongoDBSorterParam mongoDBSorterParam;
    protected final List<GroupByElement> groupByElements;
    
    public MongoDBQueryAggregation(List<GroupByElement> groupByElements, BasicDBObject matchDbObject, BasicDBObject projectDbObject, MongoDBSorterParam mongoDBSorterParam, String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables) {
        super(collectionName, operationType, signature, columnNames, parameters, namedTables);
        this.matchDbObject = matchDbObject;
        this.projectDbObject = projectDbObject;
        this.mongoDBSorterParam = mongoDBSorterParam;
        this.groupByElements = groupByElements;
    }

    public List<GroupByElement> getGroupByElements() {
        return groupByElements;
    }
    
    public BasicDBObject getMatchDbObject() {
        return matchDbObject;
    }

    public BasicDBObject getProjectDbObject() {
        return projectDbObject;
    }

    public MongoDBSorterParam getMongoDBSorterParam() {
        return mongoDBSorterParam;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.matchDbObject);
        hash = 67 * hash + Objects.hashCode(this.projectDbObject);
        hash = 67 * hash + Objects.hashCode(this.mongoDBSorterParam);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MongoDBQueryAggregation other = (MongoDBQueryAggregation) obj;
        if (!Objects.equals(this.matchDbObject, other.matchDbObject)) {
            return false;
        }
        if (!Objects.equals(this.projectDbObject, other.projectDbObject)) {
            return false;
        }
        return Objects.equals(this.mongoDBSorterParam, other.mongoDBSorterParam);
    }

    @Override
    public String toString() {
        return "MongoDBQueryAggregation{" + "matchDbObject=" + matchDbObject + ", projectDbObject=" + projectDbObject + ", mongoDBSorterParam=" + mongoDBSorterParam + '}';
    }
    
}
