package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class ResultSetClosedException extends Exception {
    public ResultSetClosedException(String message) {
        super(message);
    }    
}
