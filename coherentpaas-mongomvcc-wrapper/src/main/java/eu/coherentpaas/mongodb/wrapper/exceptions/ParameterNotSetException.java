package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class ParameterNotSetException extends Exception {
    public ParameterNotSetException(String message) {
        super(message);
    }
    
}
