package eu.coherentpaas.mongodb.wrapper.exceptions;

/**
 *
 * @author idezol
 */
public class MongoDBQueryMalformedException extends Exception {
    public MongoDBQueryMalformedException(String message) {
        super(message);
    }
    
}
