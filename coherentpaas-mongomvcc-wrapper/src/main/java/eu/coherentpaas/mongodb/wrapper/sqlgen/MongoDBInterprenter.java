package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.NativeQueryPlan;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.TransformQueryPlan;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class MongoDBInterprenter {
    
    private static final Logger Log = LoggerFactory.getLogger(MongoDBInterprenter.class);

    
    public MongoDBQuery getMongoDBQueryParam(QueryPlan plan) throws CQEException {
        
        if(plan instanceof TransformQueryPlan) {
            return QueryBuilder.build((TransformQueryPlan) plan);
        } 
        else if (plan instanceof NativeQueryPlan) {
            return QueryBuilder.build((NativeQueryPlan) plan);
        }
        else  {
            String mes = "No Transform neither Native query plan. It is: " + plan.getClass().getName();
            MalformedQueryException ex = new MalformedQueryException(mes);
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
            
        
    }
    
    
    
}
