package eu.coherentpaas.mongodb.wrapper;

import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.DBWrapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.DataStore;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.mongodb.wrapper.exceptions.PortNumberException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The MongoDB's implementation of the CQE.DataStore is an extended MongoMVCCDBClient
 * This implementation "wraps around" the MongoMVCCDBClient instance in order to be used by the CQE
 * Everything that need to be done are (initialization and configuration of the MongoMVCCDatabase are implemented
 * by the MongoMVCCDBClient class that is also registered to the local Transactional Manager (TM)
 *
 * @author idezol
 */
public class MongoCQEDatastore implements DataStore {
    private static final Logger Log = LoggerFactory.getLogger(MongoCQEDatastore.class);
    
    
    private static String hostname;
    private static int port = 0;
    private static String databaseName;
    
    private MongoClient mongoMVCCDbClient;
    private boolean incrementVersion = false;
    private final List<MongoCQEConnection> connections = new ArrayList<>();
    protected void removeConnection(MongoCQEConnection connection) {
        connections.remove(connection);
    }
    

    /**
     * Initialize the data store.
     * 
     * @param configuration
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void start(Properties configuration) throws CQEException {
        
        hostname = configuration.getProperty(ConfigurationParameters.HOSTNAME);
        
        //check if port is in valid foramt
        try {
            port = portStringToInt(configuration.getProperty(ConfigurationParameters.PORT));
        } catch (PortNumberException ex) {
            throw new CQEException(CQEException.Severity.DataStore, ex.getMessage(), ex);
        }
        
        databaseName = configuration.getProperty(ConfigurationParameters.DATABASENAME);
        if(configuration.getProperty(ConfigurationParameters.SYNCHRONIZE, "0").equals("1"))
                this.incrementVersion = true;
        
        
        //initiliazes the MongoMVVC Client (that is will be further registered to the TM
        //TODO --NEED TO CHANGE THAT, SINGLETON INSTEAD OF FACTORY pattern, 
        //or a mix of those that will ALWAYS return a singleton
        try {
            Log.info("Initializing MongoTransactional Client");
            
            this.mongoMVCCDbClient = MongoClientFactory.getInstance().createClient(hostname, port);
            //this.mongoMVCCDbClient = new MongoClient(hostname, port);
            
            Log.info("Adding Mongo Client's supported methods to cache");
            MongoCollectionMethods.getInstance();
            
        } catch (TransactionManagerException | UnknownHostException ex) {
            throw new CQEException(CQEException.Severity.DataStore, ex.getMessage(), ex);
        } 
        
        Log.info("MongoCQE Datastore has started.");
    }

    
    /**
     * Stop this data store.
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public void close() throws CQEException {
        MongoClientFactory.getInstance().closeClient();
        this.mongoMVCCDbClient=null;
    }

    
    /**
     * Open a connection to the data store using default authentication.
     * 
     * @return a data store connection
     * @throws eu.coherentpaas.cqe.CQEException
     */
    @Override
    public Connection getConnection() throws CQEException {
        
        //creates a new instance of the MongoMVCCDatabase (the connection) that is being assigned the transaction's context
        //by the mongoCLient automatically
        
        //ANY GIVEN CONNETION MUST BE ASSIGNED TO A TRANSACTION. If it is already assigned, then this will throw an exception.
        //SHOULD HAVE BEEN COMMITTED/ROLLBACK
        //REMEMBER .. IT HAS TO ALSO ASSOCIATE THIS CONNECTION WITH THE DATASTORE --this is done in the MongoCQEConnection constructor        
        DBWrapper mongoDatabase = (DBWrapper) this.mongoMVCCDbClient.getDBWrapper(databaseName);
        /*
        try {
            mongoDatabase.startTransaction();
            
        } catch (TransactionManagerException ex) {
            throw new CQEException(CQEException.Severity.DataStore, ex.getMessage(), ex);
        }
        */
        Connection mongoCQEConnection = new MongoCQEConnection(mongoDatabase, this);
        
        //register the connection to the mongoDatabase, so as to be informed in case of rollbacks
        mongoDatabase.registerDatabaseTransactionRollbackListener((MongoCQEConnection) mongoCQEConnection);
        return mongoCQEConnection; 
    }

    protected boolean isIncrementVersion() {
        return incrementVersion;
    }
    
    

    
    /**
     * Open a connection to the data store using explicit username / password
     * authentication.
     * 
     * @param username
     * @param password
     * @return 
     * @throws eu.coherentpaas.cqe.CQEException
     * @retu
     */
    @Override
    public Connection getConnection(String username, String password) throws CQEException {
        //no authentication is needed right now
        return getConnection();
    }
    
    
    
    //checks for port mal format
    private int portStringToInt(String str) throws PortNumberException {
        
        if(str==null)
            throw new PortNumberException("Port number does not exist");
        
        if(!NumberUtils.isDigits(str))
            throw new PortNumberException("Port number is not in a valid format");
        
        port = NumberUtils.toInt(str);
        
        if(port==0)
            throw new PortNumberException("Port number cannot be zero");
        
        return port;
    }

    @Override
    public JsonNode getMetaData() throws CQEException {
        return null;
    }

    
    /*
    @Override
    public void applyWS() throws DataStoreException {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.applyWS();
        else throw new DataStoreException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public void applyWS(TxnCtx txnctx) {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.applyWS(txnctx);
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public void rollback() {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.rollback();
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public void rollback(TxnCtx txnctx) {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.rollback(txnctx);
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public byte[] getWS() throws DataStoreException {
        if(this.mongoMVCCDbClient!=null)
            return this.mongoMVCCDbClient.getWS();
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public byte[] getWS(TxnCtx txnctx) throws DataStoreException {
        if(this.mongoMVCCDbClient!=null)
            return this.mongoMVCCDbClient.getWS(txnctx);
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public DataStoreId getDataStoreID() {
        return DataStoreId.MONGODB;
    }

    @Override
    public void redoWS(long l, byte[] bytes) throws DataStoreException {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.redoWS(l, bytes);
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }

    @Override
    public void unleash(TxnCtx txnctx) throws DataStoreException {
        if(this.mongoMVCCDbClient!=null)
            this.mongoMVCCDbClient.unleash(txnctx);
        else throw new RuntimeException("MongoCQEDatastore has not been started yet");
    }
    */
}
