package eu.coherentpaas.mongodb.wrapper.sqlgen.query.params;

import eu.coherentpaas.cqe.Type;

/**
 *
 * @author idezol
 */
public class MongoDBParamInt extends MongoDBParam {
    protected Integer value;

    public MongoDBParamInt(String name) {
        super(name, Type.INT);
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public void setValue(Object value) {
        if(!(value instanceof Integer)) 
            throw new IllegalArgumentException("value must be type of " + Type.INT.name());
        else 
            this.value = (Integer) value;
    }
    
    @Override
    public String toString() {
        return "MongoDBParamInt{" + super.toString() + " value=" + value + '}';
    }
}
