package eu.coherentpaas.mongodb.wrapper.sqlgen;

import java.util.Objects;

/**
 *
 * Stores the name of the element to be project and the name of the field that references
 * 
 * @author idezol
 */
public class GroupByElement {
    private final String name;
    private final String referencedField;

    public GroupByElement(String name, String referencedField) {
        this.name = name;
        this.referencedField = referencedField;
    }

    public String getName() {
        return name;
    }

    public String getReferencedField() {
        return referencedField;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.name);
        hash = 13 * hash + Objects.hashCode(this.referencedField);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GroupByElement other = (GroupByElement) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.referencedField, other.referencedField);
    }

    @Override
    public String toString() {
        return "GroupByElement{" + "name=" + name + ", referencedField=" + referencedField + '}';
    }
    
    
}
