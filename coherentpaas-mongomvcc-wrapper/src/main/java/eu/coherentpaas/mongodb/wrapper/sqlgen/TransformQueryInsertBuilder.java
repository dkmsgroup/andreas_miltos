package eu.coherentpaas.mongodb.wrapper.sqlgen;

import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.plan.Call;
import eu.coherentpaas.cqe.plan.Const;
import eu.coherentpaas.cqe.plan.Expression;
import eu.coherentpaas.cqe.plan.Insert;
import eu.coherentpaas.cqe.plan.NamedExpr;
import eu.coherentpaas.cqe.plan.Operation;
import eu.coherentpaas.cqe.plan.Param;
import eu.coherentpaas.cqe.plan.Project;
import eu.coherentpaas.cqe.plan.TableRef;
import eu.coherentpaas.mongodb.wrapper.exceptions.MalformedQueryException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQueryInsertFactory;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamUnknown;
import eu.coherentpaas.mongodb.wrapper.sqlgen.utils.ParsingUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransformQueryInsertBuilder {
    private static final Logger Log = LoggerFactory.getLogger(TransformQueryInsertBuilder.class);
    private String collectionName;
    private ArrayList<String> columnNames;
    private ArrayList<Object> columnData = null;
    private NamedTableExpression namedTable = null;
    
    public MongoDBQueryInsertFactory build(Operation operation) throws CQEException {
        
        Insert insertOperation = (Insert) operation;
        if(insertOperation.getOperands().length!=2) {
            throwValidException("Insert query must have 2 operands. This has " + insertOperation.getOperands().length);
        }

        for(Operation op : insertOperation.getOperands()) {
            if(op instanceof Project) {
                if(((Project)op).getOperands()==null) {
                    //parse the data to be inserted to table
                    parseDataProjection((Project)op);
                } else {                    
                    Operation operationProject = ((Project) op).getOperands()[0];
                    if(operationProject instanceof TableRef)
                        //parse the table to be inserted
                        parseTableProjection((Project)op);
                    else if(operationProject instanceof Call)
                        //data will be inserted by a named expression 
                        this.namedTable = ParsingUtils.parseNamedTable((Call) operationProject);
                }
            } else {
                throwValidException("Insert query's operand must be of type Project. This is " + op.getClass().getName());
            }
        }        
        
        
        MongoDBQueryInsertFactory factory = new MongoDBQueryInsertFactory();
        if(this.columnData!=null) {
            for(int i=0; i<this.columnData.size(); i++) 
                factory.getInsertObject().append(this.columnNames.get(i), this.columnData.get(i));
        }
        else {
            for(int i=0; i<this.columnNames.size(); i++) 
                factory.getInsertObject().append(this.columnNames.get(i), null);
        }
        
        factory.setCollectionName(collectionName);
        if(this.namedTable!=null)
            factory.addNamedTable(namedTable);
        return factory;
    }
    
    
    
    private void parseDataProjection(Project project) throws CQEException {
        this.columnData = new ArrayList<>(project.getColumns().length);
        for(NamedExpr expr : project.getColumns()) {
            Expression expression = expr.getValue();
            if(expression instanceof Const) {
                this.columnData.add(getColumn((Const) expression));
            } else if (expression instanceof Param) {
                this.columnData.add(getParam((Param) expression));
            } else 
                throwValidException("incobatible type of expression. It should be either Const or Param, but it is " + expression.getClass().getName());
        }
    }
    
    
    private Object getColumn(Const column) throws CQEException {
        try {
            return ParsingUtils.getValueByConst(column);
        } catch(SQLException ex) {
            throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
        }
    }
    
    private Object getParam(Param param) {
        return new MongoDBParamUnknown(param.getName());
    }
    
    private void parseTableProjection(Project project) throws CQEException {
        //this is the projection to the table that the data will be inserted
        this.collectionName = ((TableRef) project.getOperands()[0]).getName(); 

        this.columnNames = new ArrayList(project.getColumns().length);
        for(NamedExpr column : project.getColumns()) {
            this.columnNames.add(column.getName());
        }
    }

    private void throwValidException(String message) throws CQEException {
        MalformedQueryException ex = new MalformedQueryException(message);
        throw new CQEException(CQEException.Severity.Statement, ex.getMessage(), ex);
    }
    
    private MongoDBParam interpretParameter(String paramName, Const expr) {
        return ParsingUtils.interpretParameter(paramName, expr);
    }
}
