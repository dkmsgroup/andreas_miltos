package eu.coherentpaas.mongodb.wrapper;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.NativeQueryPlan;
import eu.coherentpaas.cqe.QueryPlan;
import eu.coherentpaas.cqe.ResultSet;
import eu.coherentpaas.cqe.TransformQueryPlan;
import eu.coherentpaas.cqe.datastore.Connection;
import eu.coherentpaas.cqe.datastore.Statement;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBInterprenter;
import eu.coherentpaas.mongodb.wrapper.sqlgen.MongoDBSetDependencies;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBTransformationQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBQuerySelect;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamInt;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParamString;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.LTMServerProxy;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Hello world!
 *
 */
public class App {   
    private static final Logger Log = LoggerFactory.getLogger(App.class);
    
    public static class TestConfiguration {
        public static final String HOSTNAME = "127.0.0.1";
        public static final int PORT = 27017;
        public static final String DATABASENAME = "tpch";
        public static final String SYNCHRONIZE = "0";
    }

    public static void main(String [] args ) throws TransactionManagerException, UnknownHostException, DataStoreException, IOException, CQEException, InterruptedException, Exception 
    { 
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("groupby_no_where_clause.json");
        //InputStream inStream = ClassLoader.getSystemResourceAsStream("or_expression.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        //NativeQueryPlan nativePlan = (NativeQueryPlan) queryPlan;
        //TransformQueryPlan transformPlan = (TransformQueryPlan) queryPlan;
        
        MongoDBQuery query = (new MongoDBInterprenter()).getMongoDBQueryParam(queryPlan);
        
        
        
        //App.testInsert();
        
        App app = new App();
        app.testAggregations();
        
        /*
        app.testRaquel();
        app.testSimple();
        app.testFindOne();
        app.testCoundParams();
        
        app.test();
        App.dropDb();
        */
        
        
        

        
        LTMClient.close();
        TMMiniCluster.stopMiniCluster();
        
    }
    
    public void testAggregations() throws UnknownHostException, TransactionManagerException, IOException, CQEException, InterruptedException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        //TMMiniCluster.startMiniCluster(true, 1);
        TMMiniCluster.startMiniCluster(true, 1, false);
        
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        //InputStream inStream = ClassLoader.getSystemResourceAsStream("native/tpch_aggregations.json");
        //InputStream inStream = ClassLoader.getSystemResourceAsStream("cqe_count.json");
        InputStream inStream = ClassLoader.getSystemResourceAsStream("groupby_no_where_clause.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
       
        LTMServerProxy proxy = new LTMServerProxy();
        
        ///LTMClient.getInstance().getConnection().startTransaction();
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        
        
        //TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        long startTime = System.currentTimeMillis();
        TxnCtx txnCtx = proxy.startTransaction();
        ResultSet resultSet = statement.execute(txnCtx);
        
        
        Object [][] result = null;
        
        int count = 0;
        while((result = resultSet.next()).length>0)  {
            printResults(result, count);
            Log.info("{} number of 'next' invocations ...", ++count);
        }
        
        long endTime = System.currentTimeMillis();
        Log.info("Total execution time: {} millis", (endTime - startTime));
        
        txnCtx.commit();
        
        
        
    }
    
    public void testRaquel() throws UnknownHostException, TransactionManagerException, IOException, CQEException, InterruptedException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 1);
        //TMMiniCluster.startMiniCluster(true, 1, false);
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("raquel_basic_mongo_insert.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
       
        LTMServerProxy proxy = new LTMServerProxy();
        
        ///LTMClient.getInstance().getConnection().startTransaction();
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("iddoc", "id");
        statement.setString("text", "text");
        
        
        //TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        TxnCtx txnCtx = proxy.startTransaction();
        ResultSet resultSet = statement.execute(txnCtx);        
        System.out.println(resultSet.next());
        txnCtx.commit();
        
        
        inStream = ClassLoader.getSystemResourceAsStream("raquel_basic_mongo_validate.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        statement.prepare(queryPlan);
        txnCtx = proxy.startTransaction();
        resultSet = statement.execute(txnCtx);  
        printResults(resultSet.next());
        txnCtx.commit();
        
        
        
        
        
        txnCtx = proxy.startTransaction();
        txnCtx.commit();
        //LTMClient.getInstance().getConnection().commit();
        //LTMClient.getInstance().getConnection().startTransaction();
        statement.close();
        connection.close();
        dds.close();
        LTMClient.getInstance().getConnection().commit();
    }
    
    public static void testInsert() throws UnknownHostException, TransactionManagerException, IOException, CQEException, InterruptedException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/insert_multiple.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("city", "athens");
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet res = statement.execute(txnCtx);
        
        LTMClient.getInstance().getConnection().commitSync();
        
        statement.close();
        connection.close();
        dds.close();
        
        
    }
    
    public void testSimple() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/simple_select.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setString("city", "athens");
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Log.info("Lenght: " + result.length);
        printResults(result);
        
        statement.close();
        connection.close();
        dds.close();
    }
    
    public void testFindOne() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/findone.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setInt("id", 5);
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Log.info("Lenght: " + result.length);
        printResults(result);
        Log.info((String) result[0][0]);
        
        statement.close();
        connection.close();
        dds.close();
    }
    
    public void testCoundParams() throws UnknownHostException, TransactionManagerException, DataStoreException, CQEException, IOException, InterruptedException {
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("native/count_params.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();        
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        statement.setInt("id", 5);
        
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();
        ResultSet resultSet = statement.execute(txnCtx);        
        LTMClient.getInstance().getConnection().commit();
        
        Object [][] result = resultSet.next();
        Log.info("Lenght: " + result.length);
        printResults(result);
        Log.info("Count: " + result[0][0]);
        
        statement.close();
        connection.close();
        dds.close();
    }
   
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        DB database = mongoClient.getDB(TestConfiguration.DATABASENAME);
        database.startTransaction();
        DBCollection collection = database.createCollection("reviewers", null);
        Random generator = new Random();
        List<DBObject> list = new ArrayList<>();
        for(int i=0; i<200; i++) {
            BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                    .append("name", "sample")
                    .append("pub_id", (i+1))
                    .append("random_id", generator.nextInt(1000));
            if(i<70)
                obj.append("city", "athens");
            else
                obj.append("city", "berlin");
            
            list.add(obj);
        }
        
        collection.insert(list);
        database.commit();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        collection = database.createCollection("publications", null);
        list = new ArrayList<>();
        for(int i=0; i<200; i++) {
            BasicDBObject obj = new BasicDBObject("rev_id", (i+1))
                    .append("name", RandomStringUtils.random(5))
                    .append("pub_id", (i+1));
            
            list.add(obj);
        }
        
        collection.insert(list);
        database.commit();
        database.startTransaction();
        database.commit();
        
        
        database.startTransaction();
        collection = database.createCollection("authors", null);
        list = new ArrayList<>();
        for(int i=0; i<200; i++) {
            BasicDBObject obj = new BasicDBObject("auth_id", (i+1))
                    .append("pub_id", (i+1));
            if(i<50)
                obj.append("name", "Pavlaras");
            else
                obj.append("name", "Kwstas Mitroglou!");
            
            list.add(obj);
        }
        
        collection.insert(list);
        database.commit();
        database.startTransaction();
        database.commit();
        
        Log.info("Records to nested tables added.");
        
        
        
        database.close();
    }
    
    
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASENAME);
        Log.info("database dropped");
    }
    
    
    public void test() throws IOException, CQEException, InterruptedException, TransactionManagerException, DataStoreException, Exception {
        Log.info("Starting minicluster, in case it is not yet started");
        TMMiniCluster.startMiniCluster(true, 0);
        
        Log.info("Setting properties file");
        Properties properties = new Properties();
        properties.put(ConfigurationParameters.HOSTNAME, TestConfiguration.HOSTNAME);
        properties.put(ConfigurationParameters.PORT, String.valueOf(TestConfiguration.PORT));
        properties.put(ConfigurationParameters.DATABASENAME, TestConfiguration.DATABASENAME);
        Log.info("Properties have been set");
        
        
        
        
        
        InputStream inStreamNested1 = ClassLoader.getSystemResourceAsStream("updated_named_nested1.json");
        ObjectMapper mapper = new ObjectMapper();
        QueryPlan queryPlanNested1= mapper.readValue(inStreamNested1, QueryPlan.class);
        InputStream inStreamNested2 = ClassLoader.getSystemResourceAsStream("updated_named_nested2.json");
        QueryPlan queryPlanNested2= mapper.readValue(inStreamNested2, QueryPlan.class);
        
        
        MongoCQEDatastore ddsNested1 = new MongoCQEDatastore();        
        ddsNested1.start(properties);
        Connection connectionNested1 = ddsNested1.getConnection(); 
        Statement statementNested1 = connectionNested1.createStatement();
        statementNested1.prepare(queryPlanNested1);
        MongoCQEDatastore ddsNested2 = new MongoCQEDatastore();        
        ddsNested2.start(properties);
        Connection connectionNested2 = ddsNested2.getConnection(); 
        Statement statementNested2 = connectionNested2.createStatement();
        statementNested2.prepare(queryPlanNested2);
        Log.info("Two nested queries are prepared");
        
        InputStream inStream = ClassLoader.getSystemResourceAsStream("updated_named.json");
        QueryPlan queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        MongoCQEDatastore dds = new MongoCQEDatastore();
        dds.start(properties);
        Connection connection = dds.getConnection(); 
        Statement statement = connection.createStatement();
        statement.prepare(queryPlan);
        
        statement.useNamedTable("t1", statementNested1);
        statement.useNamedTable("t2", statementNested2);
        statement.setInt("x", 30);
        statement.setInt("y", 10);
        LTMClient.getInstance().getConnection().startTransaction();
        TxnCtx txnCtx = LTMClient.getInstance().getConnection().getTxnCtx();  
        statement.execute(txnCtx);
        LTMClient.getInstance().getConnection().commit();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().commit();
        
        
        statementNested1.close();
        connectionNested1.close();
        ddsNested1.close();
        statementNested2.close();
        connectionNested2.close();
        ddsNested2.close();
        statement.close();
        connection.close();
        dds.close();
        
        
        inStream = ClassLoader.getSystemResourceAsStream("updated_named_validator.json");
        queryPlan = mapper.readValue(inStream, QueryPlan.class);
        
        dds = new MongoCQEDatastore();
        dds.start(properties);
        connection = dds.getConnection(); 
        statement = connection.createStatement();
        statement.prepare(queryPlan);
        LTMClient.getInstance().getConnection().startTransaction();
        txnCtx = LTMClient.getInstance().getConnection().getTxnCtx(); 
        
        ResultSet resultSet = statement.execute(txnCtx);
        Object [][] result = resultSet.next();
        Log.info(result.length + "");
        
        printResults(result);
        
        LTMClient.getInstance().getConnection().commit();
        resultSet.close();
        statement.close();
        connection.close();
        dds.close();
       
        
        
    }
    
    private static void printResults(Object [][] result, int numberOfIterations) {
        
        for(int i=0; i<result.length; i++) {
            Object[] row = result[i];
            String line = (200*numberOfIterations + (i+1)) + ": ";
            for (Object row_element : row) {
                if(row_element==null)
                    line +="null ";
                else
                    line += row_element.toString() + " ";
            }
            System.out.println(line);
        }
    }
    
    private static void printResults(Object [][] result) {
        
        for(int i=0; i<result.length; i++) {
            Object[] row = result[i];
            String line = (i+1) + ": ";
            for (Object row_element : row) {
                if(row_element==null)
                    line +="null ";
                else
                    line += row_element.toString() + " ";
            }
            System.out.println(line);
        }
    }
}
