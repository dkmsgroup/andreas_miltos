package eu.coherentpaas.mongodb.wrapper;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import eu.coherentpaas.cqe.CQEException;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.utils.WrapperMethodResponseType;
import eu.coherentpaas.mongodb.wrapper.exceptions.ParameterNotSetException;
import eu.coherentpaas.mongodb.wrapper.exceptions.QueryResultException;
import eu.coherentpaas.mongodb.wrapper.exceptions.UnknownResponseTypeException;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.MongoDBNativeQuery;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class NativeQueryExecutorHelper {
    private static final Logger Log = LoggerFactory.getLogger(NativeQueryExecutorHelper.class);
    
    public static MongoCQEResultSet createResponse(Object obj, WrapperMethodResponseType responseType, boolean includeIDInResult, Type [] signature, String [] columnNames) throws CQEException {
        switch(responseType) {
            case CURSOR:
                return createResponseCursor((DBCursor) obj, includeIDInResult, signature, columnNames);
            case DBOBJECT:
                return createResponseCursor((DBObject) obj, includeIDInResult, signature, columnNames);
            case AGGREGATION_OUTPUT:
                return createResponseCursor((AggregationOutput) obj, signature, columnNames);
            case LONG:
                return createResponseCursor((long) obj);
            case NULL:
                return null;
            default:
                UnknownResponseTypeException ex = new UnknownResponseTypeException("The response of the query is of unknown type");
                throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);                    
        }
    }
    
    private static MongoCQEResultSet createResponseCursor(long value) {
        MongoCQEResultSetLong resultSetLong = new MongoCQEResultSetLong(value);
        return resultSetLong;
    }
    
    private static MongoCQEResultSet createResponseCursor(DBObject dbObject, boolean includeIDInResult, Type [] signature, String [] columnNames) {
        MongoCQEResultSetDBObject cQEResultSetDBObject = new MongoCQEResultSetDBObject(dbObject, includeIDInResult, signature, columnNames);
        return cQEResultSetDBObject;
    }
    
    private static MongoCQEResultSet createResponseCursor(AggregationOutput aggregationOutput, Type [] signature, String [] columnNames) throws CQEException {
        if(aggregationOutput==null) {
            QueryResultException ex = new QueryResultException("Returned aggregation output from MongoDB transactional driver cannot be null");
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        }
        MongoCQEResultSetAggregationOutput cQEResultSetDBObject = new MongoCQEResultSetAggregationOutput(aggregationOutput, signature, columnNames);
        return cQEResultSetDBObject;
    }
    
    private static MongoCQEResultSet createResponseCursor(DBCursor cursor, boolean includeIDInResult, Type [] signature, String [] columnNames) {
        MongoCQEResultSet resultSet = new MongoCQEResultSet(cursor, includeIDInResult, signature, columnNames);
        return resultSet;
    }
    
   
    
    public static MongoDBNativeQuery assignParamValues(MongoDBNativeQuery query) throws CQEException {
        List<MongoDBParam> params = query.getParameters();
        for(MongoDBParam param : params) {
            String paramValue = getStringParamValue(param);
            ListIterator<String> it = query.getArgs().listIterator();
            while(it.hasNext()) {
                String argument = it.next();
                Log.debug("Argument before: {}", argument);
                argument = argument.replace("$$" + param.getName(), paramValue);
                Log.debug("Argument after : {}", argument);
                it.set(argument);
            }
        }
        
        return query;
    }
    
    private static String getStringParamValue(MongoDBParam param) throws CQEException {
        if(param.getValue()==null) {
            ParameterNotSetException ex = new ParameterNotSetException("Paraneter " + param.getName() + " has not been set yet");
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        }
        
        switch(param.getDataType())  {
            case ARRAY:
                return parseArray(((ArrayList<Object>)param.getValue()));
            case BINARY:
                return parseBinary(((Blob)param.getValue()));
            case BOOL:
                boolean booleanValue = (Boolean) param.getValue();
                return String.valueOf(booleanValue);
            case DICTIONARY:
                return parseDictionary((Dictionary<String, Object>)param.getValue());
            case DOUBLE:
                double doubleValue = (Double) param.getValue();
                return String.valueOf(doubleValue);
            case FLOAT:
                float floatValue = (Float) param.getValue();
                return String.valueOf(floatValue);
            case INT:
                int intValue = (Integer) param.getValue();
                return String.valueOf(intValue);
            case LONG:
                long longValue = (Long) param.getValue();
                return String.valueOf(longValue);
            case STRING:
                return "\"" + (String) param.getValue() + "\"";
            case TIMESTAMP:
                return parseTimestemp((Timestamp)param.getValue());
            case UNKNOWN:
                Log.warn("{} has unknown type. cannot evaluate its value", param.getDataType());
                return "";
            default:
                return "";
        }
    }
    
    private static String parseTimestemp(Timestamp timestamp) {
        return "\"" + String.format("%1$TD %1$TT", timestamp) + "\"";
    }
    
    private static String parseDictionary(Dictionary<String, Object> dictionary) {
        Map<String, Object> map = new HashMap<>(dictionary.size());
        Enumeration<String> enumur = dictionary.keys();
        while(enumur.hasMoreElements()) {
            String key = enumur.nextElement();
            map.put(key, dictionary.get(key));
        }
        BasicDBObject dbObject = new BasicDBObject(map);
        return dbObject.toJson();
    }
    
    @SuppressWarnings({"empty-statement", "BroadCatchBlock", "TooBroadCatch", "UseSpecificCatch"})
    private static String parseBinary(Blob blop) throws CQEException {
        StringBuilder sb = new StringBuilder();
        InputStream inStream = null;
        BufferedReader reader = null;
        try {
            inStream = blop.getBinaryStream();
            reader = new BufferedReader(new InputStreamReader(inStream));
            String line;
            while( ( line = reader.readLine() ) != null ) {
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
        } catch(Exception ex) {
            throw new CQEException(CQEException.Severity.Execution, ex.getMessage(), ex);
        } finally {
            if(inStream!=null) {
                try {
                    inStream.close();
                } catch(Exception ex1) {;/*DO NOTHING*/}
            }
            if(reader!=null) {
                try {
                    reader.close();
                } catch(Exception ex1) {;/*DO NOTHING*/}
            }
        }
        
        return sb.toString();
    }
    
    private static String parseArray(ArrayList<?> array) {
        BasicDBList basisDBList = new BasicDBList();
        for(int i=0; i<array.size(); i++) 
            basisDBList.add(array.get(i));
        return basisDBList.toString();
    }
}
