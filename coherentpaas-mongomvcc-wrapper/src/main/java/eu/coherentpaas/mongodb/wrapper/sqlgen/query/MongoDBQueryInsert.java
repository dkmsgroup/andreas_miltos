package eu.coherentpaas.mongodb.wrapper.sqlgen.query;

import com.mongodb.BasicDBObject;
import eu.coherentpaas.cqe.Type;
import eu.coherentpaas.mongodb.wrapper.MongoOperationType;
import eu.coherentpaas.mongodb.wrapper.sqlgen.NamedTableExpression;
import eu.coherentpaas.mongodb.wrapper.sqlgen.query.params.MongoDBParam;
import java.util.List;
import java.util.Objects;
import javax.annotation.concurrent.Immutable;

/**
 *
 * @author idezol
 */
@Immutable
public class MongoDBQueryInsert extends MongoDBTransformationQuery {
    protected final BasicDBObject insertDbObject;

    public MongoDBQueryInsert(BasicDBObject insertDbObject, String collectionName, MongoOperationType operationType, Type[] signature, String [] columnNames, List<MongoDBParam> parameters, List<NamedTableExpression> namedTables) {
        super(collectionName, operationType, signature, columnNames, parameters, namedTables);
        this.insertDbObject = insertDbObject;
    }

    public BasicDBObject getInsertDbObject() {
        return insertDbObject;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.insertDbObject);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MongoDBQueryInsert other = (MongoDBQueryInsert) obj;
        return Objects.equals(this.insertDbObject, other.insertDbObject);
    }

    @Override
    public String toString() {
        return "MongoDBQueryInsert{" + "insertDbObject=" + insertDbObject + '}';
    }
    
}
