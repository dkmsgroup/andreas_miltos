package eu.coherentpaas.mongodb.utils;


import org.bson.types.ObjectId;
import junit.framework.Assert;
import org.junit.Test;
/**
 *
 * @author idezol
 */
public class ObjectIdUtilsTest {
    

    @Test
    public void testLong() {
        
        Long longKey1 = new Long(100);
        Long longKey2 = new Long(200);
        Long longKey3 = new Long(5228);
        Long longKey4 = Long.valueOf("6166968228214299628");
        
        ObjectId obj1 = ObjectIdUtils.toObjectId(longKey1);
        ObjectId obj2 = ObjectIdUtils.toObjectId(longKey2);
        ObjectId obj3 = ObjectIdUtils.toObjectId(longKey3);
        ObjectId obj4 = ObjectIdUtils.toObjectId(longKey4);
        
        
        //Assert.assertTrue(obj1.compareTo(obj2)<0);
        //Assert.assertTrue(obj2.compareTo(obj3)<0);
        //Assert.assertTrue(obj3.compareTo(obj4)<0);
        //Assert.assertTrue(obj1.compareTo(obj4)<0);
        
        Long res1 = ObjectIdUtils.objectIdToLong(obj1);
        Long res2 = ObjectIdUtils.objectIdToLong(obj2);
        Long res3 = ObjectIdUtils.objectIdToLong(obj3);
        Long res4 = ObjectIdUtils.objectIdToLong(obj4);
        
        Assert.assertEquals(longKey1, res1);
        Assert.assertEquals(longKey2, res2);
        Assert.assertEquals(longKey3, res3);
        Assert.assertEquals(longKey4, res4);
        
    }
}
