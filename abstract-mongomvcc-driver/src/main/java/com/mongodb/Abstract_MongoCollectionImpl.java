package com.mongodb;

import com.mongodb.MongoException;
import com.mongodb.ReadPreference;
import static com.mongodb.ReadPreference.primary;
import com.mongodb.binding.ClusterBinding;
import com.mongodb.binding.ReadBinding;
import com.mongodb.binding.ReadWriteBinding;
import com.mongodb.binding.WriteBinding;
import com.mongodb.connection.Cluster;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public abstract class Abstract_MongoCollectionImpl implements CpMongoCollection {
    protected final String collectionName;
    //protected Long version;
    //protected Long tid;
    
    protected final Abstract_MongoDatabaseImpl abstractDB;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply;
    //protected final List<AbstractMongoCursorImpl> clientCursors = new ArrayList<>();
    protected AtomicBoolean isClosed = new AtomicBoolean(false);
    protected AtomicBoolean isAutoCommit = new AtomicBoolean(false);
    protected boolean hasUpdates = false;
    protected MongoTransactionalContext mongoTransactionalContext;

    protected Abstract_MongoCollectionImpl(Abstract_MongoDatabaseImpl abstractDB, String collectionName, MongoTransactionalContext mongoTransactionalContext) {
        this.abstractDB = abstractDB;
        this.collectionName = collectionName;
        this.writeset = new ConcurrentHashMap<>();
        this.mongoTransactionalContext = mongoTransactionalContext;
        this.pendingWritesetsToApply = new ConcurrentHashMap<>();
    }
    
    protected Abstract_MongoCollectionImpl(Abstract_MongoDatabaseImpl abstractDB, String collectionName, MongoTransactionalContext mongoTransactionalContext, ConcurrentMap<Object, Bson> writeset, ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply) {
        this.abstractDB = abstractDB;
        this.collectionName = collectionName;
        this.writeset = writeset;
        this.pendingWritesetsToApply = pendingWritesetsToApply;
        this.mongoTransactionalContext = mongoTransactionalContext;
    }
    
    

    @Override
    public String getName() {
        return this.collectionName;
    }
    
    public String getDabaseName() {
        return this.abstractDB.getName();
    }

    @Override
    public void close() {
        _close();
        this.abstractDB.removeCollection(this);
    }
    
    /*
    protected void fireConflictResolutionDetectedEvent() throws TransactionManagerException {
        this.abstractDB.rollback();
    }
    */
    
    protected void _checkClose() {
        if(isClosed.get())
            throw new IllegalStateException("Collection has been closed");
    }
    
    
    
    protected abstract void _getTransactionalContext() throws CoherentPaaSException;
    
    
    
    protected abstract void clearTxnCtx();
    
    protected abstract void _close();
    
    
    protected ReadBinding getReadBinding(final ReadPreference readPreference) {
        return getReadWriteBinding(readPreference);
    }
    
    protected ReadWriteBinding getReadWriteBinding(final ReadPreference readPreference) {
        return new ClusterBinding(getCluster(), readPreference);
    }
    
    protected Cluster getCluster() {
        return this.abstractDB._depracedMongoClient.getCluster();
    }
    
    protected WriteBinding getWriteBinding() {
        return getReadWriteBinding(primary());
    }
    
    protected void autoCommit() {
        try {
            LTMClient.getInstance().getConnection().commitSync();
            //LTMClient.getInstance().getConnection().commit();
            
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        } finally {
            this.isAutoCommit.set(false);
        }
        
        //again for flushing...
        /*
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            Logger.getLogger(Abstract_MongoCollectionImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.isAutoCommit.set(false);
        }
        */
    }
    
    protected void addToPendingWritesetsToApply(long tid, WriteSetToApply writeSetToApply) {
        this.pendingWritesetsToApply.put(tid, writeSetToApply);
    }
     
    
    //protected abstract ConcurrentMap _getWriteSet() throws MongoException;
    
    protected abstract void _applyWriteSet(long tid, long commitTmp) throws MongoException;
    
    protected abstract void _rollback(long tid) throws MongoException;
    
}
