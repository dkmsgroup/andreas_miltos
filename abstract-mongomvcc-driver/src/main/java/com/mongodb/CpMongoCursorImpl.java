package com.mongodb;

import com.mongodb.ServerAddress;
import com.mongodb.ServerCursor;
import com.mongodb.client.MongoCursor;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import java.util.ArrayList;
import org.bson.BsonDocument;
import org.bson.BsonDocumentReader;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 * @param <T>
 */
public class CpMongoCursorImpl<T extends Bson> extends AbstractMongoCursorImpl implements MongoCursor<T> {
    private static final Logger Log = LoggerFactory.getLogger(CpMongoCursorImpl.class);
    
    protected final MongoCursor<T> mongoCursor;
    protected final Class<T> resultClass;
    protected final CodecRegistry codecRegistry;
    protected T _curObj = null;
    protected T _nextObj = null;

    protected CpMongoCursorImpl(MongoCursor<T> mongoCursor, Class<T> resultClass, CollectionBean dbCollection, CodecRegistry codecRegistry, int skip, int limit) {
        super(dbCollection, skip, limit);
        this.resultClass = resultClass;
        this.mongoCursor = mongoCursor;
        this.codecRegistry = codecRegistry;
    }
    
    @Override
    public boolean hasNext() {
        _checkIfClosed(); 
        return _hasNext();
    }

    @Override
    public T next() {
        _checkIfClosed();
        return _next();
    }

    @Override
    public T tryNext() {
        _checkIfClosed();
        _hasNext();
        return  _curObj;
    }

    @Override
    public ServerCursor getServerCursor() {
        _checkIfClosed();
        return this.mongoCursor.getServerCursor();
    }

    @Override
    public ServerAddress getServerAddress() {
        _checkIfClosed();
        return this.mongoCursor.getServerAddress();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cursors do not support removal");
    }

    @Override
    protected void _close() {
        this.mongoCursor.close();
        this._curObj = null;
        this._nextObj = null;
        this.isClosed.set(true);
    }
    
    
    
    protected boolean _hasNext() {
        
        //chech if there is a nextObj
        if(_nextObj!=null)
            return true;
        
        //check if already has scanned more than the limit
        if((limit>0)&&(totalScanned>=limit))
            return false;
        
        //long startRead = System.nanoTime();
        while(this.mongoCursor.hasNext()) {
            T documentFound = this.mongoCursor.next();
            
            if(!(documentFound instanceof Bson)) 
                throw new IllegalStateException("retrieved document is not in a valid Bson type");
            
            BsonDocument bsonFound = ((Bson) documentFound).toBsonDocument(resultClass, codecRegistry);
            
            
            //if document is already in the writeset, then check if this version is outdated or current (private)
            //if((this.writeset!=null) && (this.writeset.containsKey(bsonFound.getObjectId(MongoMVCCFields.DATA_ID).getValue()))) {
            if((this.writeset!=null) && (this.writeset.containsKey(bsonFound.get(MongoMVCCFields.DATA_ID)))) {
                
                if(bsonFound.getInt64(MongoMVCCFields.TID).getValue()==this.mongoTransactionalContext.getCtxId()) {
                    //discard the first 'skip' documents
                    if(skip>0) {
                        skip--;
                        continue;
                    }
                    
                    //_nextObj = documentFound;
                    _nextObj = fixIdentifier(documentFound);
                    totalScanned++;
                    
                    return true;
                } else {
                    //there's an updated version for this document, do not count it
                    //continue;
                }
            } else {
                
                //document is from the public versions --no updated version found in the private writeset
                //discard the first 'skip' documents
                if(skip>0) {
                    skip--;
                    continue;
                }
                
                
                //_nextObj = documentFound;
                _nextObj = fixIdentifier(documentFound);
                totalScanned++;
                     
                return true;
            }
        }
        
        return false;
    }
    
    //returns the _nextObject and sets it to be the current (while set the _nextObj to null
    //if nextObj is null, then checks a hasNext to find the next one
    protected T _next() {
        
        if(_nextObj!=null) {
            _curObj = _nextObj;
            _nextObj = null;
            return _curObj;
        }
        else {
            if(_hasNext()) {
                T obj =_next();
                if(obj!=null)
                    return obj;
            }
        }
        
        //couldn't find any
        return null;
    }
   
   
    protected T fixIdentifier(final T bson) {
        //long startTme = System.nanoTime();
        BsonDocument dbObject = ((Bson)bson).toBsonDocument(resultClass, codecRegistry);
        BsonValue objID = dbObject.get(MongoMVCCFields.DATA_ID);
        dbObject.put(MongoMVCCFields._ID, objID);
        dbObject.remove(MongoMVCCFields.DATA_ID);
        
        //T obj = codecRegistry.get(resultClass).decode(new BsonDocumentReader(dbObject), null);
        //long endTim = System.nanoTime();
        //Log.info("Total time to fixIdentifier: {}", (endTim-startTme));
        
        //return obj;
        return codecRegistry.get(resultClass).decode(new BsonDocumentReader(dbObject), null);
        
    }
    
}
