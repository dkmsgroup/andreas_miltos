package com.mongodb;

import com.mongodb.client.ListDatabasesIterable;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.TransasctionalMongo;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public abstract class AbstractMongoClient implements TransasctionalMongo, IMongoClient {
    protected static final Logger Log = LoggerFactory.getLogger(AbstractMongoClient.class);
    
    protected final AtomicLong dbCounter = new AtomicLong(0);
    protected static DeprecatedMongoClient mongoClient = null;
    protected ServerAddress serverAddress;
    protected MongoClientOptions options;
    protected final ConcurrentMap<Long, Abstract_MongoDatabaseImpl> databases = new ConcurrentHashMap<>();
    protected final ConcurrentMap<Long, Abstract_MongoDatabaseImpl> openTransactionalDatabases = new ConcurrentHashMap<> ();
    protected final ReentrantLock lock = new ReentrantLock();
    
    protected void removeDatabase(Long dbId) {
        this.databases.remove(dbId);
    }
    
    protected void addToOpenTransactions(Abstract_MongoDatabaseImpl db, long tid) {
        this.openTransactionalDatabases.putIfAbsent(tid, db);
    }
    
    protected void removeFromOpenTransactions(Abstract_MongoDatabaseImpl clientDatabase, long tid) {
        this.openTransactionalDatabases.remove(tid);
    }
    
    protected DeprecatedMongoClient getDepracedMongoClient() {
        return mongoClient;
    }
    
    
    
    
    /**
     * Gets the list of credentials that this client authenticates all connections with
     *
     * @return the list of credentials
     * @since 2.11.0
     */
    @Override
    public List<MongoCredential> getCredentialsList() {
        return mongoClient.getCredentialsList();
    }

    @Override
    public MongoClientOptions getMongoClientOptions() {
        return options;
    }
    
    
    /**
     * Gets a {@code String} representation of current connection point, i.e. master.
     *
     * @return server address in a host:port form
     */
    @Override
    public String getConnectPoint(){
        return mongoClient.getConnectPoint();
    }
    
    /**
     * Get the status of the replica set cluster.
     *
     * @return replica set status information
     */
    @Override
    public ReplicaSetStatus getReplicaSetStatus() {
        return this.mongoClient.getReplicaSetStatus();
    }
    
    /**
     * Gets the address of the current master
     *
     * @return the address
     */
    @Override
    public ServerAddress getAddress(){
        return mongoClient.getAddress();
    }
    
    /**
     * Gets a list of all server addresses used when this Mongo was created
     *
     * @return list of server addresses
     * @throws MongoException
     */
    @Override
    public List<ServerAddress> getAllAddress() {
        return mongoClient.getAllAddress();
    }
    
    /**
     * Gets the list of server addresses currently seen by this client. This includes addresses auto-discovered from a replica set.
     *
     * @return list of server addresses
     * @throws MongoException
     */
    @Override
    public List<ServerAddress> getServerAddressList() {
        return mongoClient.getServerAddressList();
    }
    
    /**
     * Sets the write concern for this database. Will be used as default for writes to any collection in any database. See the documentation
     * for {@link WriteConcern} for more information.
     *
     * @param concern write concern to use
     */
    @Override
    public void setWriteConcern( WriteConcern concern ){
        mongoClient.setWriteConcern(concern);
    }
    
    /**
     * Gets the default write concern
     *
     * @return the default write concern
     */
    @Override
    public WriteConcern getWriteConcern(){
        return mongoClient.getWriteConcern();
    }
    
    
    /**
     * Sets the read preference for this database. Will be used as default for reads from any collection in any database. See the
     * documentation for {@link ReadPreference} for more information.
     *
     * @param preference Read Preference to use
     */
    @Override
    public void setReadPreference(ReadPreference preference) {
        mongoClient.setReadPreference(preference);
    }
    
    /**
     * Gets the default read preference
     *
     * @return the default read preference
     */
    @Override
    public ReadPreference getReadPreference(){
        return mongoClient.getReadPreference();
    }
    
    /**
     * Add a default query option keeping any previously added options.
     *
     * @param option value to be added to current options
     */
    @Override
    public void addOption( int option ){
        mongoClient.addOption(option);
    }

    /**
     * Set the default query options.  Overrides any existing options.
     *
     * @param options value to be set
     */
    @Override
    public void setOptions( int options ){
        mongoClient.setOptions(options);
    }

    /**
     * Reset the default query options
     */
    @Override
    public void resetOptions(){
        mongoClient.resetOptions();
    }

    /**
     * Gets the default query options
     *
     * @return an int representing the options to be used by queries
     */
    @Override
    public int getOptions(){
        return mongoClient.getOptions();
    }
    
    /**
     * Gets the maximum size for a BSON object supported by the current master server. Note that this value may change over time depending
     * on which server is master.
     *
     * @return the maximum size, or 0 if not obtained from servers yet.
     * @throws MongoException
     */
    @Override
    public int getMaxBsonObjectSize() {
        return mongoClient.getMaxBsonObjectSize();
    }
    
    /**
     * Forces the master server to fsync the RAM data to disk This is done automatically by the server at intervals, but can be forced for
     * better reliability.
     *
     * @param async if true, the fsync will be done asynchronously on the server.
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public CommandResult fsync(boolean async) {
        return mongoClient.fsync(async);
    }
    
    /**
     * Forces the master server to fsync the RAM data to disk, then lock all writes. The database will be read-only after this command
     * returns.
     *
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public CommandResult fsyncAndLock() {
        return mongoClient.fsyncAndLock();
    }
    
    /**
     * Unlocks the database, allowing the write operations to go through. This command may be asynchronous on the server, which means there
     * may be a small delay before the database becomes writable.
     *
     * @return {@code DBAbstractObject} in the following form {@code {"ok": 1,"info": "unlock completed"}}
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public DBObject unlock() {
        return mongoClient.unlock();
    }
    
    
    /**
     * Returns true if the database is locked (read-only), false otherwise.
     *
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/fsync/ fsync command
     */
    @Override
    public boolean isLocked() {
        return mongoClient.isLocked();
    }
    
    
    @Override
    public String toString() {
        return "Mongo{" +
                "serverAddress=" + this.serverAddress +
                ", options=" + this.options +
                '}';
    }
    
    
    /**
     * Returns the list of databases used by the driver since this Mongo instance was created. This may include DBAbstracts that exist in the client
 but not yet on the server.
     *
     * @return a collection of database objects
     */
    @Override
    public Collection<IDB> getUsedDatabases(){
        List<IDB> list = new ArrayList<>();
        Iterator<Entry<Long, Abstract_MongoDatabaseImpl>> iterator = this.databases.entrySet().iterator();
        while(iterator.hasNext()) {
            Abstract_MongoDatabaseImpl database = iterator.next().getValue();
            if(database instanceof DBAbstract)
                list.add((DBAbstract) database);
        }
        
        return list;
    }
    
    /**
     * Gets a list of the names of all databases on the connected server.
     *
     * @return list of database names
     * @throws MongoException
     */
    @Override
    public List<String> getDatabaseNames(){
        List<String> list = new ArrayList<>();
        Iterator<Entry<Long, Abstract_MongoDatabaseImpl>> it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            list.add(it.next().getValue().getName());
        }
        return list;
    }
    
    
    /**
     * Gets a database object from this MongoDBAbstract instance.
     *
     * @param dbname the name of the database to retrieve
     * @return a DBAbstract representing the specified database
     */
    @Override
    public abstract DBAbstract getDB(final String dbname);
    
    
    
    
    
    
    /**
     * Drops the database if it exists.
     *
     * @param dbName name of database to drop
     * @throws MongoException
     */
    @Override
    public void dropDatabase(String dbName) {
        Iterator it = this.openTransactionalDatabases.entrySet().iterator();
        while(it.hasNext()) {
            Abstract_MongoDatabaseImpl db = (Abstract_MongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            if(db.getName().equals(dbName)) {
                try {
                    if(LTMClient.getInstance().isTransactionOpen())
                        db.rollback();
                    it.remove();
                } catch (Exception ex) {
                    throw new MongoException(ex.getMessage(), ex);
                }
            }
            
        }
        
        it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            Abstract_MongoDatabaseImpl db = (Abstract_MongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            if(db.getName().equals(dbName)) {
                db._close();                
                it.remove();
            }
        }
        
        mongoClient.dropDatabase(dbName);
    }
    
    
    /**
     * Rollbacks all open transcations
     * Then closes all active databases
     * Finally Closes the underlying connector, which in turn closes all open connections. Once called, this Mongo instance can no longer be used.
     * 
     */
    @Override
    public void close(){
        
        Iterator it = this.openTransactionalDatabases.entrySet().iterator();
        while(it.hasNext()) {
            Abstract_MongoDatabaseImpl db = (Abstract_MongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            try {
                db.rollback();
            } catch (Exception ex) {
                Log.warn("Database " + db.getName() + " could not be rollbacked");
            }
        }
        this.openTransactionalDatabases.clear();
        
        it = this.databases.entrySet().iterator();
        while(it.hasNext()) {
            Abstract_MongoDatabaseImpl db = (Abstract_MongoDatabaseImpl) ((Map.Entry) it.next()).getValue();
            db._close();
        }
        this.databases.clear();
        
        
        mongoClient.close();
        mongoClient = null;
    }
    
    
    //CODE COMPATIBLE FOR AbstractMongoClient v3.0.x
    
    
    /**
     * Gets the default codec registry.  It includes the following providers:
     * <ul>
     * <li>{@link org.bson.codecs.ValueCodecProvider}</li>
     * <li>{@link org.bson.codecs.DocumentCodecProvider}</li>
     * <li>{@link com.mongodb.DBObjectCodecProvider}</li>
     * <li>{@link org.bson.codecs.BsonValueCodecProvider}</li>
     * <li>{@link com.mongodb.client.model.geojson.codecs.GeoJsonCodecProvider}</li>
     * </ul>
     *
     * @return the default codec registry
     * @see MongoClientOptions#getCodecRegistry()
     * @since 3.0
     */
    public static CodecRegistry getDefaultCodecRegistry() {
        return DeprecatedMongoClient.getDefaultCodecRegistry();
    }
    
    
    
    /**
     * Get a list of the database names
     *
     * @mongodb.driver.manual reference/commands/listDatabases List Databases
     * @return an iterable containing all the names of all the databases
     * @since 3.0
     */
    public MongoIterable<String> listDatabaseNames() {
        return mongoClient.listDatabaseNames();
    }
    
    
    /**
     * Gets the list of databases
     *
     * @return the list of databases
     * @since 3.0
     */
    public ListDatabasesIterable<Document> listDatabases() {
        return mongoClient.listDatabases();
    }
    
    
    /**
     * Gets the list of databases
     *
     * @param clazz the class to cast the database documents to
     * @param <T>   the type of the class to use instead of {@code Document}.
     * @return the list of databases
     * @since 3.0
     */
    public <T> ListDatabasesIterable<T> listDatabases(final Class<T> clazz) {
        return mongoClient.listDatabases(clazz);
    }
    
    
    public abstract MongoDatabase getDatabase(final String databaseName);
    
    
    /*
    //@Override
    public void applyWS() {
        
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();
        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            applyWS(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not applyWS " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }
    
    
    @Override
    public long applyWS(TxnCtx txnctx) {
        long startApply = System.currentTimeMillis();
        Log.debug("Ordered to applyWS for tid: " + txnctx.getCtxId() + " at: " + txnctx.getCommitTimestamp());
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        
        long result = applyWS(mongoTxnCtx);
        long endApply = System.currentTimeMillis();
        Log.debug("Total time to applyWS: {} msecs", (endApply-startApply));
        return result;
    }
    
    private long applyWS(MongoTransactionContext mongoTxnCtx) {
        long tid = mongoTxnCtx.getTid();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._applyWS(mongoTxnCtx);
                this.openTransactionalDatabases.remove(tid);
            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }
        } catch (TransactionManagerException ex) {
            Log.error("Could not applyWS " + " for transaction tid: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage());
        } 
        
        //FIX WHAT TO RETURN
        return tid;
    }

    
    
    public void rollback() {
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            rollback(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not rollback " + ex.getClass().getName() + ": " + ex.getMessage());
        }
    }
    
    @Override
    public void rollback(TxnCtx txnctx) {
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        
        rollback(mongoTxnCtx);
    }

    private void rollback(MongoTransactionContext mongoTxnCtx) {
        Log.info("MongoClient is invoked to rollback for: " + mongoTxnCtx.toString());
        Long tid = null;
        
        tid = mongoTxnCtx.getTid();
        if(this.openTransactionalDatabases.containsKey(tid)) {
            this.openTransactionalDatabases.get(tid)._rollback(mongoTxnCtx);
            this.openTransactionalDatabases.remove(tid);
        } else {
            Log.warn("There is no open transaction associated with tid " + tid);
        }
    }
    
    //@Override
    public byte[] getWS() throws DataStoreException {
        byte [] bytes = null;
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext();        
        try {
            mongoTxnCtx.setTid(LTMClient.getInstance().getConnection().getCtxId());
            mongoTxnCtx.setStartTimestamp(LTMClient.getInstance().getConnection().getStartTimestamp());
            mongoTxnCtx.setCommitTimestamp(LTMClient.getInstance().getConnection().getCommitTimestamp());
            
            bytes = getWS(mongoTxnCtx);
        } catch (TransactionManagerException ex) {
            Log.error("Could not get transactional context for getting the transaction's context");
        }
        return bytes;
    }
    
    @Override
    public byte[] getWS(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("Ordered to getWS for tid: " + txnctx.getCtxId() + " at: " + txnctx.getCommitTimestamp());
        
        //long startTmp = System.nanoTime();
        
        MongoTransactionContext mongoTxnCtx = new MongoTransactionContext(
                txnctx.getCtxId(), 
                txnctx.getStartTimestamp(), 
                txnctx.getCommitTimestamp());
        //long intermTmp = System.nanoTime();
        
        
        byte [] bytes = getWS(mongoTxnCtx);
        
        //long endTmp = System.nanoTime();
        //Log.info("Time to getBytes: {} and time to get ctx: {}", (endTmp-intermTmp), (intermTmp-startTmp));
        
        return bytes;
    }
    
    private byte[] getWS(MongoTransactionContext mongoTxnCtx) throws DataStoreException {

        byte [] bytes = null;
        DataBaseRecoveryWriteset dataBaseRecoveryWriteset = null;
        
        long tid = mongoTxnCtx.getTid();
        
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                AbstractMongoDatabaseImpl database = this.openTransactionalDatabases.get(tid);
                List<CollectionRecoveryWriteset> list = database._getWS();
                if(!list.isEmpty())
                    dataBaseRecoveryWriteset = new DataBaseRecoveryWriteset(database.databaseName, list);

            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }

            //if emmpty writeset, then return null for the HTM to continue
            if(dataBaseRecoveryWriteset==null) 
                return null;

            bytes = SerializationUtils.serialize(dataBaseRecoveryWriteset);
        
        } catch (Exception ex) {
            String message = "Could not get writeset for transaction: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage();
            Log.error(message);
            throw new DataStoreException(message);
        }
        
        return bytes;
    }
    
   
    @Override
    public void redoWS(long l, byte[] bytes) throws DataStoreException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        
        
        List<byte []> listWritesets = new ArrayList<>();
        listWritesets = ByteOperators.getBytesFromWriteSet(bytes);
        for(byte [] collectionWritesetBytes : listWritesets ) {
            ByteArrayInputStream byteIn = new ByteArrayInputStream(collectionWritesetBytes);
            ObjectInputStream in;
            try {
                in = new ObjectInputStream(byteIn);
                Map<String, Bson> collectionWriteset = (Map<String, Bson>) in.readObject();
                
                String databaseName = null;
                String collectionName = null;
                Map<ObjectId, Bson> writeset = new HashMap<>();
                Iterator<Entry<String, Bson>> it = collectionWriteset.entrySet().iterator();
                while(it.hasNext()) {
                    Entry<String, Bson> entry = it.next();
                    String [] values = entry.getKey().split(MongoMVCCFields.STRING_SEPARATOR);
                    if(databaseName==null) {
                        databaseName = values[0];
                        collectionName = values[1];
                    }
                    
                    ObjectId objId = new ObjectId(values[2]);
                    writeset.put(objId, entry.getValue());
                }
                
                
                MongoDatabase db = this.getDatabase(databaseName);
                CpMongoCollectionImpl<Document> coll = (CpMongoCollectionImpl<Document>) db.getCollection(collectionName);
                coll.recover(l, writeset);
                
            } catch (IOException | ClassNotFoundException ex) {
                throw new DataStoreException(ex.getMessage());
            }
        }
        */
        
    
/*
    @Override
    public void unleash(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("Mongoclient is invoked to unleash for: " + txnctx.toString());
        
        long tid = txnctx.getCtxId();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._unleash(txnctx);
                this.openTransactionalDatabases.remove(tid);
            }
        } catch(Exception ex) {
            Log.error("Exception when trying to unleash transactionid: {}. {}: {}", tid, ex.getClass().getName(), ex.getMessage());
            throw new DataStoreException(ex.getMessage());
        }
    }
 
    @Override
    public void notify(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("MongoClient is invoked to notify for: " + txnctx.toString());
    }

    @Override
    public void garbageCollect(long l) {
        //System.out.println("Garbage collection was invoked with timestamp: " + l);
        if(Log.isDebugEnabled())
            Log.debug("Garbage collection was invoked with timestamp: {}", l);
    }
    */

    @Override
    public abstract long applyMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    @Override
    public abstract void rollbackMongo(MongoTransactionalContext mongoTxnctx);

    @Override
    public abstract byte[] getMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    @Override
    public abstract void notifyMongo(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    @Override
    public abstract void garbageCollectMongo(long l);

}