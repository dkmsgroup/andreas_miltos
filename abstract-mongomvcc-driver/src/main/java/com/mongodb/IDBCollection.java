package com.mongodb;

import com.mongodb.AggregationOptions;
import com.mongodb.AggregationOutput;
import com.mongodb.CommandResult;
import com.mongodb.Cursor;
import com.mongodb.DBDecoderFactory;
import com.mongodb.DBEncoder;
import com.mongodb.DBEncoderFactory;
import com.mongodb.DBObject;
import com.mongodb.GroupCommand;
import com.mongodb.InsertOptions;
import com.mongodb.MapReduceCommand;
import com.mongodb.MapReduceOutput;
import com.mongodb.ParallelScanOptions;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author stavroula
 */
public interface IDBCollection  extends CpMongoCollection {
    
   public WriteResult insert(DBObject[] arr , WriteConcern concern );
   
   public WriteResult insert(DBObject[] arr , WriteConcern concern, DBEncoder encoder) ;
   
   public WriteResult insert(DBObject o , WriteConcern concern );
   
   public WriteResult insert(DBObject ... arr);
   
   public WriteResult insert(WriteConcern concern, DBObject ... arr);
   
   public WriteResult insert(List<DBObject> list );
   
   public WriteResult insert(List<DBObject> list, WriteConcern concern );
   
   public WriteResult insert(List<DBObject> list, WriteConcern concern, DBEncoder encoder);
   
   public WriteResult insert(final List<DBObject> documents, final InsertOptions insertOptions) ;
   
   public WriteResult update( DBObject q , DBObject o , boolean upsert , boolean multi , WriteConcern concern );
   
   public WriteResult update( DBObject q , DBObject o , boolean upsert , boolean multi , WriteConcern concern, DBEncoder encoder );
   
   public WriteResult update( DBObject q , DBObject o , boolean upsert , boolean multi );
   
   public WriteResult update( DBObject q , DBObject o );
   
   public WriteResult updateMulti( DBObject q , DBObject o );
   
   public WriteResult remove( DBObject o , WriteConcern concern );
   
   public WriteResult remove( DBObject o , WriteConcern concern, DBEncoder encoder );
   
   public WriteResult remove( DBObject o );
   
   public DBObject findOne(final Object id);
   
   public DBObject findOne(final Object id, final DBObject projection);
   
   public DBObject findAndModify(DBObject query, DBObject fields, DBObject sort, boolean remove, DBObject update, boolean returnNew, boolean upsert);
   
   public DBObject findAndModify(final DBObject query, final DBObject fields, final DBObject sort,
                                  final boolean remove, final DBObject update,
                                  final boolean returnNew, final boolean upsert,
                                  final long maxTime, final TimeUnit maxTimeUnit);
   
   public DBObject findAndModify( DBObject query , DBObject sort , DBObject update);
   
   public DBObject findAndModify( DBObject query , DBObject update );
   
   public DBObject findAndRemove( DBObject query );
   
   public void createIndex( final String name );
   
   public void createIndex( final DBObject keys );
   
   public void createIndex( DBObject keys , DBObject options );
   
   public void createIndex( DBObject keys , String name );
   
   public void createIndex( DBObject keys , String name , boolean unique );
   
   public void setHintFields(final List<DBObject> indexes);
   
   public IDBCursor find( DBObject ref );
   
   public IDBCursor find( DBObject ref , DBObject keys );
   
   public IDBCursor find();
   
   public IDBCursor find( DBObject query , DBObject fields , int numToSkip , int batchSize );
   
   public IDBCursor find( DBObject query , DBObject fields , int numToSkip , int batchSize , int options );
   
   public DBObject findOne();
   
   public DBObject findOne( DBObject o );
   
   public DBObject findOne( DBObject o, DBObject fields );
   
   public DBObject findOne( DBObject o, DBObject fields, DBObject orderBy);
   
   public DBObject findOne( DBObject o, DBObject fields, ReadPreference readPref );
   
   public DBObject findOne(DBObject o, DBObject fields, DBObject orderBy, ReadPreference readPref);
   
   public WriteResult save(DBObject jo);
   
   public WriteResult save( DBObject jo, WriteConcern concern );
   
   public void dropIndexes();
   
   public void dropIndexes( String name );
   
   public void drop();
   
   public long count();
   
   public long count(DBObject query);
   
   public long count(DBObject query, ReadPreference readPrefs );
   
   public long getCount();
   
   public long getCount(ReadPreference readPrefs);
   
   public long getCount(DBObject query);
   
   public long getCount(DBObject query, DBObject fields);
   
   public long getCount(DBObject query, DBObject fields, ReadPreference readPrefs);
   
   public long getCount(DBObject query, DBObject fields, long limit, long skip);
   
   public long getCount(DBObject query, DBObject fields, long limit, long skip, ReadPreference readPrefs );
   
   public IDBCollection rename( String newName );
   
   public IDBCollection rename( String newName, boolean dropTarget );
   
   public DBObject group( DBObject key , DBObject cond , DBObject initial , String reduce );
   
   public DBObject group( DBObject key , DBObject cond , DBObject initial , String reduce , String finalize );
   
   public DBObject group( DBObject key , DBObject cond , DBObject initial , String reduce , String finalize, ReadPreference readPrefs );
   
   public DBObject group( GroupCommand cmd );
   
   public DBObject group( GroupCommand cmd, ReadPreference readPrefs );
   
   public List distinct( String key );
   
   public List distinct( String key, ReadPreference readPrefs );
   
   public List distinct( String key , DBObject query );
   
   public List distinct( String key , DBObject query, ReadPreference readPrefs );
   
   public MapReduceOutput mapReduce( String map , String reduce , String outputTarget , DBObject query );
   
   public MapReduceOutput mapReduce(String map, String reduce, String outputTarget, MapReduceCommand.OutputType outputType,
                                     DBObject query);
   
   public MapReduceOutput mapReduce(String map, String reduce, String outputTarget, MapReduceCommand.OutputType outputType, DBObject query,
                                     ReadPreference readPrefs);
   
   public MapReduceOutput mapReduce( MapReduceCommand command );
   
   public AggregationOutput aggregate(final List<DBObject> pipeline);
   
   public AggregationOutput aggregate(final List<DBObject> pipeline, ReadPreference readPreference);
   
   public Cursor aggregate(final List<DBObject> pipeline, AggregationOptions options);
   
   public Cursor aggregate(List<DBObject> pipeline, AggregationOptions options, ReadPreference readPreference);
   
   public CommandResult explainAggregate(final List<DBObject> pipeline, final AggregationOptions options);
   
   public BulkWriteOperation initializeOrderedBulkOperation();
   
   public BulkWriteOperation initializeUnorderedBulkOperation();
   
   public List<DBObject> getIndexInfo();

   public void dropIndex(final DBObject keys) ;
   
   public void dropIndex(final String indexName);
   
   public CommandResult getStats();
   
   public boolean isCapped();
   
   public IDBCollection getCollection( String n );
   
   
   public String getFullName();
   
   public IDB getDB();
   
    public void setObjectClass(Class c);    
    
    public void setInternalClass( String path , Class c );
    
    public void setWriteConcern(final WriteConcern writeConcern);
    
    public WriteConcern getWriteConcern();
    
    public void setReadPreference(final ReadPreference preference);
    
    public List<Cursor> parallelScan(ParallelScanOptions options);
    
    public ReadPreference getReadPreference();
    
    public void addOption(final int option);
   
    public void setOptions(final int options);
    
    public void resetOptions();
    
    public int getOptions();
    
    public void setDBDecoderFactory(DBDecoderFactory fact);
    
    public DBDecoderFactory getDBDecoderFactory();
    
    public void setDBEncoderFactory(DBEncoderFactory fact);
    
    public DBEncoderFactory getDBEncoderFactory();
    
    
    
}