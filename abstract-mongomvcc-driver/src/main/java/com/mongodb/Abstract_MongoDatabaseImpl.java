package com.mongodb;

import eu.coherentpaas.mongodb.utils.CollectionRecoveryWriteset;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public abstract class Abstract_MongoDatabaseImpl implements Cp_MongoDatabase {
    private final long dbID;
    protected final String databaseName;
    protected final AbstractMongoClient mongoMVCCClient;
    protected final DeprecatedMongoClient _depracedMongoClient;
    protected MongoTransactionalContext mongoTransactionalContexct;
    protected ConcurrentMap<String, Abstract_MongoCollectionImpl> dbOpenCollections = new ConcurrentHashMap<> ();

    protected Abstract_MongoDatabaseImpl(long dbID, DeprecatedMongoClient _depracedMongoClient, AbstractMongoClient mongoMVCCClient, String dataBaseName) {
        this.dbID = dbID;
        this._depracedMongoClient = _depracedMongoClient;
        this.mongoMVCCClient = mongoMVCCClient;
        this.databaseName = dataBaseName;
    }
    
    
    /**
     * Returns the name of this database.
     *
     * @return the name
     */
    @Override
    public String getName() {
        return this.databaseName;
    }
    
    /**
     * Returns the name of this database.
     *
     * @return the name
     */
    
    public String getTransactionalName() {
        return this.databaseName + ": " + dbID;
    } 
    
    
    protected void removeCollection(Abstract_MongoCollectionImpl collection) {
        this.dbOpenCollections.remove(collection.getName());
        //this.dbOpenCollections.remove(collection);
    }
    
    
    @Override
    public void close() {
        _close();
        this.mongoMVCCClient.removeDatabase(this.dbID);
    }
    
    
    protected void _close() {
        //do closing things
        
        //close collections
        Iterator<Map.Entry<String, Abstract_MongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
        while(it.hasNext()) {
            it.next().getValue()._close();
            it.remove();
        }
        
        //this.tid=null;
        //this.version=null;
        this.mongoTransactionalContexct = null;
    }
   
    /**
     * @param force is remained only for compatibility reasons
     * @throws MongoException
     */
    public void cleanCursors( boolean force ){
        /*
        Iterator<Map.Entry<String, Abstract_MongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
        while(it.hasNext())
            it.next().getValue().cleanCursors();
        */
    }
    
    
    /**
     * Returns a list of CollectionRecoveryWriteset for recovery. if no collection is open or there's no writeset to apply (RO operations) then it returns an empty list
     *
     * @return list of writesets to recover from this database
     */
    protected List<CollectionRecoveryWriteset> _getWS() {
        List<CollectionRecoveryWriteset> listRecoverWritesets = new ArrayList<>(this.dbOpenCollections.size());
        
        if(this.dbOpenCollections.isEmpty())
            return listRecoverWritesets;
        
        for (Map.Entry entry : this.dbOpenCollections.entrySet()) {
            Abstract_MongoCollectionImpl dBCollection = (Abstract_MongoCollectionImpl) entry.getValue();
            if(dBCollection.writeset.isEmpty()) {
                dBCollection.clearTxnCtx();
                continue;
            }
            
            //here store the ids to be applied in the 2nd phase of the 2PC protocol
            BasicDBList dbList = new BasicDBList();
            //here store (copy) the writeset to recover during the 1st phase of the 2PC protocol
            Map<Object, Bson> writesetToRecover = new HashMap<>(dBCollection.writeset.size());
            Iterator<Map.Entry<Object, Bson>> it = dBCollection.writeset.entrySet().iterator();
            while(it.hasNext()) {
                Map.Entry<Object, Bson> writesetEntry = it.next();
                //add to recover 
                writesetToRecover.put(writesetEntry.getKey(), writesetEntry.getValue());
                dbList.add(writesetEntry.getKey());
                it.remove();
            }
            listRecoverWritesets.add(new CollectionRecoveryWriteset(dBCollection.collectionName, writesetToRecover));
            dBCollection.addToPendingWritesetsToApply(this.mongoTransactionalContexct.getCtxId(), new WriteSetToApply(dBCollection.hasUpdates, dbList));
            
            //and clear the transactional context, so the next operation on the same collection will get the new one
            dBCollection.clearTxnCtx();
        }
        
        return listRecoverWritesets;
    }
    
    
    protected void _unleash(MongoTransactionalContext txnctx) throws TransactionManagerException{
        if(!this.dbOpenCollections.isEmpty()) {
            Iterator iterator = this.dbOpenCollections.entrySet().iterator();
            while(iterator.hasNext()) {

                Abstract_MongoCollectionImpl dbCollection = (Abstract_MongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();

                try {
                    dbCollection.clearTxnCtx();
                } catch(MongoException ex) {
                    throw new TransactionManagerException(ex);
                }
            }
        }
    }
    
    protected void _applyWS(MongoTransactionalContext txnctx) throws TransactionManagerException {
        
        //apply WS
        Iterator iterator = this.dbOpenCollections.entrySet().iterator();
        while(iterator.hasNext()) {
            
            Abstract_MongoCollectionImpl dbCollection = (Abstract_MongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();
            
            try {
                dbCollection._applyWriteSet(txnctx.getCtxId(), txnctx.getCommitTimestamp());
            } catch(MongoException ex) {
                throw new TransactionManagerException(ex);
            }
        }
    }
    
    protected void _rollback(MongoTransactionalContext txnctx) {
        //remove private versions from database
        Iterator iterator = this.dbOpenCollections.entrySet().iterator();
        while(iterator.hasNext()) {            
            Abstract_MongoCollectionImpl dbCollection = (Abstract_MongoCollectionImpl) ((Map.Entry) iterator.next()).getValue();
            dbCollection._rollback(txnctx.getCtxId());
            dbCollection.clearTxnCtx();
        }
        
    }

    @Override
    public String toString() {
        return "AbstractMongoDatabaseImpl{" + "dbID=" + dbID + ", databaseName=" + databaseName + '}';
    }
    
    public abstract void startTransaction(MongoTransactionalContext txnContext);
    
    
    @Override
    public abstract void startTransaction() throws TransactionManagerException;
    
    @Override
    public abstract void commit();
    
    @Override
    public abstract void rollback();
    
}
