package com.mongodb;

import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public class CollectionBean {
    
    protected final Abstract_MongoCollectionImpl collection;
    protected final MongoTransactionalContext mongoTransactionalContext;
    protected final ConcurrentMap<Object, Bson> writeset;

    public CollectionBean(Abstract_MongoCollectionImpl collection, MongoTransactionalContext mongoTransactionalContext, ConcurrentMap<Object, Bson> writeset) {
        this.collection = collection;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this.writeset = writeset;
    }

    public Abstract_MongoCollectionImpl getCollection() {
        return collection;
    }

    public MongoTransactionalContext getMongoTransactionalContext() {
        return mongoTransactionalContext;
    }

    public ConcurrentMap<Object, Bson> getWriteset() {
        return writeset;
    }
    
    
}
