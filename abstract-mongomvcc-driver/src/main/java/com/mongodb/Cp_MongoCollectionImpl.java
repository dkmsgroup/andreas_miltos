package com.mongodb;


import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.FindIterableImpl;
import com.mongodb.MongoException;
import com.mongodb.MongoNamespace;
import com.mongodb.ReadPreference;
import com.mongodb.WriteConcern;
import com.mongodb.WriteResult;
import com.mongodb.binding.ReadBinding;
import com.mongodb.binding.WriteBinding;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.bulk.DeleteRequest;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.ListIndexesIterable;
import com.mongodb.client.MapReduceIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.CountOptions;
import com.mongodb.client.model.DeleteManyModel;
import com.mongodb.client.model.DeleteOneModel;
import com.mongodb.client.model.FindOneAndDeleteOptions;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.IndexModel;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.InsertManyOptions;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.RenameCollectionOptions;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.UpdateManyModel;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.operation.MixedBulkTransactionalWriteOperation;
import com.mongodb.operation.OperationExecutor;
import com.mongodb.operation.ReadOperation;
import com.mongodb.operation.WriteOperation;
import com.mongodb.utils.CpRemover;
import com.mongodb.utils.CpUpdator;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.mongodb.utils.ByteOperators;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import static java.lang.String.format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import org.bson.BsonArray;
import org.bson.BsonBinary;
import org.bson.BsonBinarySubType;
import org.bson.BsonDocument;
import org.bson.BsonDocumentReader;
import org.bson.BsonDocumentWrapper;
import org.bson.BsonInt64;
import org.bson.BsonObjectId;
import org.bson.BsonReader;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.CollectibleCodec;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * 
 * @author idezol
 * @param <TDocument>
 */
public abstract class Cp_MongoCollectionImpl<TDocument> extends Abstract_MongoCollectionImpl implements MongoCollection<TDocument> {
    protected static final Logger Log = LoggerFactory.getLogger(Cp_MongoCollectionImpl.class);
    
    protected final Class<TDocument> documentClass;
    protected MongoCollection<TDocument> _dbDepracedCollection;
    protected final VersionQueryUtils versionQueryUtils = new VersionQueryUtils();
    
    /**
     * constructor of this class
     * sets the collection's name and the DBCollection object of MongoDB driver
     *
     * @param name
     * @param documentClass
     * @param dbDepracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @dochub CpMongoCollectionImpl
     */
    protected Cp_MongoCollectionImpl(final String name, final Class<TDocument> documentClass, final MongoCollection<TDocument> dbDepracedCollection, final MongoTransactionalContext mongoTransactionalContext, final Abstract_MongoDatabaseImpl mongoMVCCDB) { 
        super(mongoMVCCDB, name, mongoTransactionalContext);
        this._dbDepracedCollection = dbDepracedCollection;       
        this.documentClass = documentClass;
    }
    
    protected Cp_MongoCollectionImpl(final String name, final Class<TDocument> documentClass,  final MongoCollection<TDocument> dbDepracedCollection, final MongoTransactionalContext mongoTransactionalContext, final Abstract_MongoDatabaseImpl mongoMVCCDB, ConcurrentMap<Object, Bson> writeset, ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply) { 
        super(mongoMVCCDB, name, mongoTransactionalContext, writeset, pendingWritesetsToApply);
        this._dbDepracedCollection = dbDepracedCollection;       
        this.documentClass = documentClass;
    }
   

    /*
    @Override
    protected ConcurrentMap _getWriteSet() throws MongoException {
        _checkClose();
        
        try {
            _checkTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        try {
            try (MongoCursor<BasicDBObject> cursor = this._dbDepracedCollection.find(VersionQueryUtils.getAllPrivateVersions(this.mongoTransactionalContext.getTid()), BasicDBObject.class).iterator()) {
                while(cursor.hasNext()) {
                    BasicDBObject object = (BasicDBObject) cursor.next();
                    this.writeset.putIfAbsent(object.get(MongoMVCCFields.DATA_ID), object);
                }
            }
        } catch(Exception ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        return this.writeset;
    }
    */
    
    
    @Override
    protected void _applyWriteSet(long tid, long commitTmp) throws MongoException {
        //_checkClose();
        //no check. commit is asyncrhonous and collection might be already closed when ordered to apply the WS
		
        //get the ids of the documents to by 'applied' from the local cache
        WriteSetToApply writeSetToApply = pendingWritesetsToApply.get(tid);
        if((writeSetToApply==null)||(writeSetToApply.getDbList().isEmpty()))
            return; //nothing to apply
                
        
        //FIRST UPDATE THE OLD AND THEN INSERT THE NEW
        //otherwise the new created versions will be updated with the new nextCommitTimestamp       
        BasicDBList dbList = writeSetToApply.getDbList();
        BasicDBObject query;
        BasicDBObject updatePrivate;
        WriteResult writeResult;
        try {
            if(writeSetToApply.isWithUpdates()) {
                //older versions to update
                query = new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                                            .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BasicDBObject("$ne", ""))
                                            .append(MongoMVCCFields.DATA_ID, new BasicDBObject("$in", dbList));
                updatePrivate = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, commitTmp));
                this._dbDepracedCollection.updateMany(query, updatePrivate);
                if(Log.isDebugEnabled())
                    Log.debug("Older versions updated");
            } 
            
            query = VersionQueryUtils.getAllPrivateVersions(tid);
            updatePrivate = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.COMMIT_TIMESTAMP, commitTmp));
            this._dbDepracedCollection.updateMany(query, updatePrivate);
            if(Log.isDebugEnabled())
                Log.info("Newer versions added");
            
            //remove now from the local cache
            pendingWritesetsToApply.remove(tid);
            
            
        } catch(Exception ex) {
            Log.error("Error applying writeset. {}: {}", ex.getClass().getName(), ex.getMessage());
            throw new MongoException(ex.getMessage(), ex);
        }
        
    }

    @Override
    protected void _rollback(long tid) throws MongoException {
        //_checkClose();
        
        
        try {
            BasicDBObject query = VersionQueryUtils.getAllPrivateVersions(tid);
            this._dbDepracedCollection.deleteMany(query);
            if(Log.isDebugEnabled())
                Log.debug("Private versions have been removed");

            //remove now from local cache
            //writeset was not cleared via the getWS, as the getWS was never invoked due to the rollback
            this.writeset.clear();
            
        } catch(Exception ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    
    /**
     * only for loading a workload for the transactional ycsb benchmark
     * 
     * @param tDoc
     */
    public void bulkInsertForYCSB(TDocument tDoc) {
        
        CodecRegistry codecRegistry = getCodecRegistry();
        Codec<TDocument> codec = codecRegistry.get(documentClass);
        BsonDocument document = ((Bson)tDoc).toBsonDocument(documentClass, codecRegistry);
        
        //automatically set the primary key (default type of ObjectId)
        if(document.containsKey(MongoMVCCFields._ID)) {
            //if already contains in _id, put it to its right place
            Object _id = document.get(MongoMVCCFields._ID);
            document.append(MongoMVCCFields.DATA_ID, (BsonValue) _id);
        }  else {
            //otherwise put a new one
            document.append(MongoMVCCFields.DATA_ID, new BsonObjectId(ObjectId.get()));
        }
        
        //add the tid and defualt timestamps
        document.append(MongoMVCCFields.TID, new BsonInt64(0L))
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonInt64(0L))
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));
        
        //add the concatenation of its key+tid
        byte [] _idBytes = ByteOperators.getBytes(document.get(MongoMVCCFields.DATA_ID), 0L);
        document.append(MongoMVCCFields._ID, new BsonBinary(BsonBinarySubType.USER_DEFINED, _idBytes));
        
        BsonReader reader = new BsonDocumentReader(document);
        TDocument otherDocument = codec.decode(reader, null);
        this._dbDepracedCollection.insertOne(otherDocument);
    }
    
    private void _insert(List<? extends TDocument> documents, InsertManyOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        CodecRegistry codecRegistry = getCodecRegistry();
        Codec<TDocument> codec = codecRegistry.get(documentClass);
        
        
        List<TDocument> insertRequests = new ArrayList<>();
        for(TDocument tDoc : documents) {
            if(!(tDoc instanceof Bson))
                throw new IllegalArgumentException("all documents in TDocument list must implement the Bson interface");
            
            
            BsonDocument document = ((Bson)tDoc).toBsonDocument(documentClass, codecRegistry);

            
            //automatically set the primary key (default type of ObjectId)
            if(document.containsKey(MongoMVCCFields._ID)) {
                //if already contains in _id, put it to its right place
                Object _id = document.get(MongoMVCCFields._ID);
                document.append(MongoMVCCFields.DATA_ID, (BsonValue) _id);
            }  else {
                //otherwise put a new one
                document.append(MongoMVCCFields.DATA_ID, new BsonObjectId(ObjectId.get()));
            }
            
            //add the tid and null timestamps
            document.append(MongoMVCCFields.TID, new BsonInt64(this.mongoTransactionalContext.getCtxId()))
                    .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                    .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""));
            
            //add the concatenation of its key+tid
            byte [] _idBytes = ByteOperators.getBytes(document.get(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getCtxId());
            document.append(MongoMVCCFields._ID, new BsonBinary(BsonBinarySubType.USER_DEFINED, _idBytes));
            this.writeset.putIfAbsent(document.get(MongoMVCCFields.DATA_ID), document);
            
            BsonReader reader = new BsonDocumentReader(document);
            TDocument otherDocument = codec.decode(reader, null);
            insertRequests.add(otherDocument);
        }
        
        if(options==null)
            this._dbDepracedCollection.insertMany((List<? extends TDocument>) insertRequests);
        else
            this._dbDepracedCollection.insertMany((List<? extends TDocument>) insertRequests, options);
        
        if(this.isAutoCommit.get())
            autoCommit();
    }
    
    //gets the query parameter, if it is null, then it creates one
    //if a user has aleady provided a _id for searching, then move it to its corresponding place
    //it does not appends the timestamps, but lets the corresponding implementations deal with it
    private BsonDocument fixDataModificationQuery(Bson query) {
        
        BsonDocument bsonDocument;
        if(query==null) ///if no query, then create an empty: it will be needed to append the timestamps related
            bsonDocument = new BsonDocument();
        else {
            CodecRegistry codecRegistry = this._dbDepracedCollection.getCodecRegistry();
            bsonDocument = query.toBsonDocument(documentClass, codecRegistry);
            //check if user has provided a find on the identifier, if so, then move the _id to _dataID
            if(bsonDocument.containsKey(MongoMVCCFields._ID)) {
                BsonValue _id = bsonDocument.get(MongoMVCCFields._ID);
                bsonDocument.put(MongoMVCCFields.DATA_ID, _id);
                bsonDocument.remove(MongoMVCCFields._ID);
            }
        }
        
        
        return bsonDocument;
    }
    
    //private long totalTime = 0;
    //private int totalOperations = 0;
    
    private Bson fixSelectQuery(Bson query) {
        
        //long startTimp = System.nanoTime();
        
        BsonDocument bsonDocument = fixDataModificationQuery(query);
        
        List<BsonValue> listQuerys = new ArrayList<>(2);
        if(!this.writeset.isEmpty())
            listQuerys.add(VersionQueryUtils.getQueryPrivateVersion(BsonDocument.parse(bsonDocument.toJson()), mongoTransactionalContext.getCtxId(), this._dbDepracedCollection.getCodecRegistry()));
        listQuerys.add(versionQueryUtils.getQueryVersion(BsonDocument.parse(bsonDocument.toJson()), mongoTransactionalContext.getStartTimestamp()));
        bsonDocument = new BsonDocument("$or", new BsonArray(listQuerys));  
        
        //long endTmpe = System.nanoTime();
        //Log.info("Total time to fix the query: {}", (endTmpe-startTimp));
        /*
        totalOperations++;
        if(totalOperations>1000) {
            Log.info("Average time: {}", totalTime/(totalOperations-1000));
            totalTime = totalTime + (endTmpe-startTimp);
        }
        else 
            Log.info("total operations: {}", totalOperations);
        */
        
        return bsonDocument;
    }
    
    private <TResult> FindIterable<TResult> _find(Bson filter, Class<TResult> resultClass) {
        
        //Log.info("Total cursors at least: " + this.clientCursors.size());
        
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(filter==null)
            throw new IllegalArgumentException("filter cannot be null");
        
        
        filter = fixSelectQuery(filter);
        FindIterable<TResult> iteraBle = this._dbDepracedCollection.find(filter, resultClass);
        
        CpFindIterableImpl<TDocument, TResult> result;
        
        if(!this.isAutoCommit.get())
            result = new CpFindIterableImpl<>(documentClass, resultClass, (FindIterableImpl) iteraBle, new CollectionBean(this, mongoTransactionalContext, writeset), filter, this._dbDepracedCollection.getCodecRegistry());
        else
            result = new CpFindIterableImpl<>(documentClass, resultClass, (FindIterableImpl) iteraBle, new CollectionBean(this, mongoTransactionalContext, null), filter, this._dbDepracedCollection.getCodecRegistry());
        
        if(this.isAutoCommit.get()) 
            autoCommit();
        
        return result;
    }

    private long _count(Bson filter, CountOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(filter==null)
            throw new IllegalArgumentException("filter cannot be null");
        
        filter = fixSelectQuery(filter);
        
        long result = this._dbDepracedCollection.count(filter, options);
        
        /*
        long result = 0;
        long limit = options.getLimit();
        long skip = options.getSkip();
        try (MongoCursor<TDocument> cursor = this._dbDepracedCollection.find(filter, documentClass).iterator()) {
            while(cursor.hasNext()) {
                TDocument tDocFound = cursor.next();
                if(!(tDocFound instanceof Bson)) 
                    throw new IllegalStateException("retrieved document is not in a valid Bson type");
                
                BsonDocument bsonFound = ((Bson) tDocFound).toBsonDocument(documentClass, this._dbDepracedCollection.getCodecRegistry());
                //if document is already in the writeset, then check if this version is outdated or current (private)
                if(this.writeset.containsKey(bsonFound.get(MongoMVCCFields.DATA_ID))) {
                    if(bsonFound.getInt64(MongoMVCCFields.TID).getValue()==this.mongoTransactionalContext.getTid()) 
                        result++;
                    else 
                        //there's an updated version for this document, do not count it
                        continue;
                }
                else
                    //there is no updated version for this document
                    result++;
                
                //skip the first 'skip'
                if(skip>0) {
                    result--;
                    skip--;
                }
                
                //find until the limit
                if(result==limit)
                    break;
            }
        }        
        */
        if(this.isAutoCommit.get())
            autoCommit();
        
        return result;        
    }
    
    private <TResult> DistinctIterable<TResult> _distinct(String fieldName, Bson filter, Class<TResult> resultClass) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(filter==null)
            throw new IllegalArgumentException("filter cannot be null");
        
        if((fieldName==null)||fieldName.equals(""))
            throw new MongoException("fieldName does not contain any value");
        
        
        filter = fixSelectQuery(filter); 
        FindIterable<TResult> iteraBle = this._dbDepracedCollection.find(filter, resultClass);
        
        
        CpDistinctIterableImpl<TDocument, TResult> result;
        if(!this.isAutoCommit.get())
            result = new CpDistinctIterableImpl<>(documentClass, resultClass, (FindIterableImpl) iteraBle, new CollectionBean(this, mongoTransactionalContext, writeset), filter, this._dbDepracedCollection.getCodecRegistry(), fieldName);
        else
            result = new CpDistinctIterableImpl<>(documentClass, resultClass, (FindIterableImpl) iteraBle, new CollectionBean(this, mongoTransactionalContext, null), filter, this._dbDepracedCollection.getCodecRegistry(), fieldName);
        
        if(this.isAutoCommit.get()) 
            autoCommit();
        
        return result;
    }
    
    
    
    private DeleteResult _delete(Bson filter, boolean removeOne) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        filter = fixDataModificationQuery(filter);
        
        CpRemover remover = new CpRemover(documentClass, writeset, filter, this.mongoTransactionalContext, this._dbDepracedCollection, getCodecRegistry(), removeOne, null, versionQueryUtils);
        
        DeleteResult deleteResult = null;
        try {
            deleteResult = remover.remove();
        } catch (TransactionManagerException | MongoMVCCException | TransactionalMongoException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return deleteResult;
    }
    
    private TDocument _delete(Bson filter, FindOneAndDeleteOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        filter = fixDataModificationQuery(filter);
        
        Bson sort = null;
        if(options!=null)
            sort = options.getSort();
        
        CpRemover remover = new CpRemover(documentClass, writeset, filter, this.mongoTransactionalContext, this._dbDepracedCollection, getCodecRegistry(), true, sort, versionQueryUtils);
        
        Object result = null;
        try {
            result = remover.findAndRemove();
        } catch (TransactionManagerException | MongoMVCCException | TransactionalMongoException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        if(result!=null)
            return (TDocument) result;
        return null;
    }
    
    private TDocument _update(Bson filter, Bson update, FindOneAndUpdateOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
               
        if(update==null)
            throw new IllegalAccessError("update/replace parameter cannot be null");
        
         if(options==null)
            options = new FindOneAndUpdateOptions();
        
        filter = fixDataModificationQuery(filter);
        
        CpUpdator updator = new CpUpdator(writeset, filter, this.mongoTransactionalContext, _dbDepracedCollection, documentClass, getCodecRegistry(), false, versionQueryUtils);
        
        Object result = null;
        try {
            result = updator.findAndupdateSet(update, options);
        } catch (TransactionManagerException | MongoMVCCException | TransactionalMongoException  ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        if(result!=null)
            return (TDocument) result;
        return null;  
    }
    
    
    private TDocument _update(Bson filter, TDocument update, FindOneAndReplaceOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(update==null)
            throw new IllegalAccessError("update/replace parameter cannot be null");
        
        
         if(options==null)
            options = new FindOneAndReplaceOptions();
        
        filter = fixDataModificationQuery(filter);
        
        CpUpdator updator = new CpUpdator(writeset, filter, this.mongoTransactionalContext, _dbDepracedCollection, documentClass, getCodecRegistry(), false, versionQueryUtils);
        
        Object result = null;
        try {
            result = updator.findAndReplace(update, options);
        } catch (TransactionManagerException | MongoMVCCException | TransactionalMongoException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        if(result!=null)
            return (TDocument) result;
        return null;  
    }
    
   
    private UpdateResult _update(Bson filter, Bson update, TDocument replaceDocument, boolean replace, boolean updateOne, UpdateOptions options) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        if((update==null)&&(replaceDocument==null))
            throw new IllegalAccessError("update/replace parameter cannot be null");
        
        if(options==null)
            options = new UpdateOptions();
        
        filter = fixDataModificationQuery(filter);
        
        CpUpdator updator = new CpUpdator(writeset, filter, this.mongoTransactionalContext, _dbDepracedCollection, documentClass, getCodecRegistry(), !updateOne, versionQueryUtils);
        UpdateResult updateResult = null;
        
        try {
            if(!replace)
                updateResult = updator.updateSet(update, options);
            else
                updateResult = updator.updateReplace(replaceDocument, options);
        } catch (TransactionManagerException | MongoMVCCException | TransactionalMongoException ex) {
            throw new MongoException(ex.getMessage(), ex);           
        }
        
        if(!this.hasUpdates)
            this.hasUpdates = true;
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        
        return updateResult;
    }
    
    
    @Override
    public MongoNamespace getNamespace() {
        _checkClose();
        return this._dbDepracedCollection.getNamespace();
    }

    @Override
    public Class<TDocument> getDocumentClass() {
        _checkClose();
        return this._dbDepracedCollection.getDocumentClass();
    }

    @Override
    public CodecRegistry getCodecRegistry() {
        _checkClose();
        return this._dbDepracedCollection.getCodecRegistry();
    }

    @Override
    public ReadPreference getReadPreference() {
        _checkClose();
        return this._dbDepracedCollection.getReadPreference();
    }

    @Override
    public WriteConcern getWriteConcern() {
        _checkClose();
        return this._dbDepracedCollection.getWriteConcern();
    }

    @Override
    public <NewTDocument> MongoCollection<NewTDocument> withDocumentClass(Class<NewTDocument> clazz) {
        _checkClose();
        this._dbDepracedCollection = (MongoCollection<TDocument>) this._dbDepracedCollection.withDocumentClass(clazz);
        return (MongoCollection<NewTDocument>) this;
    }

    @Override
    public MongoCollection<TDocument> withCodecRegistry(CodecRegistry codecRegistry) {
        _checkClose();
        this._dbDepracedCollection = this._dbDepracedCollection.withCodecRegistry(codecRegistry);
        return this;
    }

    @Override
    public MongoCollection<TDocument> withReadPreference(ReadPreference readPreference) {
        _checkClose();
        this._dbDepracedCollection = this._dbDepracedCollection.withReadPreference(readPreference);
        return this;
    }

    @Override
    public MongoCollection<TDocument> withWriteConcern(WriteConcern writeConcern) {
        _checkClose();
        this._dbDepracedCollection = this._dbDepracedCollection.withWriteConcern(writeConcern);
        return this;
    }

    @Override
    public void drop(){ 
         _checkClose();
        this._dbDepracedCollection.drop();
        close();
    }

    
    @Override
    public String createIndex(Bson keys) {
        _checkClose();
        return this._dbDepracedCollection.createIndex(keys);
    }

    @Override
    public String createIndex(Bson keys, IndexOptions indexOptions) {
        _checkClose();
        return this._dbDepracedCollection.createIndex(keys, indexOptions);
    }

    @Override
    public List<String> createIndexes(List<IndexModel> indexes) {
        _checkClose();
        return this._dbDepracedCollection.createIndexes(indexes);
    }

    @Override
    public ListIndexesIterable<Document> listIndexes() {
        _checkClose();
        return this._dbDepracedCollection.listIndexes();
    }

    @Override
    public <TResult> ListIndexesIterable<TResult> listIndexes(Class<TResult> resultClass) {
        _checkClose();
        return this._dbDepracedCollection.listIndexes(resultClass);
    }

    @Override
    public void dropIndex(String indexName) {
        _checkClose();
        this._dbDepracedCollection.dropIndex(indexName);
    }

    @Override
    public void dropIndex(Bson keys) {
        _checkClose();
        this._dbDepracedCollection.dropIndex(keys);
    }

    @Override
    public void dropIndexes() {
        _checkClose();
        ListIndexesIterable<Document> indexes = this._dbDepracedCollection.listIndexes();
        for(Document index : indexes) {
            if(!index.containsKey(MongoMVCCFields.DEFAULT_COMPOUND_INDEX))
                this._dbDepracedCollection.dropIndex(index);
        }
    }
    

    @Override
    public long count() {
        return count(new BsonDocument(), new CountOptions());
    }

    @Override
    public long count(Bson filter) {
        return count(filter, new CountOptions());
    }

    @Override
    public long count(Bson filter, CountOptions options) {
        return _count(filter, options);
    }
    
    
    

    @Override
    public <TResult> DistinctIterable<TResult> distinct(String fieldName, Class<TResult> resultClass) {
        return distinct(fieldName, new BsonDocument(), resultClass);
    }

    @Override
    public <TResult> DistinctIterable<TResult> distinct(String fieldName, Bson filter, Class<TResult> resultClass) {
        return _distinct(fieldName, filter, resultClass);
    }
    
    
    

    @Override
    public FindIterable<TDocument> find() {
        return find(new BsonDocument(), documentClass);
    }

    @Override
    public <TResult> FindIterable<TResult> find(Class<TResult> resultClass) {
        return find(new BsonDocument(), resultClass);
    }

    @Override
    public FindIterable<TDocument> find(Bson filter) {
        return find(filter, documentClass);
    }

    @Override
    public <TResult> FindIterable<TResult> find(Bson filter, Class<TResult> resultClass) {
        return _find(filter, resultClass);
    }

    
    
    @Override
    public AggregateIterable<TDocument> aggregate(List<? extends Bson> pipeline) {
        return aggregate(pipeline, documentClass);
    }

    @Override
    public <TResult> AggregateIterable<TResult> aggregate(List<? extends Bson> pipeline, Class<TResult> resultClass) {
        try {
            _checkClose();
            _getTransactionalContext();
        } catch (CoherentPaaSException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
        List<? extends Bson> list = fixPipeline(pipeline, getCodecRegistry());
        
        if(this.isAutoCommit.get())
            autoCommit();
        
        return this._dbDepracedCollection.aggregate(list, resultClass);
    }
    
    
    private List<? extends Bson> fixPipeline(List<? extends Bson> pipeline, CodecRegistry codecRegistry) {
        List<Bson> fixPipeLineList = new ArrayList<>();
        boolean findMatch = false;
        for(Bson bson : pipeline) {
            BsonDocument bsonDoc = bson.toBsonDocument(documentClass, codecRegistry);
            Iterator<String> it = bsonDoc.keySet().iterator();
            while(it.hasNext()) {
                String key = it.next();
                
                if(key.equals("$match")) {
                    findMatch = true;
                    
                    Bson bsonValue = fixMatchPipeline((Bson) bsonDoc.get(key), codecRegistry);
                    
                    BsonDocument matchToAdd = new BsonDocument("$match", (BsonValue) bsonValue);
                    fixPipeLineList.add(matchToAdd);
                    break;
                } else {
                    fixPipeLineList.add(bson);
                    break;
                }
            }
        }
        
        if(!findMatch) {
            fixPipeLineList.add(0, new BsonDocument("$match", (BsonValue) fixMatchPipeline(null, codecRegistry)));
        }
        
        return fixPipeLineList;
    }

    
    private Bson fixMatchPipeline(Bson bson, CodecRegistry codecRegistry) {
        
        BsonDocument newMatch;
        if(bson==null)
            newMatch = new BsonDocument();
        else
            newMatch = ((Bson)bson).toBsonDocument(documentClass, codecRegistry);
        
        newMatch = versionQueryUtils.getQueryVersion(newMatch, mongoTransactionalContext.getStartTimestamp());
        
        return newMatch;
    }
    
    
    @Override
    @Deprecated
    public MapReduceIterable<TDocument> mapReduce(String mapFunction, String reduceFunction) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Deprecated
    public <TResult> MapReduceIterable<TResult> mapReduce(String mapFunction, String reduceFunction, Class<TResult> resultClass) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    
    @Override
    public void insertOne(final TDocument document) {
        _insert(Arrays.asList(document), null);
    }

    @Override
    public void insertMany(List<? extends TDocument> documents) {
        _insert(documents, null);
    }

    @Override
    public void insertMany(List<? extends TDocument> documents, InsertManyOptions options) {
        _insert(documents, options);
    }
    
    

    @Override
    public DeleteResult deleteOne(Bson filter) {
        return _delete(filter, true);
    }

    @Override
    public DeleteResult deleteMany(Bson filter) {
        return _delete(filter, false);
    }
    
    
    

    @Override
    public UpdateResult replaceOne(Bson filter, TDocument replacement) {
        return _update(filter, null, replacement, true, true, new UpdateOptions());
    }

    @Override
    public UpdateResult replaceOne(Bson filter, TDocument replacement, UpdateOptions updateOptions) {
        return _update(filter, null, replacement, true, true, updateOptions);
    }

    @Override
    public UpdateResult updateOne(Bson filter, Bson update) {
        return _update(filter, update, null, false, true, new UpdateOptions());
    }

    @Override
    public UpdateResult updateOne(Bson filter, Bson update, UpdateOptions updateOptions) {
        return _update(filter, update, null, false, true, updateOptions);
    }

    @Override
    public UpdateResult updateMany(Bson filter, Bson update) {
        return _update(filter, update, null, false, false, new UpdateOptions());
    }

    @Override
    public UpdateResult updateMany(Bson filter, Bson update, UpdateOptions updateOptions) {
        return _update(filter, update, null, false, false, updateOptions);
    }

    @Override
    public TDocument findOneAndDelete(Bson filter) {
        return _delete(filter, null);
    }

    @Override
    public TDocument findOneAndDelete(Bson filter, FindOneAndDeleteOptions options) {
        return _delete(filter, options);
    }

    @Override
    public TDocument findOneAndReplace(Bson filter, TDocument replacement) {
        return _update(filter, replacement, new FindOneAndReplaceOptions());
    }

    @Override
    public TDocument findOneAndReplace(Bson filter, TDocument replacement, FindOneAndReplaceOptions options) {
        return _update(filter, replacement, options);
    }

    @Override
    public TDocument findOneAndUpdate(Bson filter, Bson update) {
        return _update(filter, update, new FindOneAndUpdateOptions());
    }

    @Override
    public TDocument findOneAndUpdate(Bson filter, Bson update, FindOneAndUpdateOptions options) {
        return _update(filter, update, options);
    }

    
    @Override
    @Deprecated
    public void renameCollection(MongoNamespace newCollectionNamespace) {
        throw new UnsupportedOperationException(("cannot rename collection. collection's name is stored at logs"));
    }

    @Deprecated
    @Override
    public void renameCollection(MongoNamespace newCollectionNamespace, RenameCollectionOptions renameCollectionOptions) {
        throw new UnsupportedOperationException(("cannot rename collection. collection's name is stored at logs"));
    }

    
    private Codec<TDocument> getCodec() {
        return  this._dbDepracedCollection.getCodecRegistry().get(documentClass);
    }
    
    @Override
    public BulkWriteResult bulkWrite(List<? extends WriteModel<? extends TDocument>> requests) {
        return bulkWrite(requests, new BulkWriteOptions());
    }

    @Override
    public BulkWriteResult bulkWrite(List<? extends WriteModel<? extends TDocument>> requests, BulkWriteOptions options) {
        List<com.mongodb.bulk.WriteRequest> writeRequests = new ArrayList<>(requests.size());
        for (WriteModel<? extends TDocument> writeModel : requests) {
            com.mongodb.bulk.WriteRequest writeRequest;
            if (writeModel instanceof InsertOneModel) {
                TDocument document = ((InsertOneModel<TDocument>) writeModel).getDocument();
                if (getCodec() instanceof CollectibleCodec) {
                    document = ((CollectibleCodec<TDocument>) getCodec()).generateIdIfAbsentFromDocument(document);
                }
                writeRequest = new com.mongodb.bulk.InsertRequest(documentToBsonDocument(document));
            } else if (writeModel instanceof ReplaceOneModel) {
                ReplaceOneModel<TDocument> replaceOneModel = (ReplaceOneModel<TDocument>) writeModel;
                writeRequest = new com.mongodb.bulk.UpdateRequest(toBsonDocument(replaceOneModel.getFilter()), documentToBsonDocument(replaceOneModel
                                                                                                                     .getReplacement()),
                                                 com.mongodb.bulk.WriteRequest.Type.REPLACE)
                                   .upsert(replaceOneModel.getOptions().isUpsert());
            } else if (writeModel instanceof UpdateOneModel) {
                UpdateOneModel<TDocument> updateOneModel = (UpdateOneModel<TDocument>) writeModel;
                writeRequest = new com.mongodb.bulk.UpdateRequest(toBsonDocument(updateOneModel.getFilter()), toBsonDocument(updateOneModel.getUpdate()),
                                                 com.mongodb.bulk.WriteRequest.Type.UPDATE)
                                   .multi(false)
                                   .upsert(updateOneModel.getOptions().isUpsert());
            } else if (writeModel instanceof UpdateManyModel) {
                UpdateManyModel<TDocument> updateManyModel = (UpdateManyModel<TDocument>) writeModel;
                writeRequest = new com.mongodb.bulk.UpdateRequest(toBsonDocument(updateManyModel.getFilter()), toBsonDocument(updateManyModel.getUpdate()),
                                                 com.mongodb.bulk.WriteRequest.Type.UPDATE)
                                   .multi(true)
                                   .upsert(updateManyModel.getOptions().isUpsert());
            } else if (writeModel instanceof DeleteOneModel) {
                DeleteOneModel<TDocument> deleteOneModel = (DeleteOneModel<TDocument>) writeModel;
                writeRequest = new DeleteRequest(toBsonDocument(deleteOneModel.getFilter())).multi(false);
            } else if (writeModel instanceof DeleteManyModel) {
                DeleteManyModel<TDocument> deleteManyModel = (DeleteManyModel<TDocument>) writeModel;
                writeRequest = new DeleteRequest(toBsonDocument(deleteManyModel.getFilter())).multi(true);
            } else {
                throw new UnsupportedOperationException(format("WriteModel of type %s is not supported", writeModel.getClass()));
            }

            writeRequests.add(writeRequest);
        }
        
        return createOperationExecutor().execute(new MixedBulkTransactionalWriteOperation(this._dbDepracedCollection.getNamespace(), writeRequests, options.isOrdered(), this._dbDepracedCollection.getWriteConcern(), this));
    }
    
    
    
    OperationExecutor createOperationExecutor() {
        return new OperationExecutor() {
            @Override
            public <T> T execute(final ReadOperation<T> operation, final ReadPreference readPreference) {
                return Cp_MongoCollectionImpl.this.execute(operation, readPreference);
            }

            @Override
            public <T> T execute(final WriteOperation<T> operation) {
                return Cp_MongoCollectionImpl.this.execute(operation);
            }
        };
    }
    
    <T> T execute(final ReadOperation<T> operation, final ReadPreference readPreference) {
        ReadBinding binding = getReadBinding(readPreference);
        try {
            return operation.execute(binding);
        } finally {
            binding.release();
        }
    }
    
    
    
    <T> T execute(final WriteOperation<T> operation) {
        WriteBinding binding = getWriteBinding();
        try {
            return operation.execute(binding);
        } finally {
            binding.release();
        }
    }
    
    
    
    private BsonDocument documentToBsonDocument(final TDocument document) {
        return BsonDocumentWrapper.asBsonDocument(document, getCodecRegistry());
    }
    
    private BsonDocument toBsonDocument(final Bson bson) {
        return bson == null ? null : bson.toBsonDocument(documentClass, getCodecRegistry());
    }
    
    
}
