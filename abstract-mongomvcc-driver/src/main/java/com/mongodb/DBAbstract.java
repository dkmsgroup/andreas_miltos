package com.mongodb;

//import com.mongodb.DBApiLayer;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public abstract class DBAbstract extends Abstract_MongoDatabaseImpl implements IDB {
    protected static final Logger Log = LoggerFactory.getLogger(DBAbstract.class);
    
    protected final DeprecatedDB _db;
    
    protected DBAbstract(long dbID, String databaseName, DeprecatedMongoClient _depracedMongoClient, AbstractMongoClient mongoClient, DeprecatedDB deprecatedDb) {
        super(dbID, _depracedMongoClient, mongoClient, databaseName);
        this._db = deprecatedDb;
    }
    
    
    
    public DeprecatedDB getDepracedDB() {
        return (DeprecatedDB) this._db;
    }
    
    
   
    /**
     * Adds the given flag to the default query options.
     *
     * @param option value to be added
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public void addOption( int option ){
        this._db.addOption(option);
    }
    
    
    /**
     * Sets the default query options, overwriting previous value.
     *
     * @param options bit vector of query options
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public void setOptions( int options ){
        this._db.setOptions( options );
    }

    /**
     * Resets the query options.
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public void resetOptions(){
        this._db.resetOptions();
    }

    /**
     * Gets the default query options
     *
     * @return bit vector of query options
     * @mongodb.driver.manual ../meta-driver/latest/legacy/mongodb-wire-protocol/#op-query Query Flags
     */
    @Override
    public int getOptions(){
        return this._db.getOptions();
    }
    
    /**
     * Helper method for calling a 'dbStats' command. It returns storage statistics for a given database.
     *
     * @return result of the execution
     * @throws MongoException
     * @mongodb.driver.manual reference/command/dbStats/ Database Stats
     */
    @Override
    public CommandResult getStats() {
        return this._db.getStats();
    }
    
    
    /**
     * Checks to see if a collection with a given name exists on a server.
     *
     * @param collectionName a name of the collection to test for existence
     * @return {@code false} if no collection by that name exists, {@code true} if a match to an existing collection was found
     * @throws MongoException
     */
    @Override
    public boolean collectionExists(String collectionName) {
        return this._db.collectionExists(collectionName);
    }
    
    
    /**
     * Returns a set containing the names of all collections in this database.
     *
     * @return the names of collections in this database
     * @throws MongoException
     * @mongodb.driver.manual reference/method/db.getCollectionNames/ getCollectionNames()
     */
    @Override
    public Set<String> getCollectionNames() {
        return _db.getCollectionNames();
    }
    
    /**
     * Returns the name of this database.
     *
     * @return the name
     */
    @Override
    public String toString(){
        return this.databaseName;
    }
    
    
    /**
     * Sets the write concern for this database. It will be used for write operations to any collection in this database. See the
     * documentation for {@link WriteConcern} for more information.
     *
     * @param writeConcern {@code WriteConcern} to use
     * @mongodb.driver.manual core/write-concern/ Write Concern
     */
    @Override
    public void setWriteConcern(final WriteConcern writeConcern) {
        this._db.setWriteConcern(writeConcern);
    }
    
    
    /**
     * Gets the write concern for this database.
     *
     * @return {@code WriteConcern} to be used for write operations, if not specified explicitly
     * @mongodb.driver.manual core/write-concern/ Write Concern
     */
    @Override
    public WriteConcern getWriteConcern(){
        return this._db.getWriteConcern();
    }
    
    
    /**
     * Sets the read preference for this database. Will be used as default for read operations from any collection in this database. See the
     * documentation for {@link ReadPreference} for more information.
     *
     * @param readPreference {@code ReadPreference} to use
     * @mongodb.driver.manual core/read-preference/ Read Preference
     */
    @Override
    public void setReadPreference(final ReadPreference readPreference) {
        this._db.setReadPreference(readPreference);
    }
    
    
    /**
     * Gets the read preference for this database.
     *
     * @return {@code ReadPreference} to be used for read operations, if not specified explicitly
     * @mongodb.driver.manual core/read-preference/ Read Preference
     */
    @Override
    public ReadPreference getReadPreference(){
        return this._db.getReadPreference();
    }
    
    
    /**
     * Gets the Mongo instance
     *
     * @return the mongo instance that this database was created from.
     */
    public IMongoClient getMongo(){ 
        return this.mongoMVCCClient;
    }
    
    
    /**
     * Gets another database on same server
     *
     * @param name name of the database
     * @return the DBAbstract for the given name
     */
    @Override
    public DBAbstract getSisterDB( String name ){
        throw new UnsupportedOperationException("Cannot support sister db on the same transaction");
    }
    
    
    
    
    @Override
    public abstract DBCollectionAbstract getCollection( String name );
    
    @Override
    public abstract DBCollectionAbstract createCollection(final String name, final DBObject options);
    
    public abstract DBCollectionAbstract createCollectionForWrapper(final String name);
    
    
    /**
     * Returns a collection matching a given string.
     *
     * @param s the name of the collection
     * @return the collection
     */
    @Override
    public DBCollectionAbstract getCollectionFromString(String s) {
        
        /*
        DepracedDBCollection foo = null;

        int idx = s.indexOf( "." );
        while ( idx >= 0 ){
            String b = s.substring( 0 , idx );
            s = s.substring( idx + 1 );
            if ( foo == null )
                foo = getCollection( b );
            else
                foo = foo.getCollection( b );
            idx = s.indexOf( "." );
        }

        if ( foo != null )
            return foo.getCollection( s );
        return getCollection( s );
         * 
         */
        
        throw new UnsupportedOperationException();
    }
    
    
    /**
     * Executes a database command. This method calls {@link DB#command(DBObject, ReadPreference) } with the default read preference for the
     * database.
     *
     * @param cmd {@code DBAbstractObject} representation of the command to be executed
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     */
    @Override
    public CommandResult command(DBObject cmd) {
        return _db.command(cmd);
    }

    
    /**
     * Executes a database command. This method calls {@link DB#command(DBObject, ReadPreference, DBEncoder) } with the default read
     * preference for the database.
     *
     * @param cmd     {@code DBAbstractObject} representation of the command to be executed
     * @param encoder {@link DBEncoder} to be used for command encoding
     * @return result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     */
    @Override
    public CommandResult command(DBObject cmd, DBEncoder encoder) {
        return _db.command(cmd, encoder);
    }

    
    
    /**
     * Executes a database command with the selected readPreference, and encodes the command using the given encoder.
     *
     * @param cmd            The {@code DBAbstractObject} representation the command to be executed
     * @param readPreference Where to execute the command - this will only be applied for a subset of commands
     * @param encoder        The DBAbstractEncoder that knows how to serialise the command
     * @return The result of executing the command, success or failure
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     * @since 2.12
     */
    @Override
    public CommandResult command(DBObject cmd, ReadPreference readPreference, DBEncoder encoder) {
        return _db.command(cmd, readPreference, encoder);
    }

    
    /**
     * Executes the command against the database with the given read preference.  This method is the preferred way of setting read
     * preference, use this instead of {@link #command(com.mongodb.DBObject, int) }
     *
     * @param cmd            The {@code DBAbstractObject} representation the command to be executed
     * @param readPreference Where to execute the command - this will only be applied for a subset of commands
     * @return The result of executing the command, success or failure
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     * @since 2.12
     */
    @Override
    public CommandResult command(DBObject cmd, ReadPreference readPreference) {
        return _db.command(cmd, readPreference);
    }

    
    /**
     * Executes a database command. This method constructs a simple DBAbstractObject using {@code command} as the field name and {@code true} as its
     * value, and calls {@link #command(DBObject, ReadPreference) } with the default read preference for the database.
     *
     * @param cmd command to execute
     * @return result of command from the database
     * @throws MongoException
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     */
    @Override
    public CommandResult command(String cmd) {
        return _db.command(cmd);
    }

    
    /**
     * Executes a database command. This method constructs a simple {@link DBObject} and calls {@link #command(DBObject, ReadPreference) }.
     *
     * @param cmd            The name of the command to be executed
     * @param readPreference Where to execute the command - this will only be applied for a subset of commands
     * @return The result of the command execution
     * @throws MongoException
     * @mongodb.driver.manual tutorial/use-database-commands Commands
     * @since 2.12
     */
    @Override
    public CommandResult command(String cmd, ReadPreference readPreference) {
        return _db.command(cmd, readPreference);
    }

    
    @Deprecated
    @Override
    public CommandResult doEval(String code, Object... args) {
        throw new UnsupportedOperationException("no javascript code is supported yet");
    }

    @Deprecated
    @Override
    public Object eval(String code, Object... args) {
        throw new UnsupportedOperationException("no javascript code is supported yet");
    }

    
    


    @Deprecated
    @Override
    public WriteResult addUser(String username, char[] passwd, boolean readOnly) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Deprecated
    @Override
    public WriteResult removeUser(String username) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    
    
    /**
     * Drops this database. Removes all data on disk. Use with caution.
     *
     * @throws MongoException
     * @mongodb.driver.manual reference/command/dropDatabase/ Drop Database
     */
    @Override
    public void dropDatabase(){
        _close();
        this._db.dropDatabase();        
    }

}