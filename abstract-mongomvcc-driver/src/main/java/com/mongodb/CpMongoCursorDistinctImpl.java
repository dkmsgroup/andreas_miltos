package com.mongodb;

import com.mongodb.client.MongoCursor;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import java.util.ArrayList;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 * @param <T>
 */
public class CpMongoCursorDistinctImpl<T extends Bson> extends CpMongoCursorImpl<T> {
    private static final Logger Log = LoggerFactory.getLogger(CpMongoCursorDistinctImpl.class);
    private final String fieldName;
    protected final ArrayList<BsonValue> foundFields;

 
    
    protected CpMongoCursorDistinctImpl(MongoCursor<T> mongoCursor, Class<T> resultClass, CollectionBean dbCollection, CodecRegistry codecRegistry, int skip, int limit, String fieldName) {
        super(mongoCursor, resultClass, dbCollection, codecRegistry, skip, limit);
        this.fieldName = fieldName;
        this.foundFields = new ArrayList<>();
    }
    
    @Override
    protected boolean _hasNext() {
        
        //chech if there is a nextObj
        if(_nextObj!=null)
            return true;
        
        //check if already has scanned more than the limit
        if((limit>0)&&(totalScanned>=limit))
            return false;
        
        //long startRead = System.nanoTime();
        while(this.mongoCursor.hasNext()) {
            T documentFound = this.mongoCursor.next();
            
            if(!(documentFound instanceof Bson)) 
                throw new IllegalStateException("retrieved document is not in a valid Bson type");
            
            BsonDocument bsonFound = ((Bson) documentFound).toBsonDocument(resultClass, codecRegistry);
            
            
            //if distinct is set and record is already found, then continue
            if(this.fieldName!=null) {
                if(bsonFound.containsKey(this.fieldName)) {
                    if(this.foundFields.contains(bsonFound.get(this.fieldName)))
                        continue;
                }
            }
            
            //if document is already in the writeset, then check if this version is outdated or current (private)
            //if((this.writeset!=null) && (this.writeset.containsKey(bsonFound.getObjectId(MongoMVCCFields.DATA_ID).getValue()))) {
            if((this.writeset!=null) && (this.writeset.containsKey(bsonFound.get(MongoMVCCFields.DATA_ID)))) {
                
                if(bsonFound.getInt64(MongoMVCCFields.TID).getValue()==this.mongoTransactionalContext.getCtxId()) {
                    //discard the first 'skip' documents
                    if(skip>0) {
                        skip--;
                        continue;
                    }
                    
                    //_nextObj = documentFound;
                    _nextObj = fixIdentifier(documentFound);
                    totalScanned++;
                    
                    //if distinct is set, then put to the already found list
                    if(this.fieldName!=null) 
                        this.foundFields.add(bsonFound.get(this.fieldName));
                    
                    return true;
                } else {
                    //there's an updated version for this document, do not count it
                    //continue;
                }
            } else {
                
                //document is from the public versions --no updated version found in the private writeset
                //discard the first 'skip' documents
                if(skip>0) {
                    skip--;
                    continue;
                }
                
                
                //_nextObj = documentFound;
                _nextObj = fixIdentifier(documentFound);
                totalScanned++;
                
                //if distinct is set, then put to the already found list
                if(this.fieldName!=null) 
                    this.foundFields.add(bsonFound.get(this.fieldName));
                     
                return true;
            }
        }
        
        return false;
    }
}
