package com.mongodb;

import com.mongodb.Block;
import com.mongodb.FindIterableImpl;
import com.mongodb.Function;
import com.mongodb.MappingIterable;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 * @param <TDocument>
 * @param <TResult>
 */
final class CpDistinctIterableImpl<TDocument, TResult> implements DistinctIterable<TResult>  {

    private final Class<TDocument> documentClass;
    private final Class<TResult> resultClass;
    private final FindIterableImpl findIterable;
    private final CollectionBean cpCollection;
    private final CodecRegistry codecRegistry;
    private final String fieldName;
    private Bson filter;
    
    protected CpDistinctIterableImpl(Class<TDocument> documentClass, Class<TResult> resultClass, FindIterableImpl findIterable, CollectionBean cpCollection, Bson filter, CodecRegistry codecRegistry, String fieldName) {
        this.documentClass = documentClass;
        this.resultClass = resultClass;
        this.findIterable = findIterable;
        this.cpCollection = cpCollection;
        this.codecRegistry = codecRegistry;
        this.filter = filter;
        this.fieldName = fieldName;
    }

    @Override
    public DistinctIterable<TResult> filter(Bson filter) {
        this.filter = applyVersionFilters(filter);
        this.findIterable.filter(this.filter);
        return this;
    }

    @Override
    public DistinctIterable<TResult> maxTime(long maxTime, TimeUnit timeUnit) {
        this.findIterable.maxTime(maxTime, timeUnit);
        return this;
    }

    @Override
    public DistinctIterable<TResult> batchSize(int batchSize) {
        this.findIterable.batchSize(batchSize);
        return this;
    }

    @Override
    public MongoCursor<TResult> iterator() {
        MongoCursor<TResult> mongoCursor = this.findIterable.iterator();
        CpMongoCursorDistinctImpl cpCursor = new CpMongoCursorDistinctImpl(mongoCursor, resultClass, cpCollection, this.codecRegistry, 0, -1, fieldName);
        //this.cpCollection.collection.clientCursors.add(cpCursor);
        return cpCursor;
    }

    @Override
    public TResult first() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <U> MongoIterable<U> map(Function<TResult, U> mapper) {
        return new MappingIterable<>(this, mapper);
    }

    @Override
    public void forEach(Block<? super TResult> block) {
        try (MongoCursor<TResult> cursor = iterator()) {
            while(cursor.hasNext()) {
                block.apply(cursor.next());
            }
        }
    }

    @Override
    public <A extends Collection<? super TResult>> A into(final A target) {
        forEach(new Block<TResult>() {
            @Override
            public void apply(TResult t) {
                target.add(t);
            }
        });
        
        return target;
    }
    
    
    private BsonDocument applyVersionFilters(Bson filter) {
        VersionQueryUtils versionQueryUtils = new VersionQueryUtils();
        BsonDocument bsonDocument = filter.toBsonDocument(documentClass, codecRegistry);
        List<BsonValue> listQuerys = new ArrayList<>();
        listQuerys.add(VersionQueryUtils.getQueryPrivateVersion(BsonDocument.parse(bsonDocument.toJson()), this.cpCollection.mongoTransactionalContext.getCtxId(), codecRegistry));
        listQuerys.add(versionQueryUtils.getQueryVersion(BsonDocument.parse(bsonDocument.toJson()), this.cpCollection.mongoTransactionalContext.getStartTimestamp()));
        BsonDocument query = new BsonDocument("$or", new BsonArray(listQuerys)); 
        return query;
    }
    
}
