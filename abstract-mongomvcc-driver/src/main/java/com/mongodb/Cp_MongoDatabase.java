package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.Closeable;

/**
 *
 * @author idezol
 */
public interface Cp_MongoDatabase extends Closeable {
    
    public String getName();
    
    public void startTransaction() throws TransactionManagerException;
    
    public void commit() throws TransactionManagerException, DataStoreException;
    
    public void rollback() throws TransactionManagerException;
      
    
}
