package com.mongodb.utils;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TestConf {
    private static final org.slf4j.Logger Log = LoggerFactory.getLogger(TestConf.class);
    
    private static final String FILE_NAME = "testByteArrayFile.txt";
    
    public static void writeWriteSetToFile(byte [] bytes) {
        File file = new File(FILE_NAME);
        try {
            Files.deleteIfExists(file.toPath());
        } catch(IOException ex) {
            Log.info("Could not delete existing file at: {} . {}: {}", file.getAbsolutePath(), ex.getClass().getName(), ex.getMessage());
            return;
        }
        
        try {
            FileUtils.writeByteArrayToFile(file, bytes);
        } catch(IOException ex) {
            Log.info("Could not write private writeset to file at: {} . {}: {}", file.getAbsolutePath(), ex.getClass().getName(), ex.getMessage());
            return;
        }
        
        Log.info("Writeset was written to file: {}", file.getAbsolutePath());
    }
    
    public static byte [] restoreWriteSetFromFile() {
        File file = new File(FILE_NAME);
        if(!Files.exists(file.toPath())) {
            Log.info("File containing the private wriset does not exist at: {}", file.getAbsolutePath());
            return null;
        }
        
        byte [] bytes;
        try {
            bytes = FileUtils.readFileToByteArray(file);
        } catch(IOException ex) {
            Log.info("Could not read private writeset from file at: {} . {}: {}", file.getAbsolutePath(), ex.getClass().getName(), ex.getMessage());
            return null;
        }
        
        
        return bytes;
    }
}
