package com.mongodb.utils;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.mongodb.utils.ByteOperators;
import eu.coherentpaas.mongodb.utils.VersionQueryUtils;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import org.bson.BsonBinary;
import org.bson.BsonBinarySubType;
import org.bson.BsonBoolean;
import org.bson.BsonDocument;
import org.bson.BsonDocumentReader;
import org.bson.BsonInt64;
import org.bson.BsonString;
import org.bson.BsonValue;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 * @param <TDocument>
 */
public class CpRemover<TDocument> {
    private static final Logger Log = LoggerFactory.getLogger(CpRemover.class);
    private final int NUM_THREADS = 10;
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final Bson _query;
    protected final MongoTransactionalContext mongoTransactionContext;
    protected final Class<TDocument> documentClass;
    protected final CodecRegistry codecRegistry;
    protected final MongoCollection _depracedDBCollection;
    protected final boolean removeOne;
    protected Bson sort;
    protected VersionQueryUtils versionQueryUtils;

    public CpRemover(Class<TDocument> documentClass, ConcurrentMap<Object, Bson> writeset, Bson _query, MongoTransactionalContext mongoTransactionContext, MongoCollection _depracedDBCollection, CodecRegistry codecRegistry, boolean removeOne, Bson sort, VersionQueryUtils versionQueryUtils) {
        this.documentClass = documentClass;
        this.writeset = writeset;
        this._query = _query;
        this.mongoTransactionContext = mongoTransactionContext;
        this._depracedDBCollection = _depracedDBCollection;
        this.codecRegistry = codecRegistry;
        this.removeOne = removeOne;
        this.sort = sort;
        this.versionQueryUtils = versionQueryUtils;
    }
    
    public DeleteResult remove() throws TransactionManagerException, MongoMVCCException, TransactionalMongoException {
        long countTotal = 0;
        BsonDocument query = this._query.toBsonDocument(documentClass, codecRegistry);
        
        //first update the private writeset and set to be deleted
        if(!this.writeset.isEmpty()) {
            BsonDocument setDeleteFlag = 
                    new BsonDocument("$set", new BsonDocument(MongoMVCCFields.DELETED_DOCUMENT, new BsonBoolean(true)));
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getCtxId(), codecRegistry);

            UpdateResult intermediateRes = null;
            if(!removeOne) {
                intermediateRes = this._depracedDBCollection.updateMany(queryVersioned, setDeleteFlag);
                if(intermediateRes.wasAcknowledged())
                    countTotal = countTotal + intermediateRes.getModifiedCount();
            }
            else {
                Object obj = this._depracedDBCollection.findOneAndUpdate(queryVersioned, setDeleteFlag);
                if(obj!=null)
                    countTotal = 1;
            }

            if((intermediateRes!=null)&&(!intermediateRes.wasAcknowledged()))
                throw new MongoMVCCException("Could not remove from transaction's private versions");

            //if was set to remove one, and one is found then return
            if((removeOne)&&(countTotal==1))
                    return  DeleteResult.acknowledged(1);
        
        }
        
        
        //now add a new 'deleted' version to documents that are public and ARE NOT in the transaction's private writeset
        List<TDocument> versionsToInsert = new ArrayList<>();
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getCtxId()), documentClass)
                .iterator()) {
            while(cursor.hasNext()) {
                TDocument obj = cursor.next();
                
                if(!(obj instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                
                BsonDocument dbObject = ((Bson)obj).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId =  dbObject.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been checked previosly)
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
                    
                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);
                    
                    countTotal++;
                    versionsToInsert.add(removeDocument(obj));
                    this.writeset.putIfAbsent(currentObjectId, dbObject);
                    
                    //if set to remove one, then add this first one and return
                    if(removeOne)
                        break;
                }
            }
        }
        
        //now add the new versions
        if(!versionsToInsert.isEmpty())
            this._depracedDBCollection.insertMany(versionsToInsert);
        
        return DeleteResult.acknowledged(countTotal);
    }
    
    
    public TDocument findAndRemove() throws TransactionManagerException, MongoMVCCException, TransactionalMongoException {
        BsonDocument query = this._query.toBsonDocument(documentClass, codecRegistry);
        TDocument res = null;
        
        //first update the private writeset and set to be deleted
        if(!this.writeset.isEmpty()) {
            BsonDocument setDeleteFlag = 
                    new BsonDocument("$set", new BsonDocument(MongoMVCCFields.DELETED_DOCUMENT, new BsonBoolean(true)));
            BsonDocument queryVersioned = VersionQueryUtils
                    .getQueryPrivateVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getCtxId(), codecRegistry);


            FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
            if(this.sort!=null)
                options = options.sort(sort);

            res =  (TDocument) this._depracedDBCollection.findOneAndUpdate(queryVersioned, setDeleteFlag, options);
            if(res!=null)
                return fixIdentifier(res);
        
        }
        
        //now add a new 'deleted' version to documents that are public and ARE NOT in the transaction's private writeset
        if(sort==null)
            sort = new BsonDocument();
        try (MongoCursor<TDocument> cursor = this._depracedDBCollection
                .find(versionQueryUtils.getQueryVersion(BsonDocument.parse(query.toJson()), mongoTransactionContext.getCtxId()), documentClass)
                .sort(sort)
                .iterator()) {
            while(cursor.hasNext()) {
                res = cursor.next();
                
                if(!(res instanceof Bson)) {
                    IllegalStateException ex = new IllegalStateException("Documents in database are not in BSON format");
                    throw new MongoMVCCException(MongoMVCCException.Severity.Execution, ex.getMessage(), ex);
                }
                
                BsonDocument dbObject = ((Bson)res).toBsonDocument(documentClass, codecRegistry);
                BsonValue currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
                
                //if there's not in the private writeset (if it was, then the new one has already been checked previosly)
                if(!writeset.containsKey(currentObjectId)) {
                    //get the key in byte [] to be checked for conflicts
                    byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
                    
                    //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
                    this.mongoTransactionContext.checkForConflicts(key);
                    
                    this.writeset.putIfAbsent(currentObjectId, dbObject);
                    
                    //if set to remove one, then add this first one and return
                    if(removeOne)
                        break;
                }
            }
        }
        
        //now add the new versions
        this._depracedDBCollection.insertOne(removeDocument((TDocument) res));
        
        return fixIdentifier(res);
    }
    
    
    private TDocument removeDocument(final TDocument bson) {
        BsonDocument dbObject = ((Bson)bson).toBsonDocument(documentClass, codecRegistry);
        
        //remove its _id and timestamps and add the new ones
        dbObject.remove(MongoMVCCFields._ID);
        dbObject.remove(MongoMVCCFields.TID);
        dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
        dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);
        
        //append values
        dbObject.append(MongoMVCCFields.TID, new BsonInt64(this.mongoTransactionContext.getCtxId()))
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, new BsonString(""))
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, new BsonString(""))
                .append(MongoMVCCFields.DELETED_DOCUMENT, new BsonBoolean(true))
                .append(MongoMVCCFields._ID, 
                        new BsonBinary(BsonBinarySubType.USER_DEFINED,
                                ByteOperators.getBytes(dbObject.get(MongoMVCCFields.DATA_ID), this.mongoTransactionContext.getCtxId()))
                        );
        
        return codecRegistry.get(documentClass).decode(new BsonDocumentReader(dbObject), null);
    }
    
    
    /**
     * 
     * Receives a TDocument document. It then moves the DATA_ID to the ID, where it is expected to be found by the user
     * 
     * @param bson
     * @return TDocument
     */
    private TDocument fixIdentifier(final TDocument bson) {
        BsonDocument dbObject = ((Bson)bson).toBsonDocument(documentClass, codecRegistry);
        BsonValue objID = dbObject.get(MongoMVCCFields.DATA_ID);
        dbObject.put(MongoMVCCFields._ID, objID);
        dbObject.remove(MongoMVCCFields.DATA_ID);
        
        return codecRegistry.get(documentClass).decode(new BsonDocumentReader(dbObject), null);
    }
}
