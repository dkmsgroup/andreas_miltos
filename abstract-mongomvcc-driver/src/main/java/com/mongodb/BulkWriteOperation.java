package com.mongodb;

import com.mongodb.BulkWriteResult;
import com.mongodb.DBObject;
import com.mongodb.InsertRequest;
import com.mongodb.WriteConcern;
import com.mongodb.WriteRequest;
import static com.mongodb.assertions.Assertions.isTrue;
import java.util.ArrayList;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author idezol
 */
public class BulkWriteOperation  {
    private static final String ID_FIELD_NAME = "_id";
    private final boolean ordered;
    private final DBCollectionAbstract collection;
    private final List<WriteRequest> requests = new ArrayList<WriteRequest>();
    private boolean closed;
    
    BulkWriteOperation(final boolean ordered, final DBCollectionAbstract collection) {
        this.ordered = ordered;
        this.collection = collection;
    }
    
    
    /**
     * Returns true if this is building an ordered bulk write request.
     *
     * @return whether this is building an ordered bulk write operation
     * @see DBCollection#initializeOrderedBulkOperation()
     * @see DBCollection#initializeUnorderedBulkOperation()
     */
    public boolean isOrdered() {
        return ordered;
    }

    /**
     * Add an insert request to the bulk operation
     *
     * @param document the document to insert
     */
    public void insert(final DBObject document) {
        isTrue("already executed", !closed);
        if (document.get(ID_FIELD_NAME) == null) {
            document.put(ID_FIELD_NAME, new ObjectId());
        }
        addRequest(new InsertRequest(document, collection.getObjectCodec()));
    }

    /**
     * Start building a write request to add to the bulk write operation.  The returned builder can be used to create an update, replace,
     * or remove request with the given query.
     *
     * @param query the query for an update, replace or remove request
     * @return a builder for a single write request
     */
    public BulkWriteRequestBuilder find(final DBObject query) {
        isTrue("already executed", !closed);
        return new BulkWriteRequestBuilder(this, query, collection.getDefaultDBObjectCodec(), collection.getObjectCodec());
    }

    /**
     * Execute the bulk write operation with the default write concern of the collection from which this came.  Note that the
     * continueOnError property of the write concern is ignored.
     *
     * @return the result of the bulk write operation.
     * @throws com.mongodb.BulkWriteException if the write failed due some other failure specific to the write command
     * @throws MongoException if the operation failed for some other reason
     */
    public BulkWriteResult execute() {
        isTrue("already executed", !closed);
        closed = true;
        return collection.executeBulkWriteOperation(ordered, requests);
    }

    /**
     * Execute the bulk write operation with the given write concern.  Note that the continueOnError property of the write concern is
     * ignored.
     *
     * @param writeConcern the write concern to apply to the bulk operation.
     * @return the result of the bulk write operation.
     * @throws com.mongodb.BulkWriteException if the write failed due some other failure specific to the write command
     * @throws MongoException if the operation failed for some other reason
     */
    public BulkWriteResult execute(final WriteConcern writeConcern) {
        isTrue("already executed", !closed);
        closed = true;
        return collection.executeBulkWriteOperation(ordered, requests, writeConcern);
    }

    protected void addRequest(final WriteRequest request) {
        isTrue("already executed", !closed);
        requests.add(request);
    }

  
}
