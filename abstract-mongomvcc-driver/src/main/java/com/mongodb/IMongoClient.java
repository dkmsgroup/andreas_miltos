package com.mongodb;

import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ReadPreference;
import com.mongodb.ReplicaSetStatus;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author stavroula
 */
public interface IMongoClient {
    
    public List<MongoCredential> getCredentialsList();
    
    public MongoClientOptions getMongoClientOptions();
    
    //from mongo
    
    public IDB getDB( String dbname );
    
    public Collection<IDB> getUsedDatabases();
    
    public List<String> getDatabaseNames();
    
    public void dropDatabase(String dbName);
    
    public String getConnectPoint();
    
    public ReplicaSetStatus getReplicaSetStatus();
    
    public ServerAddress getAddress();
    
    public List<ServerAddress> getAllAddress();
    
    public List<ServerAddress> getServerAddressList();
    
    public void close();
    
    public void setWriteConcern( WriteConcern concern );
    
    public WriteConcern getWriteConcern();
    
    public void setReadPreference(ReadPreference preference);
    
    public ReadPreference getReadPreference();
    
    public void addOption( int option );
    
    public void setOptions( int options );
    
    public void resetOptions();
    
    public int getOptions();
    
    public int getMaxBsonObjectSize();
    
    public CommandResult fsync(boolean async);
    
    public CommandResult fsyncAndLock();
    
    public DBObject unlock();
    
    public boolean isLocked();
  
}