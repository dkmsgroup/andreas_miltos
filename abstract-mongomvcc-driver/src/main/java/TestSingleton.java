/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author idezol-user
 */
public class TestSingleton {
    private static TestSingleton instance;
    
    private TestSingleton() {}
    
    public static TestSingleton getInstance() {
        if(instance==null)
            instance = new TestSingleton();
        return instance;
    }
}
