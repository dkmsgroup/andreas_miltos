package eu.coherentpaas.mongodb.events;

/**
 *
 * @author idezol
 */
public interface DatabaseTransactionBeginListener {
    
    public void databaseTransactionBegin(DatabaseTransactionBeginEvent event);
}
