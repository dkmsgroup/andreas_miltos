package eu.coherentpaas.mongodb.events;

import java.util.EventObject;

/**
 *
 * @author idezol
 */
public class DatabaseTransactionCommitEvent extends EventObject  {
    public DatabaseTransactionCommitEvent(Object source) {
        super(source);
    }
}
