package eu.coherentpaas.mongodb.events;

import java.util.EventObject;

/**
 * MongoMVCCDatabase fires this event in case of a rollback to close
 * 
 * @author idezol
 */
public class DatabaseTransactionRollbackEvent extends EventObject  {
    public DatabaseTransactionRollbackEvent(Object source) {
        super(source);
    }
}
