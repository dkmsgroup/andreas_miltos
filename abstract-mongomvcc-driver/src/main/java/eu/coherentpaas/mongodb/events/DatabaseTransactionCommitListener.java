package eu.coherentpaas.mongodb.events;

/**
 *
 * @author idezol
 */
public interface DatabaseTransactionCommitListener {
    
    public void databaseTransactionCommitDetected(DatabaseTransactionCommitEvent event);
}
