package eu.coherentpaas.mongodb.config;


/**
 *
 * @author idezol
 */
public class MongoMVCCFields {
    
    public static final String _ID = "_id";
    public static final String DATA_ID = "_dataID";
    public static final String COMMIT_TIMESTAMP = "_cmtTmstmp";
    public static final String NEXT_COMMIT_TIMESTAMP = "_nextCcmtTmstmp";
    public static final String TID = "_tid";
    public static final String DEFAULT_COMPOUND_INDEX = "_defaultIndex";
    public static final String DEFAULT_COMPOUND_INDEX_TID = "_defaultIndex_tid";
    public static final String DELETED_DOCUMENT = "_isDeleted";
    public static final String STRING_SEPARATOR = "___";
    
}
