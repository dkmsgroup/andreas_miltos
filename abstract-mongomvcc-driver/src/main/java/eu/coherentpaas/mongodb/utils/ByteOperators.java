package eu.coherentpaas.mongodb.utils;

import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang.SerializationUtils;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ByteOperators {
    
    private static final Logger Log = LoggerFactory.getLogger(ByteOperators.class);
    
    /**
     * Generates the key to be sent for conflict resolution, according to the datastore granularity
     * this means that the key will be sent the concatenation of bytes of datastoreID, databaseName, collection name 
     * and the 12bytes objectID
     *
     * @param dataStoreId the byte array of the name of the datastore
     * @param dataBaseName the byte array of the database name of the current datastore
     * @param collectionName the byte array of the current collection
     * @param objectID the newly created object in string format
     * @return byte [] array of bytes of the new _id     
     */
    public static byte [] getKeyBytes(DataStoreId dataStoreId, String dataBaseName, String collectionName, String objectID) {
        
        byte [] dataStoreBytes = dataStoreId.name().getBytes();
        byte [] databaseBytes = dataBaseName.getBytes();
        byte [] collectionNames = collectionName.getBytes();
        byte [] objectIDBytes = new ObjectId(objectID).toByteArray();
        byte [] stringSepartor = MongoMVCCFields.STRING_SEPARATOR.getBytes();
        
        int lenght = dataStoreBytes.length + databaseBytes.length + collectionNames.length + objectIDBytes.length + (2*stringSepartor.length);
        byte [] bytes = ByteBuffer.allocate(lenght)
                .put(dataStoreBytes)
                .put(stringSepartor)
                .put(databaseBytes)
                .put(stringSepartor)
                .put(collectionNames)
                .put(objectIDBytes)
                .array();
        
        return bytes;
    }
    
    
    /**
     * Generates the key to be sent for conflict resolution, according to the datastore granularity
     * this means that the key will be sent the concatenation of bytes of datastoreID, databaseName, collection name 
     * and the 12bytes objectID
     *
     * @param dataStoreId the byte array of the name of the datastore
     * @param dataBaseName the byte array of the database name of the current datastore
     * @param collectionName the byte array of the current collection
     * @param objectID the newly created object in ObjectID format
     * @return byte [] array of bytes of the new _id     
     */
    public static byte [] getKeyBytes(DataStoreId dataStoreId, String dataBaseName, String collectionName, ObjectId objectID) {
        
        byte [] dataStoreBytes = dataStoreId.name().getBytes();
        byte [] databaseBytes = dataBaseName.getBytes();
        byte [] collectionNames = collectionName.getBytes();
        byte [] objectIDBytes = objectID.toByteArray();
        byte [] stringSepartor = MongoMVCCFields.STRING_SEPARATOR.getBytes();
        
        int lenght = dataStoreBytes.length + databaseBytes.length + collectionNames.length + objectIDBytes.length + (2*stringSepartor.length);
        byte [] bytes = ByteBuffer.allocate(lenght)
                .put(dataStoreBytes)
                .put(stringSepartor)
                .put(databaseBytes)
                .put(stringSepartor)
                .put(collectionNames)
                .put(objectIDBytes)
                .array();
        
        return bytes;
    }
    
    
    /**
     * Generates the key to be sent for conflict resolution, according to the datastore granularity
     * this means that the key will be sent the concatenation of bytes of datastoreID, databaseName, collection name 
     * and the 12bytes objectID
     *
     * @param offset String which is the concatenation of datastoreId + "___" + databaseName + "___" + collectionName. the "___" is strongly recommended NOT TO BE USED. Use the 'MongoMVCCFields.STRING_SEPARATOR' instead
     * @param objectID the newly created object 
     * @return byte [] array of bytes of the new _id     
     */
    public static byte [] getKeyBytes(String offset, Object objectID) {
        
        byte [] offsetBytes = offset.getBytes();
        byte [] objectIDBytes = SerializationUtils.serialize((Serializable) objectID);
        
        int lenght = offsetBytes.length + objectIDBytes.length;
        byte [] bytes = ByteBuffer.allocate(lenght)
                .put(offsetBytes)
                .put(objectIDBytes)
                .array();
        
        return bytes;
    }
    
    
    /**
     * Generates the key to be sent for conflict resolution, according to the datastore granularity
     * this means that the key will be sent the concatenation of bytes of datastoreID, databaseName, collection name 
     * and the 12bytes objectID
     *
     * @param offset string which is the concatenation of datastoreId + "___" + databaseName + "___" + collectionName. the "___" is strongly recommended NOT TO BE USED. Use the 'MongoMVCCFields.STRING_SEPARATOR' instead
     * @param objectID the newly created object in string format
     * @return byte [] array of bytes of the new _id     
     */
    public static byte [] getKeyBytes(String offset, String objectID) {
        
        byte [] offsetBytes = offset.getBytes();
        byte [] objectIDBytes = new ObjectId(objectID).toByteArray();
        
        int lenght = offsetBytes.length + objectIDBytes.length;
        byte [] bytes = ByteBuffer.allocate(lenght)
                .put(offsetBytes)
                .put(objectIDBytes)
                .array();
        
        return bytes;
    }
    
    
    /**
     * Receives the private write-set of a collection and returns it back as an array of bytes 
     * to be used as the logging of this transaction
     * 
     * @param databaseName
     * @param collectionName
     * @param writeset
     * @return byte [] array of bytes of the writeset   
     * @throws java.io.IOException   
     */
    public static byte [] getBytesFromWriteSet(String databaseName, String collectionName, Map<ObjectId, Bson> writeset)  throws IOException {
        
        Map<String, Bson> map = new HashMap<>();
        for (Entry entry : writeset.entrySet()) {
            ObjectId objectId = (ObjectId) entry.getKey();
            String key = databaseName
                    + MongoMVCCFields.STRING_SEPARATOR 
                    + collectionName
                    + MongoMVCCFields.STRING_SEPARATOR
                    + objectId.toString();
            
            map.put(key, (Bson) entry.getValue());
        }
        
        //now transform it to a byte array
        ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(byteOut);
        out.writeObject(writeset);
        
        return byteOut.toByteArray();
    }
    
    /**
     * Generates the 20bytes new _id of the document by concatenating the ObjectID and the current version
     *
     * @param objectId
     * @param version the newly created timestamp of this version
     * @return byte [] array of bytes of the new _id
     * @getBytes commitInsertNewVersion
     */
    public static byte [] getBytes(Object objectId, long version) {
        byte [] objectBytes = SerializationUtils.serialize((Serializable) objectId);
        
        byte [] bytes = ByteBuffer.allocate(objectBytes.length+8)
                .put(objectBytes)
                .putLong(version).array();
        return bytes;
    }
   
    
    /**
     * Generates the 20bytes new _id of the document by concatenating the ObjectID and the current version
     *
     * @param objectId the new ObjectID value of the document
     * @param version the newly created timestamp of this version
     * @return byte [] array of bytes of the new _id
     * @getBytes commitInsertNewVersion
     */
    public static byte [] getBytes(String objectId, long version) {
        
        byte [] bytes = ByteBuffer.allocate(0x14)
                .put(new ObjectId(objectId).toByteArray())
                .putLong(version).array();
        return bytes;
    }
}
