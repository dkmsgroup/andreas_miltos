package eu.coherentpaas.mongodb.utils;

import com.mongodb.WriteResult;

/**
 *
 * @author idezol
 */
public class WriteResultBuilder {
    private final boolean acknowledged;
    private final int n;
    private final boolean updateOfExisting;
    private final Object upsertedId;

    public WriteResultBuilder(boolean acknowledged, int n, boolean updateOfExisting, Object upsertedId) {
        this.acknowledged = acknowledged;
        this.n = n;
        this.updateOfExisting = updateOfExisting;
        this.upsertedId = upsertedId;
    }
    
    public WriteResultBuilder(WriteResult writeResult) {
        this.acknowledged = writeResult.wasAcknowledged();
        this.n = writeResult.getN();
        this.updateOfExisting = writeResult.isUpdateOfExisting();
        this.upsertedId = writeResult.getUpsertedId();
    }
    
    
    public WriteResult copy() {
        WriteResult result = new WriteResult(n, updateOfExisting, upsertedId);
        return result;
    }
    
    public WriteResult copy(int n) {
        WriteResult result = new WriteResult(n, updateOfExisting, upsertedId);
        return result;
    }
}
