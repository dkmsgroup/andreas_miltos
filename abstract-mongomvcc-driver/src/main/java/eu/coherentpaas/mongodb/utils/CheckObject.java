package eu.coherentpaas.mongodb.utils;

import com.mongodb.DBObject;
import com.mongodb.LazyDBList;
import com.mongodb.LazyDBObject;
import java.util.List;
import java.util.Map;

/**
 *
 * @author idezol
 */
public class CheckObject {
    
    
    /**
     * @deprecated This method should not be a part of API.
     *             If you override one of the {@code DepracedDBCollection} methods please rely on superclass
     *             implementation in checking argument correctness and validity.
     */
    @Deprecated
    public static DBObject _checkObject(DBObject o, boolean canBeNull, boolean query) {
        if (o == null) {
            if (canBeNull)
                return null;
            throw new IllegalArgumentException("can't be null");
        }

        if (o.isPartialObject() && !query)
            throw new IllegalArgumentException("can't save partial objects");

        if (!query) {
            _checkKeys(o);
        }
        return o;
    }

    /**
     * Checks key strings for invalid characters.
     */
    private static void _checkKeys( DBObject o ) {
        if ( o instanceof LazyDBObject || o instanceof LazyDBList )
            return;

        for ( String s : o.keySet() ){
            validateKey( s );
            _checkValue( o.get( s ) );
        }
    }

    /**
     * Checks key strings for invalid characters.
     */
    private static void _checkKeys( Map<String, Object> o ) {
        for ( Map.Entry<String, Object> cur : o.entrySet() ){
            validateKey( cur.getKey() );
            _checkValue( cur.getValue() );
        }
    }

    private static void _checkValues( final List list ) {
        for ( Object cur : list ) {
            _checkValue( cur );
        }
    }

    private static void _checkValue(final Object value) {
        if ( value instanceof DBObject ) {
            _checkKeys( (DBObject)value );
        } else if ( value instanceof Map ) {
            _checkKeys( (Map<String, Object>)value );
        } else if ( value instanceof List ) {
            _checkValues((List) value);
        }
    }

    /**
     * Check for invalid key names
     *
     * @param s the string field/key to check
     * @throws IllegalArgumentException if the key is not valid.
     */
    private static void validateKey(String s ) {
        if ( s.contains( "\0" ) )
            throw new IllegalArgumentException( "Document field names can't have a NULL character. (Bad Key: '" + s + "')" );
        if ( s.contains( "." ) )
            throw new IllegalArgumentException( "Document field names can't have a . in them. (Bad Key: '" + s + "')" );
        if ( s.startsWith( "$" ) )
            throw new IllegalArgumentException( "Document field names can't start with '$' (Bad Key: '" + s + "')" );
    }
}
