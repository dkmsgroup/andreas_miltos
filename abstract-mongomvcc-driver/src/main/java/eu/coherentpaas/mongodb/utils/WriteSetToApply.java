package eu.coherentpaas.mongodb.utils;

import com.mongodb.BasicDBList;

/**
 *
 * @author idezol
 */
public class WriteSetToApply {
    private final boolean withUpdates;
    private final BasicDBList dbList;

    public WriteSetToApply(boolean isInsertOnly, BasicDBList dbList) {
        this.withUpdates = isInsertOnly;
        this.dbList = dbList;
    }

    public boolean isWithUpdates() {
        return withUpdates;
    }

   
    public BasicDBList getDbList() {
        return dbList;
    }
    
}
