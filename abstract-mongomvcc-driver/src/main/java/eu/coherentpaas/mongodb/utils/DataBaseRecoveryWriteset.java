package eu.coherentpaas.mongodb.utils;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author idezol
 */
public class DataBaseRecoveryWriteset implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String databaseName;
    private final List<CollectionRecoveryWriteset> list;

    public DataBaseRecoveryWriteset(String databaseName, List<CollectionRecoveryWriteset> list) {
        this.databaseName = databaseName;
        this.list = list;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public List<CollectionRecoveryWriteset> getList() {
        return list;
    }
    
}
