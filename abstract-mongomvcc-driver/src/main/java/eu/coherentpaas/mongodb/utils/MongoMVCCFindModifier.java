package eu.coherentpaas.mongodb.utils;


import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DeprecatedDBCollection;
import com.mongodb.DeprecatedDBCursor;
import com.mongodb.MongoException;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.mongodb.exceptions.MongoMVCCException;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class MongoMVCCFindModifier {
    private static final Logger Log = LoggerFactory.getLogger(MongoMVCCFindModifier.class);
    protected final ConcurrentMap<Object, Bson> writeset;
    protected final DBObject _query;
    protected final DBObject _fields;
    protected final DBObject _sort;
    protected final boolean remove;
    protected final boolean upsert;
    protected final boolean returnNew;
    protected final MongoTransactionalContext mongoTransactionalContext;
    protected final DeprecatedDBCollection _depracedDBCollection;
    protected final DBObject _update;

    public MongoMVCCFindModifier(ConcurrentMap<Object, Bson> writeset, DBObject _query, DBObject _fields, DBObject _sort, DBObject _update, boolean remove, boolean upsert, boolean returnNew, MongoTransactionalContext mongoTransactionalContext, DeprecatedDBCollection _depracedDBCollection) {
        this.writeset = writeset;
        this._query = _query;
        this._fields = _fields;
        this._sort = _sort;
        this._update = _update;
        this.remove = remove;
        this.upsert = upsert;
        this.returnNew = returnNew;
        this.mongoTransactionalContext = mongoTransactionalContext;
        this._depracedDBCollection = _depracedDBCollection;
    }
    
    public DBObject findAndMofify() throws TransactionManagerException, MongoMVCCException, TransactionalMongoException {
        
        if (remove && !(_update == null || _update.keySet().isEmpty() || returnNew))
            throw new MongoException("FindAndModify: Remove cannot be mixed with the Update, or returnNew params!");
        
        BasicDBList listQueries = new BasicDBList();
        if(!this.writeset.isEmpty()) 
            listQueries.add(VersionQueryUtils.getQueryPrivateVersion((BasicDBObject) _query, mongoTransactionalContext.getCtxId()));
        listQueries.add(VersionQueryUtils.getQueryVersion((BasicDBObject) _query, mongoTransactionalContext.getStartTimestamp()));
        BasicDBObject queryObject = new BasicDBObject("$or", listQueries);
        DeprecatedDBCursor cursor = this._depracedDBCollection.find(queryObject, _fields).sort(_sort);
        BasicDBObject documentFound = null;
        while(cursor.hasNext()) {
            documentFound = (BasicDBObject) cursor.next();
            
            //if document is already in the writeset, then check if this version is outdated or current (private)
            if(this.writeset.containsKey(documentFound.get(MongoMVCCFields.DATA_ID))) {
                if(documentFound.getLong(MongoMVCCFields.TID)==this.mongoTransactionalContext.getCtxId())
                    break;
                else 
                    documentFound = null;
            }
            
            //if not found (previous document was outdated, then check the next one
            if(documentFound!=null)
                break;
        }
        
        if(remove) {
            //do remove and return the result of the find
            removeDoc(documentFound);
            return documentFound;
        }
        
        //do update and return correspondingly
        BasicDBObject updatedResult = updateDoc(documentFound);
        
        if(returnNew)
            return updatedResult;
        
        return documentFound;
    }
    
    
    protected BasicDBObject updateDoc(BasicDBObject dbObject) throws TransactionManagerException, MongoMVCCException, TransactionalMongoException {
        
        BasicDBObject updateQuery = new BasicDBObject(_update.toMap());
        BasicDBObject result;
        
        //document appears in the private writeset -- update and set deleted flag
        if(this.writeset.containsKey(dbObject.get(MongoMVCCFields.DATA_ID))) {
            BasicDBObject query = new BasicDBObject(MongoMVCCFields.DATA_ID, dbObject.getObjectId(MongoMVCCFields.DATA_ID))
                                                .append(MongoMVCCFields.TID, mongoTransactionalContext.getCtxId())
                                                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "");
            
            
            if(!updateQuery.keySet().iterator().next().equals("$set")) {
                //if it is not a "set" update, then check for the 'data_id'
                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!updateQuery.containsField(MongoMVCCFields.DATA_ID)) {
                    updateQuery.append(MongoMVCCFields.DATA_ID, dbObject.getObjectId(MongoMVCCFields.DATA_ID));
                }
                
                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!updateQuery.getObjectId(MongoMVCCFields.DATA_ID).equals(dbObject.getObjectId(MongoMVCCFields.DATA_ID))) {
                    updateQuery.append(MongoMVCCFields._ID, ByteOperators.getBytes(updateQuery.getObjectId(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getCtxId()));               
                }
                
            } 
            
            result = (BasicDBObject) this._depracedDBCollection.findAndModify(query, _fields, _sort, false, updateQuery, false, this.upsert);
            
        } else {
            Object currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
            
            //get the key in byte [] to be checked for conflicts
            byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
            
            //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
            this.mongoTransactionalContext.checkForConflicts(key);
            
            if(!updateQuery.keySet().iterator().next().equals("$set")) {
                //if already has a DATA_ID different than its previous one, then change the _id correspondingly
                if(!updateQuery.containsField(MongoMVCCFields.DATA_ID)) {
                    updateQuery.append(MongoMVCCFields.DATA_ID, currentObjectId);
                }

                updateQuery.append(MongoMVCCFields._ID, ByteOperators.getBytes(currentObjectId, this.mongoTransactionalContext.getCtxId()))
                                 .append(MongoMVCCFields.TID, this.mongoTransactionalContext.getCtxId())
                                 .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                                 .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "");
                
                this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) updateQuery);
                result = updateQuery;
                
                this._depracedDBCollection.insert(result);                
                
            } else {
                //do the "set" operation
                result = updateDocumentSetValues((BasicDBObject) dbObject); 
                this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) updateQuery);
                this._depracedDBCollection.insert(result);  
            }
        }
        
        return result;
    }
    
    
    protected void removeDoc(BasicDBObject dbObject) throws TransactionManagerException, TransactionalMongoException {
 
        //document appears in the private writeset -- update and set deleted flag
        if(this.writeset.containsKey(dbObject.get(MongoMVCCFields.DATA_ID))) {
            
            BasicDBObject query = new BasicDBObject(MongoMVCCFields.DATA_ID, dbObject.getObjectId(MongoMVCCFields.DATA_ID))
                                                .append(MongoMVCCFields.TID, mongoTransactionalContext.getCtxId())
                                                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "");
            BasicDBObject setDeleteFlag = new BasicDBObject("$set", new BasicDBObject(MongoMVCCFields.DELETED_DOCUMENT, true));
            this._depracedDBCollection.update(query, setDeleteFlag, true, false);
        } else {
            Object currentObjectId = dbObject.get(MongoMVCCFields.DATA_ID);
            //no need to check here if it belongs to the private writeset, this has alraedy been checked
            //get the key in byte [] to be checked for conflicts
            byte [] key = ByteOperators.getKeyBytes(MongoMVCCFields.STRING_SEPARATOR, currentObjectId);
            //if transaction fails, then a TransactionManagerException will be thrown which will lead to a rollback
            this.mongoTransactionalContext.checkForConflicts(key);
            this.writeset.putIfAbsent(currentObjectId, (BasicDBObject) dbObject);
            
             //remove its _id and timestamps and add the new ones
            dbObject.remove(MongoMVCCFields._ID);
            dbObject.remove(MongoMVCCFields.TID);
            dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
            dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);

            //append values
            dbObject.append(MongoMVCCFields.TID, this.mongoTransactionalContext.getCtxId())
                    .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                    .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                    .append(MongoMVCCFields.DELETED_DOCUMENT, true)
                    .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.getObjectId(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getCtxId()));
            
            //and add it
            this._depracedDBCollection.insert(dbObject);
        }
    }
    
    
    private BasicDBObject updateDocumentSetValues(BasicDBObject dbObject) throws MongoMVCCException {
        String key = _update.keySet().iterator().next();
        if(!key.equals("$set"))
            throw new MongoMVCCException("not a valid set parameter");
        
        BasicDBObject values = (BasicDBObject) ((BasicDBObject) _update).values().iterator().next();
        for (Entry entry : values.entrySet()) {
            String documentField = (String) entry.getKey();
            Object documentFieldValue = entry.getValue();
            if(upsert) {
                //put the value, even if the specific field does not exist
                dbObject.put(documentField, documentFieldValue);
            } else {
                //check if the field exists and update only if exists
                if(dbObject.containsField(documentField))
                    dbObject.put(documentField, documentFieldValue);
            }
        }
        
        //remove its _id and timestamps and add the new ones
        dbObject.remove(MongoMVCCFields._ID);
        dbObject.remove(MongoMVCCFields.TID);
        dbObject.remove(MongoMVCCFields.COMMIT_TIMESTAMP);
        dbObject.remove(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP);
        
        dbObject.append(MongoMVCCFields.TID, this.mongoTransactionalContext.getCtxId())
                .append(MongoMVCCFields.COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields.NEXT_COMMIT_TIMESTAMP, "")
                .append(MongoMVCCFields._ID, ByteOperators.getBytes(dbObject.get(MongoMVCCFields.DATA_ID), this.mongoTransactionalContext.getCtxId()));
        
        return dbObject;
    }
    
}
