package eu.coherentpaas.mongodb.utils;

/**
 *
 * @author idezol
 */
public enum WrapperMethodResponseType {
    LONG,
    CURSOR,
    DBOBJECT,
    AGGREGATION_OUTPUT,
    NULL
}
