package eu.coherentpaas.mongodb.utils;

import java.io.Serializable;
import java.util.Map;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public class CollectionRecoveryWriteset implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String collectionName;
    private final Map<Object, Bson> writeset;

    public CollectionRecoveryWriteset(String collectionName, Map<Object, Bson> writeset) {
        this.collectionName = collectionName;
        this.writeset = writeset;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public Map<Object, Bson> getWriteset() {
        return writeset;
    }
}
