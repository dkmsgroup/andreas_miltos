package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MongoMVCCConnectionException extends Exception {
    
    public MongoMVCCConnectionException(String message) {
        super(message);
    }
    
    public MongoMVCCConnectionException(String message, Exception ex) {
        super(message, ex);
    }
    
    
}
