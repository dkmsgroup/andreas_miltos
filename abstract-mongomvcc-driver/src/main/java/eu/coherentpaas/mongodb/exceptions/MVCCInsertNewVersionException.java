package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCInsertNewVersionException extends Exception {
    
    public MVCCInsertNewVersionException(String message, Exception ex) {
        super(message, ex);
    }
    
    public MVCCInsertNewVersionException(String message) {
        super(message);
    }
}
