package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCNotActiveDatabaseException extends Exception {
    
    public MVCCNotActiveDatabaseException(String name, Exception ex) {
        super("Database " + name + " is not opened in the context of this transaction.", ex);
    }
    
}
