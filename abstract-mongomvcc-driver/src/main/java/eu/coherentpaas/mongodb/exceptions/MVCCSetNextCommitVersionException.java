package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCSetNextCommitVersionException extends Exception {
    
    public MVCCSetNextCommitVersionException(String message) {
        super(message);
    }
    
    public MVCCSetNextCommitVersionException(String message, Exception ex) {
        super(message, ex);
    }
    
}
