package eu.coherentpaas.mongodb.exceptions;

/**
 *
 * @author idezol
 */
public class MVCCCollectionAlreadyExistsException extends Exception {
    
    public MVCCCollectionAlreadyExistsException(String collectionName) {
        super("The " + collectionName + " collection is already opened ");
    }
    
    public MVCCCollectionAlreadyExistsException(String collectionName, Exception ex) {
        super("The " + collectionName + " collection is already opened ", ex);
    }
    
}
