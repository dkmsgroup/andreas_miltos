package eu.coherentpaas.mongomvcc;

import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;

/**
 *
 * @author idezol
 */
public interface MongoTransactionalContext {
    public long getStartTimestamp();
    public long getCommitTimestamp();
    public long getCtxId();
    public void checkForConflicts(byte [] key) throws TransactionalMongoException;
}
