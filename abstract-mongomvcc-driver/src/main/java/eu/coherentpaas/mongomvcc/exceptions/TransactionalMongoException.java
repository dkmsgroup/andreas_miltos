package eu.coherentpaas.mongomvcc.exceptions;

/**
 *
 * @author idezol
 */
public class TransactionalMongoException extends Exception {
    public TransactionalMongoException(String message) {
        super(message);
    }
    
    public TransactionalMongoException(String message, Throwable ex) {
        super(message, ex);
    }
     
}
