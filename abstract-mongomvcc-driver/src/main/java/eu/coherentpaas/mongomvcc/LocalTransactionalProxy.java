package eu.coherentpaas.mongomvcc;

import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;

/**
 *
 * @author idezol
 */
public interface LocalTransactionalProxy {
    public void beginTransacation() throws TransactionalMongoException;
    public void commitTransaction() throws TransactionalMongoException;
    public void rollbackTransaction() throws TransactionalMongoException;
}
