package eu.coherentpaas.mongomvcc;

import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;

/**
 *
 * @author idezol
 */
public interface TransasctionalMongo {
    
    public long applyMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    public void rollbackMongo(MongoTransactionalContext mongoTxnctx);

    public byte[] getMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    public void notifyMongo(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException;

    public void garbageCollectMongo(long l);
}
