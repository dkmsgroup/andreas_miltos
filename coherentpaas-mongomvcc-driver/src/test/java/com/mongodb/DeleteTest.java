package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class DeleteTest {
    private static final Logger Log = LoggerFactory.getLogger(DeleteTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
         
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(1, count);
        Log.info("found 1 element");
        
        collection.remove(obj);
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        Assert.assertEquals(0, count);
        Log.info("found 0 element before commit");
        
        database.commit();
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        Assert.assertEquals(0, count);
        Log.info("found 0 element after commit");
        
        database.rollback();
        database.close();
    }
}
