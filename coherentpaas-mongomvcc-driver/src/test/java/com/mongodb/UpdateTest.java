package com.mongodb;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateTest {
    private static final Logger Log = LoggerFactory.getLogger(UpdateTest.class);
    
    public static void main(String [] args) {
        try {
            TMMiniCluster.startMiniCluster(true, 0);
            UpdateTest test = new UpdateTest();
            UpdateTest.initDB();
            test.test();
            UpdateTest.dropDb();
        } catch (UnknownHostException ex) {
            java.util.logging.Logger.getLogger(UpdateTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransactionManagerException ex) {
            java.util.logging.Logger.getLogger(UpdateTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DataStoreException ex) {
            java.util.logging.Logger.getLogger(UpdateTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CoherentPaaSException ex) {
            java.util.logging.Logger.getLogger(UpdateTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        
       database.startTransaction();
       database.rollback();
        
        database.startTransaction();
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        Log.info("got collection named: " + collection.getName());
        DBCursor cursor = collection.find();
        BasicDBObject obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("keyValue", obj.get("keyTest"));
        
        collection.update(new BasicDBObject(), new BasicDBObject("keyTest", "other Value"));
        Log.info("value updated to \"other value\"");
        database.commit();
        
        
        
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        cursor = collection.find();
        obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("other Value", obj.get("keyTest"));
        Log.info("and retrieved value has the updated value: \"other value\"");
        
        database.rollback();
        database.close();
        
    }
}