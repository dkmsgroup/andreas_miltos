package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class DistinctTest {
    private static final Logger Log = LoggerFactory.getLogger(DistinctTest.class);
    
    private static final String FIELD = "numbering";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        populate();
    }
    
    private static void populate() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        List<DBObject> list = new ArrayList<>();
        for(int i=0; i<100; i++) 
            list.add(new BasicDBObject("key", (i+1))
                                    .append(FIELD, ((i+1)%10))
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        col.insert(list); 
        col.insert(new BasicDBObject("key", (101))
                                    .append(FIELD, 50)
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        
        
        
        
        BasicDBList list = (BasicDBList) col.distinct(FIELD);
        Log.info(list.size() + "");
        Assert.assertEquals(11, list.size());
        Log.info("11 distinct fields found");
        
        
        
        col.remove(new BasicDBObject(FIELD, 50));
        
        list = (BasicDBList) col.distinct(FIELD);
        Assert.assertEquals(10, list.size());
        Log.info("10 distinct fields found, after one removal");
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
}
