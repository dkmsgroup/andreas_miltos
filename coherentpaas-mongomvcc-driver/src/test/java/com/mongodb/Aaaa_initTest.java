package com.mongodb;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class Aaaa_initTest {
    private static final Logger Log = LoggerFactory.getLogger(Aaaa_initTest.class);
    
    
    @BeforeClass
    public static void initMiniCluster() {
        Log.info("Starting series of tests");
        TMMiniCluster.startMiniCluster(true, 0);
    }
    
    @Test
    public void report() {
        Log.info("mini cluster for tests has started");
    }
    
}
