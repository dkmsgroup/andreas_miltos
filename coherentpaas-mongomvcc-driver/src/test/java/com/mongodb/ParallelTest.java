package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ParallelTest {
    private static final Logger Log = LoggerFactory.getLogger(ParallelTest.class);
    
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
       
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        collection.find();
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        
        
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, InterruptedException {
        BlockingQueue<DBObject> a_BlockingB = new ArrayBlockingQueue<>(2);
        BlockingQueue<DBObject> b_BlockingA = new ArrayBlockingQueue<>(2);
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        TransactionA transA = new TransactionA(mongoClient, a_BlockingB, b_BlockingA);
        TransactionB transB = new TransactionB(mongoClient, a_BlockingB, b_BlockingA);
        
        ExecutorService executor = Executors.newFixedThreadPool(2);
        
        Log.info("Executing parallel transcations");
        executor.execute(transA);
        executor.execute(transB);
        
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.MINUTES);
        
        
        if(!executor.isShutdown()) {
            Log.info("here");
            executor.shutdownNow();
            Log.info("and there");
        }
    }
    
}