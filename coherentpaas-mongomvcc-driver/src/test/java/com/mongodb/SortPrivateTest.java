package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import junit.framework.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class SortPrivateTest {
    private static final Logger Log = LoggerFactory.getLogger(SortPrivateTest.class);
    private static final String FIELD = "numbering";
    private static final String FIELD2 = "numbering2";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
        
    }
    
    
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        
        int keyNumber = 0;
        col.insert(new BasicDBObject("key", ++keyNumber)
                            .append(FIELD, 1));
        col.insert(new BasicDBObject("key", ++keyNumber)
                            .append(FIELD, 2));
        col.insert(new BasicDBObject("key", ++keyNumber)
                            .append(FIELD, 3));
        col.insert(new BasicDBObject("key", ++keyNumber)
                            .append(FIELD, 4));
        col.insert(new BasicDBObject("key", ++keyNumber)
                            .append(FIELD, 5));
        database.commit();
        
        database.startTransaction();
        col = database.getCollection(TestConfiguration.COLLECTION);
        BasicDBObject sort = new BasicDBObject(FIELD, 1);
        DBCursor cursor = col.find();
        cursor.sort(sort);
        int number = Integer.MIN_VALUE;
        while(cursor.hasNext()) {
            BasicDBObject object = (BasicDBObject) cursor.next();
            Assert.assertTrue((number<=object.getInt(FIELD)));
        }
        cursor.close();
        
        
        
        col.update(new BasicDBObject("key", 3), new BasicDBObject("$set", new BasicDBObject(FIELD, 10)));
        
      
        
        cursor = col.find();
        cursor.sort(sort);
        number = Integer.MIN_VALUE;
        while(cursor.hasNext()) {
            BasicDBObject object = (BasicDBObject) cursor.next();
            Assert.assertTrue((number<=object.getInt(FIELD)));
        }
        cursor.close();
        
        database.commit();
        
        database.startTransaction();
        database.commit();
                
    }    
}
