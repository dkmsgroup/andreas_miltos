package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteNewlyInsertedTest {
   private static final Logger Log = LoggerFactory.getLogger(DeleteNewlyInsertedTest.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        collection.find();
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with no elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(0, count);
        Log.info("found no elements");
        
        for(int i=0; i<20; i++) 
            collection.insert(new BasicDBObject("key", (i+1)));
        Log.info("20 elements inserterd");
        
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 elements");
        
        count = 0;
        collection.remove(new BasicDBObject("key", 14));
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(19, count);
        Log.info("found 19 elements");
        
        count = 0;
        collection.remove(new BasicDBObject("key", new BasicDBObject("$gte", 14)));
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(13, count);
        Log.info("found 13 elements");
        
        database.rollback();
        database.close();
    }
}