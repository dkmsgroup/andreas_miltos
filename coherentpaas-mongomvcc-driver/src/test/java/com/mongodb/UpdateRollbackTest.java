package com.mongodb;

import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class UpdateRollbackTest {
    private static final Logger Log = LoggerFactory.getLogger(UpdateRollbackTest.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        Log.info("got collection named: " + collection.getName());
        DBCursor cursor = collection.find();
        BasicDBObject obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("keyValue", obj.get("keyTest"));
        
        collection.update(new BasicDBObject(), new BasicDBObject("keyTest", "other Value"));
        Log.info("value updated to \"other value\"");
        database.rollback();
        Log.info("rollback");
        
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        cursor = collection.find();
        obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("keyValue", obj.get("keyTest"));
        Log.info("and retrieved value WAS NOT updated due to the rollback");
        
        database.rollback();
        database.close();
    }
}
