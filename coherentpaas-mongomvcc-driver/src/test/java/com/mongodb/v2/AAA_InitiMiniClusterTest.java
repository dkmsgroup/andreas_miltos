package com.mongodb.v2;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class AAA_InitiMiniClusterTest {
    private static final Logger Log = LoggerFactory.getLogger(AAA_InitiMiniClusterTest.class);
    
    @BeforeClass
    public static void initMiniCluster() {
        Log.info("Starting series of tests");
        TMMiniCluster.startMiniCluster(true, 0);
    }
    
    @Test
    public void report() {
        Log.info("mini cluster for tests has started");
    }
    
}
