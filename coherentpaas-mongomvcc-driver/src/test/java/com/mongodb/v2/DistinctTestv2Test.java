package com.mongodb.v2;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class DistinctTestv2Test {
    private static final Logger Log = LoggerFactory.getLogger(DistinctTestv2Test.class);
    
    private static final String FIELD = "numbering";
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        populate();
        
    }
    
    private static void populate() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        List<DBObject> list = new ArrayList<>();
        for(int i=0; i<100; i++) 
            list.add(new BasicDBObject("key", (i+1))
                                    .append(FIELD, ((i+1)%10))
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        col.insert(list); 
        col.insert(new BasicDBObject("key", (101))
                                    .append(FIELD, 50)
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        LTMClient.getInstance().getConnection().commit();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().commit();
        
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        
        
        
        
        BasicDBList list = (BasicDBList) col.distinct(FIELD);
        Log.info(list.size() + "");
        Assert.assertEquals(11, list.size());
        Log.info("11 distinct fields found");
        
        
        
        col.remove(new BasicDBObject(FIELD, 50));
        
        list = (BasicDBList) col.distinct(FIELD);
        Assert.assertEquals(10, list.size());
        Log.info("10 distinct fields found, after one removal");
        
        LTMClient.getInstance().getConnection().commit();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().commit();
    }
}
