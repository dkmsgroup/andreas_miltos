package com.mongodb.v2;

import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author idezol
 */
public class AggregateTestv2Test {
    private static final Logger Log = LoggerFactory.getLogger(AggregateTestv2Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract coll = database.createCollection(TestConfiguration.COLLECTION, null);
        
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 1)
               .add("department", "Sales")
               .add("amount", 71)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 2)
               .add("department", "Engineering")
               .add("amount", 15)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 4)
               .add("department", "Human Resources")
               .add("amount", 5)
               .add("type", "airfare")
               .get());
        coll.insert(new BasicDBObjectBuilder()
               .add("employee", 42)
               .add("department", "Sales")
               .add("amount", 77)
               .add("type", "airfare")
               .get());
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        
        Log.info("DB init with no elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
        
    }
    
    
    //@Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract col = database.getCollection(TestConfiguration.COLLECTION);
        
        // create our pipeline operations, first with the $match
        DBObject match = new BasicDBObject("$match", new BasicDBObject("type", "airfare"));
        
        // build the $projection operation
        DBObject fields = new BasicDBObject("department", 1);
        fields.put("amount", 1);
        fields.put("_id", 0);
        DBObject project = new BasicDBObject("$project", fields );

        // Now the $group operation
        DBObject groupFields = new BasicDBObject( "_id", "$department");
        groupFields.put("average", new BasicDBObject( "$avg", "$amount"));
        DBObject group = new BasicDBObject("$group", groupFields);

        // Finally the $sort operation
        DBObject sort = new BasicDBObject("$sort", new BasicDBObject("average", -1));

        // run aggregation
        List<DBObject> pipeline = Arrays.asList(match, project, group, sort);
        
        AggregationOutput output = col.aggregate(pipeline);
        
        for(DBObject res : output.results()) {
            
            String key = (String) res.get("_id");
            if(key.equals("Sales"))
                Assert.assertEquals((double)74.0, ((Double) res.get("average")));
            if(key.equals("Engineering"))
                Assert.assertEquals((double)15.0, ((Double) res.get("average")));
            if(key.equals("Human Resources"))
                Assert.assertEquals((double)5.0, ((Double) res.get("average")));
        }
        
        col.update(new BasicDBObject("employee", 42), new BasicDBObject("$set", new BasicDBObject("amount", 100)));
        LTMClient.getInstance().getConnection().commit();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        col = database.getCollection(TestConfiguration.COLLECTION);
        
        // create our pipeline operations, first with the $match
        match = new BasicDBObject("$match", new BasicDBObject("type", "airfare"));
        
        // build the $projection operation
        fields = new BasicDBObject("department", 1);
        fields.put("amount", 1);
        fields.put("_id", 0);
        project = new BasicDBObject("$project", fields );

        // Now the $group operation
        groupFields = new BasicDBObject( "_id", "$department");
        groupFields.put("average", new BasicDBObject( "$avg", "$amount"));
        group = new BasicDBObject("$group", groupFields);

        // Finally the $sort operation
        sort = new BasicDBObject("$sort", new BasicDBObject("average", -1));

        // run aggregation
        pipeline = Arrays.asList(match, project, group, sort);
        //List<DBObject> pipeline = Arrays.asList(project, group, sort);
        output = col.aggregate(pipeline);
        
        
        for(DBObject res : output.results()) {
            
            String key = (String) res.get("_id");
            if(key.equals("Sales"))
                Assert.assertEquals((double)85.5, ((Double) res.get("average")));
            if(key.equals("Engineering"))
                Assert.assertEquals((double)15.0, ((Double) res.get("average")));
            if(key.equals("Human Resources"))
                Assert.assertEquals((double)5.0, ((Double) res.get("average")));
        }
        
        LTMClient.getInstance().getConnection().rollback();
        database.close();
    }
}
