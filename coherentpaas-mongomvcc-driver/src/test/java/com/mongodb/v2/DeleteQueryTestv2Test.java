package com.mongodb.v2;

import com.mongodb.BasicDBObject;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteQueryTestv2Test {
    private static final Logger Log = LoggerFactory.getLogger(DeleteQueryTestv2Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        for(int i=0; i<20; i++)
            collection.insert(new BasicDBObject("key", (i+1)));
        LTMClient.getInstance().getConnection().commit();
        Log.info("DB init with 20 elements");
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    
    
    @Test
    public void testScan() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 elements");
        
        BasicDBObject query = new BasicDBObject("key", new BasicDBObject("$gt", 15));
        collection.remove(query);
        
                
        cursor = collection.find();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        Assert.assertEquals(15, count);
        Log.info("found 15 element before commit");
        
        LTMClient.getInstance().getConnection().commit();
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        Assert.assertEquals(15, count);
        Log.info("found 15 element after commit");
        
        LTMClient.getInstance().getConnection().rollback();
        
        database.close();
    }
    
}