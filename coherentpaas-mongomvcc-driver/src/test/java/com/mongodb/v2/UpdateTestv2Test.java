package com.mongodb.v2;

import com.mongodb.BasicDBObject;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class UpdateTestv2Test {
    private static final Logger Log = LoggerFactory.getLogger(UpdateTestv2Test.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        
        collection.insert(new BasicDBObject("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        Log.info("got collection named: " + collection.getName());
        DBCursor cursor = collection.find();
        BasicDBObject obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("keyValue", obj.get("keyTest"));
        
        collection.update(new BasicDBObject(), new BasicDBObject("keyTest", "other Value"));
        Log.info("value updated to \"other value\"");
        LTMClient.getInstance().getConnection().commit();
        
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        cursor = collection.find();
        obj = null;
        if(cursor.hasNext())
            obj = (BasicDBObject) cursor.next();
        
        Assert.assertNotNull(obj);
        Log.info("object retrieved: " + obj.toString());
        Assert.assertEquals("other Value", obj.get("keyTest"));
        Log.info("and retrieved value has the updated value: \"other value\"");
        
        LTMClient.getInstance().getConnection().rollback();
        database.close();
        
    }
    
}
