package com.mongodb.v2;

import com.mongodb.BasicDBObject;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteNewlyInsertedTestv2Test {
    private static final Logger Log = LoggerFactory.getLogger(DeleteNewlyInsertedTestv2Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        database.startTransaction();
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        collection.find();
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with no elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, CoherentPaaSException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        LTMClient.register(mongoClient);
        LTMClient.getInstance(); //just to init the lTMClient, if hasn't been init so far
        
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().associate(mongoClient);
        
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        BasicDBObject obj = null;
        DBCursor cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(0, count);
        Log.info("found no elements");
        
        for(int i=0; i<20; i++) 
            collection.insert(new BasicDBObject("key", (i+1)));
        Log.info("20 elements inserterd");
        
        count = 0;
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 elements");
        
        count = 0;
        collection.remove(new BasicDBObject("key", 14));
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(19, count);
        Log.info("found 19 elements");
        
        count = 0;
        collection.remove(new BasicDBObject("key", new BasicDBObject("$gte", 14)));
        cursor = collection.find();
        while(cursor.hasNext()) {
            obj = (BasicDBObject) cursor.next();
            count++;
        }
        Assert.assertEquals(13, count);
        Log.info("found 13 elements");
        
        LTMClient.getInstance().getConnection().rollback();
        
        LTMClient.getInstance().getConnection().startTransaction();
        LTMClient.getInstance().getConnection().commit();
        
        database.close();
    }
    
}
