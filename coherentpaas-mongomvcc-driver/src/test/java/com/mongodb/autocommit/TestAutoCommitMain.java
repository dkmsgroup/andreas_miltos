package com.mongodb.autocommit;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol-user
 */
public class TestAutoCommitMain {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.autocommit.TestAutoCommitMain.class);
    
    public static void main(String [] args) throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        TMMiniCluster.startMiniCluster(true, 0);
        SelectTest.initDB();
        SelectTest test = new SelectTest();
        test.test();
        SelectTest.dropDb();
        TMMiniCluster.stopMiniCluster();
    }
    
   
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
        
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        DBCollectionAbstract collection = database.createCollection(TestConfiguration.COLLECTION, null);
        List<DBObject> list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new BasicDBObject("key", (i+1)));
        
        collection.insert(list);        
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        DBAbstract database = mongoClient.getDB(TestConfiguration.DATABASE);
        DBCollectionAbstract collection = database.getCollection(TestConfiguration.COLLECTION);
        long count = collection.count();
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 18)));
        Assert.assertEquals(2, count);
        Log.info("2 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 10)));
        Assert.assertEquals(10, count);
        Log.info("10 elements found");
        
        for(int i=20; i<40; i++) {
            collection.insert(new BasicDBObject("key", (i+1)));
        }
        
        
        count = collection.count();
        Assert.assertEquals(40, count);
        Log.info("40 elements found");
        
        count = collection.count(new BasicDBObject("key", new BasicDBObject("$gt", 18)));
        Assert.assertEquals(22, count);
        Log.info("22 elements found");
        
        collection.remove(new BasicDBObject("key", new BasicDBObject("$lte", 3)));
        
        count = collection.count();
        Assert.assertEquals(37, count);
        Log.info("37 elements found");
        
        
    }
}
