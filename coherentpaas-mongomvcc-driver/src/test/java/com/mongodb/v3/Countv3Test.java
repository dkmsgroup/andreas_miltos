package com.mongodb.v3;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class Countv3Test {
    private static final Logger Log = LoggerFactory.getLogger(Countv3Test.class);
    
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        List<Document> list = new ArrayList<>();
        for(int i=0; i<10; i++)
            list.add(new Document("key", (i+1)));
        collection.insertMany(list);
        database.commit();
        
        for(int i=10; i<20; i++)
            collection.insertOne(new Document("key", (i+1)));
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        long count = collection.count();
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        
        count = collection.count(new Document("key", new Document("$gt", 18)));
        Assert.assertEquals(2, count);
        Log.info("2 elements found");
        
        count = collection.count(new Document("key", new Document("$gt", 10)));
        Assert.assertEquals(10, count);
        Log.info("10 elements found");
        
        List<Document> list = new ArrayList<>();
        for(int i=20; i<40; i++) {
            list.add(new Document("key", (i+1)));
        }
        collection.insertMany(list);
        
        count = collection.count();
        Assert.assertEquals(40, count);
        Log.info("40 elements found");
        
        count = collection.count(new Document("key", new Document("$gt", 18)));
        Assert.assertEquals(22, count);
        Log.info("22 elements found");
        
        
        
        
        database.rollback();
        
        database.close();
    }
}
