package com.mongodb.v3.autocommit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class FindOneAndUpdatev3Test {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.v3.FindOneAndUpdatev3Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        List<Document> list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new Document("key", (i+1))
                    .append("attr", "init"));
        collection.insertMany(list);
        
        Log.info("DB init with 20 elements");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");        
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        Document obj = null;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(!obj.getString("attr").equals("init"))
                Assert.fail("not valid init parameter");
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("found 20 element");
        
        
        
        Document query = new Document("key", new Document("$lte", 5));
        Document set = new Document("$set", new Document("attr", "other"));
        Document sort = new Document("key", 1);
        FindOneAndUpdateOptions options = new FindOneAndUpdateOptions();
        options.sort(sort);
        options.returnDocument(ReturnDocument.AFTER);
        
        Document result = collection.findOneAndUpdate(query, set, options);
        Assert.assertNotNull(result);
        
        count = 0;
        cursor = collection.find().sort(sort).iterator();
        while(cursor.hasNext()) {
            Document _next = cursor.next();
            if(_next.getString("attr").equals("other")) {
                count++;
                //Assert.assertEquals(result, _next);
            }
        }
        Assert.assertEquals(1, count);
        Log.info("one document found updated");
        
        sort = new Document("key", -1);
        query = new Document("key", new Document("$gte", 17));
        cursor = collection.find(query).sort(sort).iterator();
        Document beforeDocument = null;
        if(cursor.hasNext()) 
            beforeDocument = cursor.next();
        else
            Assert.fail("no returned document");
        
        options = new FindOneAndUpdateOptions();
        options.sort(sort);
        options.returnDocument(ReturnDocument.BEFORE);
        
        result = collection.findOneAndUpdate(query, set, options);
        Assert.assertNotNull(result);
        Assert.assertEquals(result, beforeDocument);
        
        
        count = 0;
        cursor = collection.find().sort(sort).iterator();
        while(cursor.hasNext()) {
            Document _next = cursor.next();
            if(_next.getString("attr").equals("other")) {
                count++;
            }
        }
        Assert.assertEquals(2, count);
        Log.info("one document found updated");
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        cursor = collection.find().iterator();
        count=0;
        while(cursor.hasNext()) {
            Document _next = cursor.next();
            if(_next.getString("attr").equals("other")) {
                count++;
            }
        }
        Assert.assertEquals(2, count);
        Log.info("two document found updated");
        
        
        database.close();
    }
}
