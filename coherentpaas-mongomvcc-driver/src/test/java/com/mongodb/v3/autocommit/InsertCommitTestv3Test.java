package com.mongodb.v3.autocommit;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertCommitTestv3Test {
    private static final Logger Log = LoggerFactory.getLogger(com.mongodb.v3.InsertCommitTestv3Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        mongoDatabase.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION);
        Document doc = new Document("keyTest", "keyValue");
        mongoCollection.insertOne(doc);
        
        Log.info("DB init");
        mongoDatabase.close();
    }
    
    @Test
    public void doTest() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance()
                .createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION);
        Document doc = new Document("testing", "this value");
        mongoCollection.insertOne(doc);
        MongoCursor<Document> cursor = mongoCollection.find().iterator();
        int count = 0;
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Assert.assertEquals(2, count);
        Log.info("two elements found before commit.");
                
        mongoDatabase = mongoClient.getDatabase(TestConfiguration.DATABASE);
        
        mongoCollection = mongoDatabase.getCollection(TestConfiguration.COLLECTION);
        
        count = 0;
        cursor = mongoCollection.find(new Document("testing", "this value"), Document.class).iterator();
        while(cursor.hasNext()) {
            Document result = cursor.next();
            count++;
        }
        
        Assert.assertEquals(1, count);
        Log.info("element found after commit.");
        
        mongoDatabase.close();
    }
    
    
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
    }
}
