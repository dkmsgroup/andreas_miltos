package com.mongodb.v3;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class ReplaceOnePrivatev3Test {
    private static final Logger Log = LoggerFactory.getLogger(ReplaceOnePrivatev3Test.class);
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");        
    }
    
    @Test
    public void test() throws TransactionManagerException, UnknownHostException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        Log.info("got collection named: " + collection.getNamespace());
        Document obj = null;
        
        
        List<Document> list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new Document("key", (i+1))
                            .append("attr", "init"));
        collection.insertMany(list);
        
        int count = 0;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(!obj.getString("attr").equals("init"))
                Assert.fail("attribute is not set to init");
            count++;
        }
        Assert.assertEquals(20, count);
        Log.info("20 elements found");
        
        Document query = new Document("key", new Document("$lte", 3));
        Document replace = new Document("key", 100).append("attr", "YOLO");
        
        UpdateResult result = collection.replaceOne(query, replace);
        Assert.assertEquals(1, result.getModifiedCount());
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("YOLO")) {
                int value = obj.getInteger("key");
                Assert.assertEquals(100, value);
                count++;            
            }
        }
        Assert.assertEquals(1, count);
        Log.info("1 elements found updated");
        
        database.rollback();
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        list = new ArrayList<>();
        for(int i=0; i<20; i++)
            list.add(new Document("key", (i+1))
                            .append("attr", "init"));
        collection.insertMany(list);
        
        database.commit();
        database.startTransaction();
        
        
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("YOLO")) {
                int value = obj.getInteger("key");
                Assert.assertEquals(100, value);
                count++;            
            }
        }
        Assert.assertEquals(0, count);
        Log.info("0 elements found updated");
        
        
        result = collection.replaceOne(query, replace);
        Assert.assertEquals(1, result.getModifiedCount());
        
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("YOLO")) {
                int value = obj.getInteger("key");
                Assert.assertEquals(100, value);
                count++;            
            }
        }
        Assert.assertEquals(1, count);
        Log.info("1 elements found updated");
        
        database.commit();
        database.startTransaction();
        
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            if(obj.getString("attr").equals("YOLO")) {
                int value = obj.getInteger("key");
                Assert.assertEquals(100, value);
                count++;            
            }
        }
        Assert.assertEquals(1, count);
        Log.info("1 elements found updated");
        
        database.rollback();
        
        
        database.close();        
    }
}
