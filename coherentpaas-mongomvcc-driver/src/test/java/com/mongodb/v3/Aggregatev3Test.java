package com.mongodb.v3;

import com.mongodb.AggregateTest;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBAbstract;
import com.mongodb.DBCollectionAbstract;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import junit.framework.Assert;
import org.bson.BsonDocument;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class Aggregatev3Test {
    private static final Logger Log = LoggerFactory.getLogger(Aggregatev3Test.class);
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
       
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> coll = database.getCollection(TestConfiguration.COLLECTION);
        
        coll.insertOne(new Document()
               .append("employee", 1)
               .append("department", "Sales")
               .append("amount", 71)
               .append("type", "airfare"));
        coll.insertOne(new Document()
               .append("employee", 2)
               .append("department", "Engineering")
               .append("amount", 15)
               .append("type", "airfare"));
        coll.insertOne(new Document()
               .append("employee", 4)
               .append("department", "Human Resources")
               .append("amount", 5)
               .append("type", "airfare"));
        coll.insertOne(new Document()
               .append("employee", 42)
               .append("department", "Sales")
               .append("amount", 77)
               .append("type", "airfare"));
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        
        Log.info("DB init with 4 elements to start aggregation test");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        
        // create our pipeline operations, first with the $match
        Document match = new Document("$match", new Document("type", "airfare"));
        
        // build the $projection operation
        Document fields = new Document("department", 1);
        fields.put("amount", 1);
        fields.put("_id", 0);
        Document project = new Document("$project", fields );

        // Now the $group operation
        Document groupFields = new Document( "_id", "$department");
        groupFields.put("average", new Document( "$avg", "$amount"));
        Document group = new Document("$group", groupFields);

        // Finally the $sort operation
        Document sort = new Document("$sort", new BasicDBObject("average", -1));

        // run aggregation
        List<Document> pipeline = Arrays.asList(match, project, group, sort);
        
        //AggregationOutput output = col.aggregate(pipeline);
        AggregateIterable<Document> iterable = col.aggregate(pipeline, Document.class);
        MongoCursor<Document> cursor = iterable.iterator();
        
        while(cursor.hasNext()) {
            Document res = cursor.next();
            String key = (String) res.getString("_id");
            if(key.equals("Sales"))
                Assert.assertEquals((double)74.0, ((Double) res.getDouble("average")));
            if(key.equals("Engineering"))
                Assert.assertEquals((double)15.0, ((Double) res.getDouble("average")));
            if(key.equals("Human Resources"))
                Assert.assertEquals((double)5.0, ((Double) res.getDouble("average")));
        }
        
        
        
        
        database.commit();
        database.close();        
    }
}
