package com.mongodb.v3.intkeys;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class InsertCommitTest {
    private static final Logger Log = LoggerFactory.getLogger(InsertCommitTest.class);
  
    
    @Test
    public void testIntegerKeys() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        for(int i=1; i<11 ; i++) {
            Document dBObject = new Document("keyTest", "keyValue" + i)
                    .append("_id", i);
            collection.insertOne(dBObject);
        }
        Log.info("Added {} documents", 10);
        
        MongoCursor cursor = collection.find(new Document("_id", new Document("$lte", 5))).iterator();
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
            
        }
        
        Assert.assertEquals(5, count);
        
        
        database.commit();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        
        collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        cursor = collection.find(new Document("_id", new Document("$lte", 5))).iterator();
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
        }
        
        Assert.assertEquals(5, count);
        
        database.commit();
    }
    
    @Test
    public void testIntegerKeysRollback() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        for(int i=1; i<11 ; i++) {
            Document dBObject = new Document("keyTest", "keyValue" + i)
                    .append("_id", i);
            collection.insertOne(dBObject);
        }
        Log.info("Added {} documents", 10);
        
        MongoCursor cursor = collection.find(new Document("_id", new Document("$lte", 5))).iterator();
        int count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
            
        }
        
        Assert.assertEquals(5, count);
        
        
        database.rollback();
        database.startTransaction();
        database.commit();
        
        database.startTransaction();
        
        cursor = collection.find(new Document("_id", new Document("$lte", 5))).iterator();
        count = 0;
        while(cursor.hasNext()) {
            Log.info("{}: {}", ++count, (Document) cursor.next());
        }
        
        Assert.assertEquals(0, count);
        
        database.commit();
    }
}
