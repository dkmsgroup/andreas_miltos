package com.mongodb.v3.intkeys;

import com.mongodb.MongoClient;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.mongodb.config.MongoMVCCFields;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class DeleteTest {
    private static final Logger Log = LoggerFactory.getLogger(DeleteTest.class);

    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException {
                
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        for(int i=0; i<20; i++)
            collection.insertOne(new Document("key", (i+1)).append(MongoMVCCFields._ID, (i+1)));

        database.commit();

        database.startTransaction();
        database.commit();

        Log.info("DB init with 20 elements");
        
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection collection = database.getCollection(TestConfiguration.COLLECTION, Document.class);
        MongoCursor cursor = collection.find(new Document(MongoMVCCFields._ID, new Document("$lte", 5))).iterator();
        int count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(5, count);
        
        collection.deleteOne(new Document(MongoMVCCFields._ID, 18));
        
        cursor = collection.find().iterator();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(19, count);
        
        database.rollback();
        database.startTransaction();
        
        
        cursor = collection.find().iterator();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(20, count);
        
        
        collection.deleteMany(new BasicDBObject(MongoMVCCFields._ID, new BasicDBObject("$lte", 3)));
        
        cursor = collection.find(new BasicDBObject(MongoMVCCFields._ID, new BasicDBObject("$lte", 5))).iterator();
        count = 0;
        while(cursor.hasNext()) {
            count++;
            cursor.next();
        }
        
        Assert.assertEquals(2, count);
        
        database.commit();
        database.startTransaction();
        database.commit();
        
    }
}
