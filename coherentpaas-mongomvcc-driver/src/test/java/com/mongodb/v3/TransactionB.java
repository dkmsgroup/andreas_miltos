package com.mongodb.v3;

import com.mongodb.MongoClient;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class TransactionB implements Runnable {
    private static final Logger Log = LoggerFactory.getLogger(TransactionB.class);
    
    private BlockingQueue<Document> a_BlockingB;
    private BlockingQueue<Document> b_BlockingA;
    private MongoClient mongoClient;
    
    public TransactionB(MongoClient mongoClient, BlockingQueue<Document> a_BlockingB, BlockingQueue<Document> b_BlockingA) {
        this.mongoClient = mongoClient;
        this.a_BlockingB = a_BlockingB;
        this.b_BlockingA = b_BlockingA;
    }
    
    @Override
    public void run() {
        
        try {
            int count = 0;
            MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
            database.startTransaction();
            MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            insertRecords(collection);
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            
            unBlockA();
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(10, count);
            commit(database);
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(20, count);
            commit(database);
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            
            unBlockA();
            waitForA();
            
            count = readRecords(collection);
            Assert.assertEquals(20, count);
            commit(database);
            
            unBlockA();
            waitForA();
            
            database.startTransaction();
            collection = database.getCollection(TestConfiguration.COLLECTION);
            count = readRecords(collection);
            Assert.assertEquals(0, count);
            
            
            commit(database);
            database.close();
            
            Log.info("TranscationB is closing");
            
        } catch (Exception ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Assert.fail(ex.getMessage());
        } finally {
            Thread.currentThread().interrupt();
        }
    }
    
    private void removeAll(MongoCollection<Document> collection) {
        collection.deleteMany(new Document());
        Log.info("TransactionB removes everything");
    }
    
    private void insertRecords(MongoCollection<Document> collection) {
        List<Document> list = new ArrayList<>();
        for(int i=0; i<10; i++) {
            list.add(new Document("key", (i+1))
                    .append("transaction", "B")
                    .append("value", RandomStringUtils.randomAlphabetic(10))
                    );
        }
        collection.insertMany(list);
        Log.info("TransactionB added 10 elements");
    }
    
    
    private int readRecords(MongoCollection<Document> collection) {
        int count = 0;
        MongoCursor cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            cursor.next();
            count++;
        }
        
        Log.info("TranscationB read " + count + " elements");
        return count;
    }
    
    private void commit(MongoDatabase database) throws TransactionManagerException, DataStoreException {
        database.commit();
        Log.info("TransactionB commits");
    }
    
    
    private void waitForA() {
        try {
            Log.info("TranasctionB blocks");
            Document obk = this.a_BlockingB.take();
            Log.info("TranasctionB unblocks");
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }
    
    private void unBlockA() {
        try {
            Log.info("TranscationB is unblocking A");
            this.b_BlockingA.put(new Document());
        } catch (InterruptedException ex) {
            Log.error(ex.getClass().getName() + ". " + ex.getMessage());
            Thread.currentThread().interrupt();
        }
    }
}
