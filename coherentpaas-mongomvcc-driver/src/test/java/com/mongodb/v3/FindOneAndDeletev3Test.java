package com.mongodb.v3;

import com.mongodb.DeleteTest;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import eu.coherentpaas.transactionmanager.minicluster.TMMiniCluster;
import java.io.IOException;
import java.net.UnknownHostException;
import junit.framework.Assert;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class FindOneAndDeletev3Test {
    private static final Logger Log = LoggerFactory.getLogger(FindOneAndDeletev3Test.class);
    
    
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        
        collection.insertOne(new Document("keyTest", "keyValue"));
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        Log.info("DB init");
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");        
    }
    
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        Document obj = null;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(1, count);
        Log.info("found 1 element");
        
        
        
        Document deletedObject = collection.findOneAndDelete(null);
        Assert.assertNotNull(deletedObject);
        Log.info("Returned one mathed document that was deleted: " + deletedObject.toJson());
        
        Assert.assertEquals(deletedObject, obj);
        Log.info("The returned matched document equals the one to be deleted");
        
        count = 0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        
        Assert.assertEquals(0, count);
        Log.info("No document found after deletion");
        
        database.commit();
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        
        Assert.assertEquals(0, count);
        Log.info("No document found after commit");
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
    
    @Test
    public void testMoreNow() throws TransactionManagerException, UnknownHostException, DataStoreException, IOException {
        Log.info("Adding a new to remove from private writeset");
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> collection = database.getCollection(TestConfiguration.COLLECTION);
        int count = 0;
        Document obj = null;
        MongoCursor<Document> cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(0, count);
        Log.info("found 0 element");
        
        collection.insertOne(new Document("key", "value"));
        
        obj = null;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        Assert.assertEquals(1, count);
        Log.info("found 1 element");
        
        Document deletedObject = collection.findOneAndDelete(null);
        Assert.assertNotNull(deletedObject);
        Log.info("Returned one mathed document that was deleted: " + deletedObject.toJson());
        
        Assert.assertEquals(deletedObject, obj);
        Log.info("The returned matched document equals the one to be deleted");
        
        count = 0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        
        Assert.assertEquals(0, count);
        Log.info("No document found after deletion");
        
        database.commit();
        
        database.startTransaction();
        collection = database.getCollection(TestConfiguration.COLLECTION);
        count=0;
        cursor = collection.find().iterator();
        while(cursor.hasNext()) {
            obj = cursor.next();
            count++;
        }
        
        Assert.assertEquals(0, count);
        Log.info("No document found after commit");
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
        
        
    }
}
