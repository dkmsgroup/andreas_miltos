package com.mongodb.v3;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientFactory;
import com.mongodb.TestConfiguration;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.apache.commons.lang3.RandomStringUtils;
import org.bson.Document;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class Distinctv3Test {
    private static final Logger Log = LoggerFactory.getLogger(Distinctv3Test.class);
    
    private static final String FIELD = "numbering";
    
    public static void main(String [] args) {
        try {
            Distinctv3Test.initDB();
            Distinctv3Test test = new Distinctv3Test();
            test.test();
            Distinctv3Test.dropDb();
        } catch(TransactionManagerException | IOException | DataStoreException  ex) {
            Log.error("{}: {}", ex.getClass().getName(), ex.getMessage());            
        }
    }
    
    @BeforeClass
    public static void initDB() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        TMMiniCluster.startMiniCluster(true, 0);
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        populate();
    }
    
    private static void populate() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        database.createCollection(TestConfiguration.COLLECTION);
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        List<Document> list = new ArrayList<>();
        for(int i=0; i<100; i++) 
            list.add(new Document("key", (i+1))
                                    .append(FIELD, ((i+1)%10))
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        col.insertMany(list); 
        col.insertOne(new Document("key", (101))
                                    .append(FIELD, 50)
                                    .append("texting", RandomStringUtils.randomAlphabetic(50)));
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
    
     
     
    @AfterClass
    public static void dropDb() throws UnknownHostException, TransactionManagerException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        Log.info("datastoreID: " + mongoClient.getDataStoreID());
        Log.info("now will drop the database");
        mongoClient.dropDatabase(TestConfiguration.DATABASE);
        Log.info("database dropped");
        
    }
    
    @Test
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient(TestConfiguration.HOSTNAME, TestConfiguration.PORT);
        MongoDatabase database = mongoClient.getDatabase(TestConfiguration.DATABASE);
        database.startTransaction();
        MongoCollection<Document> col = database.getCollection(TestConfiguration.COLLECTION);
        
        
        
        DistinctIterable<Document> iterable = col.distinct(FIELD, Document.class);
        List<Document> list = new ArrayList<>();
        iterable.into(list);
        int count = 0;
        for(Document document : list) {
            Log.info(document.toJson());
            count++;
        }
        
        //Document list = (BasicDBList) col.distinct(FIELD);
        Log.info(list.size() + "");
        Assert.assertEquals(11, list.size());
        Assert.assertEquals(11, count);
        Log.info("11 distinct fields found");
        
        
        col.deleteOne(new Document(FIELD, 50));
        
        list = new ArrayList<>();
        col.distinct(FIELD, Document.class).into(list);
        
        Assert.assertEquals(10, list.size());
        Log.info("10 distinct fields found, after one removal");
        
        count=0;
        for(Document document : list) {
            Log.info(document.toJson());
            count++;
        }
        
        Assert.assertEquals(10, count);
        
        database.commit();
        
        database.startTransaction();
        database.commit();
        
        database.close();
    }
}
