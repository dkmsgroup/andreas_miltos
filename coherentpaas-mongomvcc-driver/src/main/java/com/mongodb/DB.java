package com.mongodb;


import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.WrapperTransactionContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 *
 * @author idezol
 */
public class DB extends DBAbstract {
    protected final TransactionalDSClient transactionalDSClient;
    
    protected DB(long dbID, String databaseName, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoClient, DeprecatedDB deprecatedDb) {
        super(dbID, databaseName, _depracedMongoClient, mongoClient, deprecatedDb);
        this.transactionalDSClient = mongoClient;
    }

    public TransactionalDSClient getTransactionalDSClient() {
        return transactionalDSClient;
    }

    
    
    /**
     * <p>Creates a collection with a given name and options. If the collection already exists, this throws a 
     * {@code CommandFailureException}.</p>
     *
     * <p>Possible options:</p>
     * <ul> 
     *     <li> <b>capped</b> ({@code boolean}) - Enables a collection cap. False by default. If enabled, 
     *     you must specify a size parameter. </li> 
     *     <li> <b>size</b> ({@code int}) - If capped is true, size specifies a maximum size in bytes for the capped collection. When 
     *     capped is false, you may use size to preallocate space. </li> 
     *     <li> <b>max</b> ({@code int}) -   Optional. Specifies a maximum "cap" in number of documents for capped collections. You must
     *     also specify size when specifying max. </li>
     * </ul>
     * <p>Note that if the {@code options} parameter is {@code null}, the creation will be deferred to when the collection is written
     * to.</p>
     *
     * @param name    the name of the collection to return
     * @param options options
     * @return the collection
     * @throws MongoException
     * @mongodb.driver.manual reference/method/db.createCollection/ createCollection()
     */
    
    @Override
    public DBCollection createCollection(final String name, final DBObject options) {
        
        if(options==null) {
            if(!this.dbOpenCollections.containsKey(name)) {
                DeprecatedDBCollection depracedCollection = this._db.getCollection(name);
                DBCollection dbCollection = new DBCollection(name, depracedCollection, this.mongoTransactionalContexct, this, true);
                this.dbOpenCollections.putIfAbsent(name, dbCollection);
                return (DBCollection) this.dbOpenCollections.get(name);
            } 
        }
        
        if(!this.dbOpenCollections.containsKey(name)) {
            DeprecatedDBCollection depracedCollection = this._db.createCollection(name, options);
            DBCollection dbCollection = new DBCollection(name, depracedCollection, this.mongoTransactionalContexct, this);
            this.dbOpenCollections.putIfAbsent(name, dbCollection);
        } 
        
        return (DBCollection) this.dbOpenCollections.get(name);
    }
    
    /**
     * Gets a collection with a given name.
     *
     * @param name the name of the collection to return
     * @return the collection
     */
    @Override
    public DBCollection getCollection( String name ){
        if(!this.dbOpenCollections.containsKey(name)) {
            DeprecatedDBCollection depracedCollection = this._db.getCollection(name);
            DBCollection dbCollection = new DBCollection(name, depracedCollection, this.mongoTransactionalContexct, this);
            this.dbOpenCollections.putIfAbsent(name, dbCollection);
        } 
        
        return (DBCollection) this.dbOpenCollections.get(name);
    }
    
    
    @Override
    public DBCollectionAbstract createCollectionForWrapper(final String name) {
        
        if(!this.dbOpenCollections.containsKey(name)) {
            DeprecatedDBCollection depracedCollection = this._db.getCollection(name);
            DBCollection dbCollection = new DBCollection(name, depracedCollection, this.mongoTransactionalContexct, this, false);
            this.dbOpenCollections.putIfAbsent(name, dbCollection);
            return (DBCollectionAbstract) this.dbOpenCollections.get(name);
        }
        
        return (DBCollectionAbstract) this.dbOpenCollections.get(name);
    }
    
    @Override
    public void startTransaction(MongoTransactionalContext txnContext)  {
        if((this.mongoTransactionalContexct!=null)&&(this.mongoTransactionalContexct.getCtxId()==txnContext.getCtxId()))
                return; //already assigned
        
        if(!(txnContext instanceof WrapperTransactionContext)) {
             try {
                LTMClient.getInstance().getConnection().associate((this.transactionalDSClient));
            } catch (CoherentPaaSException ex) {
                //throw new TransactionManagerException(ex);
                Log.error("Cannot start transaction {}: {}", ex.getClass().getName(), ex.getMessage());
            }
        } else {
            //if coming from the wrapper, then propagate the transactional context to all its open collections
            if(!this.dbOpenCollections.isEmpty()) {
                Iterator<Entry<String, Abstract_MongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
                while(it.hasNext()) {
                    Abstract_MongoCollectionImpl collection = it.next().getValue();
                    collection.mongoTransactionalContext = txnContext;
                }
            }
        }

        this.mongoTransactionalContexct = txnContext;
        this.mongoMVCCClient.addToOpenTransactions(this, txnContext.getCtxId());
    }
    
    @Override
    public  void startTransaction() {
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            TxnCtx ctx = LTMClient.getInstance().getConnection().getTxnCtx();
            startTransaction(new CoherentMongoTransactionalContext(ctx));
        } catch(TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
        
    }
    
    @Override
    public void commit() {
        try {
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    @Override
    public void rollback() {
        try {
            LTMClient.getInstance().getConnection().rollback();
        } catch (TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
}
