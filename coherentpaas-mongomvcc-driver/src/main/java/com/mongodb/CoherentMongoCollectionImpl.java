package com.mongodb;

import com.mongodb.client.MongoCollection;
import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public class CoherentMongoCollectionImpl<TDocument>  extends Cp_MongoCollectionImpl {
    
    
    /**
     * constructor of this class
     * sets the collection's name and the DBCollection object of MongoDB driver
     *
     * @param name
     * @param documentClass
     * @param dbDepracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @dochub CpMongoCollectionImpl
     */
    protected CoherentMongoCollectionImpl(final String name, final Class<TDocument> documentClass, final MongoCollection<TDocument> dbDepracedCollection, final MongoTransactionalContext mongoTransactionalContext, final Abstract_MongoDatabaseImpl mongoMVCCDB) { 
        super(name, documentClass, dbDepracedCollection, mongoTransactionalContext, mongoMVCCDB);
    }
    
    protected CoherentMongoCollectionImpl(final String name, final Class<TDocument> documentClass,  final MongoCollection<TDocument> dbDepracedCollection, final MongoTransactionalContext mongoTransactionalContext, final Abstract_MongoDatabaseImpl mongoMVCCDB, ConcurrentMap<Object, Bson> writeset, ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply) { 
        super(name, documentClass, dbDepracedCollection, mongoTransactionalContext, mongoMVCCDB, writeset, pendingWritesetsToApply);
    }

    @Override
    protected void _getTransactionalContext() throws CoherentPaaSException {
        if(this.mongoTransactionalContext == null) {
            if(!LTMClient.getInstance().isTransactionOpen()) {
                //if not yet open, then set to autocommit
                LTMClient.getInstance().getConnection().startTransaction();
                this.isAutoCommit.set(true);
            }
             CoherentMongoTransactionalContext transactionalCtx = new CoherentMongoTransactionalContext(
                LTMClient.getInstance().getConnection().getTxnCtx());
             this.abstractDB.startTransaction(transactionalCtx);
             this.mongoTransactionalContext = transactionalCtx;
         }
    }

    @Override
    protected void clearTxnCtx() {
         this.mongoTransactionalContext = null;
        if(this.hasUpdates)
            this.hasUpdates=false;
        if(!this.writeset.isEmpty())
            this.writeset.clear();
        if(this.isAutoCommit.get())
            this.isAutoCommit.set(false);
    }

    @Override
    protected void _close() {
        this.mongoTransactionalContext = null;
        this.isClosed.set(true);
        this.writeset.clear(); 
    }
    
}
