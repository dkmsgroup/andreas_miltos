package com.mongodb;

import eu.coherentpaas.mongodb.utils.WriteSetToApply;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import java.util.concurrent.ConcurrentMap;
import org.bson.conversions.Bson;

/**
 *
 * @author idezol
 */
public abstract class AbstractCoherentMongoCollectionImpl extends Abstract_MongoCollectionImpl {
    
    protected AbstractCoherentMongoCollectionImpl(Abstract_MongoDatabaseImpl abstractDB, String collectionName, MongoTransactionalContext mongoTransactionalContext, ConcurrentMap<Object, Bson> writeset, ConcurrentMap<Long, WriteSetToApply> pendingWritesetsToApply) {
        super(abstractDB, collectionName, mongoTransactionalContext, writeset, pendingWritesetsToApply);
    }

    private AbstractCoherentMongoCollectionImpl(Abstract_MongoDatabaseImpl abstractDB, String collectionName, MongoTransactionalContext mongoTransactionalContext) {
        super(abstractDB, collectionName, mongoTransactionalContext);
    }

    @Override
    protected abstract void _applyWriteSet(long tid, long commitTmp) throws MongoException;

    @Override
    protected abstract void _rollback(long tid) throws MongoException;
    
    @Override
    protected void clearTxnCtx() {
        this.mongoTransactionalContext = null;
        if(this.hasUpdates)
            this.hasUpdates=false;
        if(!this.writeset.isEmpty())
            this.writeset.clear();
        if(this.isAutoCommit.get())
            this.isAutoCommit.set(false);
    }
    
    @Override
    protected void _close() {
        this.mongoTransactionalContext = null;
        this.isClosed.set(true);
        this.writeset.clear();      
        
    }
    
    /**
     * Checks if the transactional context is set. If not, then it checks if there is an active transaction and re-sets it
     * If no active transaction, then opens a new one and sets to autocommit
     * 
     * @throws CoherentPaaSException 
     */
    @Override
    protected void _getTransactionalContext() throws CoherentPaaSException {
        
        if(this.mongoTransactionalContext == null) {
            if(!LTMClient.getInstance().isTransactionOpen()) {
                //if not yet open, then set to autocommit
                LTMClient.getInstance().getConnection().startTransaction();
                this.isAutoCommit.set(true);
            }
             CoherentMongoTransactionalContext transactionalCtx = new CoherentMongoTransactionalContext(
                LTMClient.getInstance().getConnection().getTxnCtx());
             this.mongoTransactionalContext = transactionalCtx;
             this.abstractDB.startTransaction(transactionalCtx);
         }
    }
    
}
