package com.mongodb;

import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author idezol
 */
public class MongoClientFactory {
    private static final Logger Log = LoggerFactory.getLogger(MongoClientFactory.class);
    private final ReentrantLock lock = new ReentrantLock();
    private static volatile MongoClient client;
    
    //singleton, make constructor private
    private MongoClientFactory() {}
    
    private static class MongoClientFactorylHolder {
        private static final MongoClientFactory INSTANCE = new MongoClientFactory();
    }
    
    public static MongoClientFactory getInstance() {
        return MongoClientFactorylHolder.INSTANCE;
    }
    
    
    /**
     * Creates an instance based on a (single) mongodb node (localhost, default port).
     *
     * @return 
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     */
    public MongoClient createClient() throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host server to connect to in format host[:port]
     * @return 
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     */
    public MongoClient createClient(final String host) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(host, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node (default port).
     *
     * @param host    server to connect to in format host[:port]
     * @param options default query options
     * @return 
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     */
    public MongoClient createClient(final String host, final MongoClientOptions options) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(host, options, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host the database's host address
     * @param port the port on which the database is running
     * @return 
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     */
    public MongoClient createClient(final String host, final int port) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(host, port, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node
     *
     * @param addr the database address
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final ServerAddress addr) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(addr, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node and a list of credentials
     *
     * @param addr the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    public MongoClient createClient(final ServerAddress addr, final List<MongoCredential> credentialsList) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(addr, credentialsList, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param options default options
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final ServerAddress addr, final MongoClientOptions options) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(addr, options, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
   
    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr
     * @param credentialsList
     * @param options
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final ServerAddress addr, final List<MongoCredential> credentialsList, final MongoClientOptions options) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(addr, credentialsList, options, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of mongod
     *              servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final List<ServerAddress> seeds) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(seeds, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds           Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list
     *                        of mongod servers in the same replica set or a list of mongos servers in the same sharded cluster. \
     * @param credentialsList the list of credentials used to authenticate all connections
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    public MongoClient createClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(seeds, credentialsList, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param options default options
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final List<ServerAddress> seeds, final MongoClientOptions options) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(seeds, options, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param credentialsList
     * @param options default options
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, final MongoClientOptions options) throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(seeds, credentialsList, options, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param uri
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClient(final MongoClientURI uri)  throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(uri, true);
                        LTMClient.register(client);
                        LTMClient.getInstance();
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param uri
     * @return 
     * @throws java.net.UnknownHostException
     * @throws eu.coherentpaas.transactionmanager.exception.TransactionManagerException
     * @see com.mongodb.ServerAddress
     */
    public MongoClient createClientForLoadingYCSB(final MongoClientURI uri, boolean initLTMClient)  throws UnknownHostException, TransactionManagerException {
        if(client==null) {
            synchronized(this) {
                if(client==null) {
                    try {
                        client = new MongoClient(uri, true);
                        if(initLTMClient) {
                            LTMClient.register(client);
                            LTMClient.getInstance();
                        }
                    } catch(TransactionManagerException ex) {
                        client = null;
                        throw ex;
                    } 
                }
            }
        } else 
            Log.warn("MongoClient has already beeen created yet. Factory will return the existed");
        
        return client;
    }
    
    public void closeClient() {
        if(client==null)
            return;
        
        synchronized(this) {
            client.close();
            client = null;
        }
    }
    
    public MongoClient getMongoClient() {        
        return client;
    }
    
    
    
    public Map<String, Object> createDocument() {
        return new BasicDBObject();
    }

    
    public Map<String, Object> createDocument(final String key, final Object value) {
        return new BasicDBObject(key, value);
    }

    
    public List<Object> createList() {
        return new BasicDBList();
    }
    
}
