package com.mongodb;

import static com.mongodb.DBAbstract.Log;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.WrapperTransactionContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.Iterator;
import java.util.Map;
import org.bson.Document;

/**
 *
 * @author idezol
 */
public class CpMongoDatabaseImpl extends CpMongoDatabaseImplAbstract {
    protected final TransactionalDSClient transactionalDSClient;
    
    protected CpMongoDatabaseImpl(long dbID, String databaseName, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoClient, MongoDatabase _deprecatedDb) {
        super(dbID, databaseName, _depracedMongoClient, mongoClient, _deprecatedDb);
        this.transactionalDSClient = mongoClient;
    }
    
    @Override
    public <TDocument> MongoCollection<TDocument> getCollection(String collectionName, Class<TDocument> documentClass) {
        if(!this.dbOpenCollections.containsKey(collectionName)) {
            MongoCollection<TDocument> deprecatedCollection = this._deprecatedDb.getCollection(collectionName, documentClass);
            CoherentMongoCollectionImpl mongoCollection = new CoherentMongoCollectionImpl(collectionName, Document.class, deprecatedCollection, this.mongoTransactionalContexct, this);
            this.dbOpenCollections.putIfAbsent(collectionName, mongoCollection);
        }
        return (Cp_MongoCollectionImpl<TDocument>) this.dbOpenCollections.get(collectionName);
    }

    @Override
    public void startTransaction(MongoTransactionalContext txnContext) {
        if((this.mongoTransactionalContexct!=null)&&(this.mongoTransactionalContexct.getCtxId()==txnContext.getCtxId()))
                return; //already assigned
        
        if(!(txnContext instanceof WrapperTransactionContext)) {
             try {
                LTMClient.getInstance().getConnection().associate(this.transactionalDSClient);
            } catch (CoherentPaaSException ex) {
                //throw new TransactionManagerException(ex);
                Log.error("Cannot start transaction {}: {}", ex.getClass().getName(), ex.getMessage());
            }
        } else {
            //if coming from the wrapper, then propagate the transactional context to all its open collections
            if(!this.dbOpenCollections.isEmpty()) {
                Iterator<Map.Entry<String, Abstract_MongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
                while(it.hasNext()) {
                    Abstract_MongoCollectionImpl collection = it.next().getValue();
                    collection.mongoTransactionalContext = txnContext;
                }
            }
        }

        this.mongoTransactionalContexct = txnContext;
        this.mongoMVCCClient.addToOpenTransactions(this, txnContext.getCtxId());
        
    }

    @Override
    public void commit() {
        try {
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }

    @Override
    public void rollback() {
        try {
            LTMClient.getInstance().getConnection().rollback();
        } catch (TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }

    @Override
    public void startTransaction() throws TransactionManagerException {
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            TxnCtx ctx = LTMClient.getInstance().getConnection().getTxnCtx();
            startTransaction(new CoherentMongoTransactionalContext(ctx));
        } catch(TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
}
