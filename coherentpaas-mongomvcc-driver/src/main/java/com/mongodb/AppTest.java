package com.mongodb;

import com.leanxcale.tests.minicluster.TMMiniCluster;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author idezol
 */
public class AppTest {
    private static final Logger Log = LoggerFactory.getLogger(AppTest.class);
    private static final String DATABASE_NAME = "DATABASE_NAME";
    private static final String COLLECTION_NAME = "COLLECTION_NAME";
    
    public static void main(String [] args) {
        Log.info("Starting...");
        AppTest test = new AppTest();
        try {
            test.test();
        } catch (TransactionManagerException | DataStoreException | IOException  ex) {
            Log.error("{}: {}", ex.getClass().getName(), ex.getMessage());
        }
    }
    
    public void test() throws UnknownHostException, TransactionManagerException, DataStoreException, IOException {
        Log.info("Start testing...");
        TMMiniCluster.startMiniCluster(true, 0);
        MongoClient mongoClient = MongoClientFactory.getInstance().createClient("127.0.0.1", 27017);
        
        //INSERT 20 DOCUMENTS
        MongoDatabase mongoDatabase = mongoClient.getDatabase(DATABASE_NAME);
        mongoDatabase.startTransaction();
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(COLLECTION_NAME);
        List<Document> documents = new ArrayList<>();
        for(int i=0; i<20; i++) {
            Document doc = new Document("user", (i+1))
                    .append("value", "value" + (i+1));
            documents.add(doc);
        }
        mongoCollection.insertMany(documents);
        mongoDatabase.commit();
        
        //FIND THE 20 DOCUMENTS
        mongoDatabase.startTransaction();
        MongoCursor<Document> cursor = mongoCollection.find().iterator();
        int count = 0;
        while(cursor.hasNext()) {
            Document doc = cursor.next();
            Log.info("{}: {}", ++count, doc.toJson());
        }
        
        
        //FIND 5 DOCUMENTS
        Document docQuery = new Document("user", new Document("$lte", 5));
        Log.info("query is: {}", docQuery.toJson());
         cursor = mongoCollection.find(docQuery).iterator();
        count = 0;
        while(cursor.hasNext()) {
            Document doc = cursor.next();
            Log.info("{}: {}", ++count, doc.toJson());
        }
        
        //CLOSE AND RETURN
        mongoDatabase.commit();
        mongoDatabase.close();
        mongoClient.close();
        TMMiniCluster.stopMiniCluster();
    }
    
}
