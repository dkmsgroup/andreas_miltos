package com.mongodb;

import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.WrapperTransactionContext;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author idezol 
 */
public class AbstractCoherentMongoDatabaseImpl extends Abstract_MongoDatabaseImpl {
    protected final TransactionalDSClient transasctionalDSClient;
    
    protected AbstractCoherentMongoDatabaseImpl(long dbID, DeprecatedMongoClient _depracedMongoClient, MongoClient mongoMVCCClient, String dataBaseName) {
        super(dbID, _depracedMongoClient, mongoMVCCClient, dataBaseName);
        this.transasctionalDSClient = mongoMVCCClient;
    }
    
    @Override
    public void startTransaction() {
        try {
            LTMClient.getInstance().getConnection().startTransaction();
            CoherentMongoTransactionalContext txnContext = new CoherentMongoTransactionalContext(
                    LTMClient.getInstance().getConnection().getTxnCtx());
            startTransaction(txnContext);
        } catch(TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        } 
    }
    
    
    @Override
    public void commit()  {        
        try {
            LTMClient.getInstance().getConnection().commit();
        } catch (TransactionManagerException | DataStoreException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }
    
    @Override
    public void rollback() {
        try {
            LTMClient.getInstance().getConnection().rollback();
        } catch (TransactionManagerException ex) {
            throw new MongoException(ex.getMessage(), ex);
        }
    }

    @Override
    public void startTransaction(MongoTransactionalContext txnContext) {
        if((this.mongoTransactionalContexct!=null)&&(this.mongoTransactionalContexct.getCtxId()==txnContext.getCtxId()))
                return; //already assigned
        
        if(!(txnContext instanceof WrapperTransactionContext)) {
             try {
                LTMClient.getInstance().getConnection().associate(this.transasctionalDSClient);
            } catch (CoherentPaaSException ex) {
                
            }
        } else {
            //if coming from the wrapper, then propagate the transactional context to all its open collections
            if(!this.dbOpenCollections.isEmpty()) {
                Iterator<Map.Entry<String, Abstract_MongoCollectionImpl>> it = this.dbOpenCollections.entrySet().iterator();
                while(it.hasNext()) {
                    Abstract_MongoCollectionImpl collection = it.next().getValue();
                    collection.mongoTransactionalContext = txnContext;
                }
            }
        }

        this.mongoTransactionalContexct = txnContext;
        this.mongoMVCCClient.addToOpenTransactions(this, txnContext.getCtxId());
    }
    
    
}
