package com.mongodb;

import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.mongodb.utils.CollectionRecoveryWriteset;
import eu.coherentpaas.mongodb.utils.DataBaseRecoveryWriteset;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.datastore.TransactionalDSClient;
import eu.coherentpaas.transactionmanager.exception.DataStoreException;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.SerializationUtils;

/**
 *
 * @author idezol
 */
public class MongoClient extends AbstractMongoClient implements TransactionalDSClient{
    
    /**
     * Creates an instance based on a (single) mongodb node (localhost, default port).
     *
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient() throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
                                  
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host server to connect to in format host[:port]
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node (default port).
     *
     * @param host    server to connect to in format host[:port]
     * @param options default query options
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host, final MongoClientOptions options) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(host, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host the database's host address
     * @param port the port on which the database is running
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    @Deprecated
    public MongoClient(final String host, final int port) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host, port);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(host);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node
     *
     * @param addr the database address
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final ServerAddress addr) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node and a list of credentials
     *
     * @param addr the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    public MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final ServerAddress addr, final MongoClientOptions options) {
        //this(addr, null, options);
        this(addr, new ArrayList<MongoCredential>(), options);
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param options default options
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    @SuppressWarnings("deprecation")
    public MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of mongod
     *              servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds);
                 serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds           Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list
     *                        of mongod servers in the same replica set or a list of mongos servers in the same sharded cluster. \
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, options);
                 this.serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param credentialsList
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, final MongoClientOptions options) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 this.serverAddress = mongoClient.getAddress();
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param uri
     * @see com.mongodb.ServerAddress
     */
    @Deprecated
    public MongoClient(final MongoClientURI uri) {       
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(uri);
                 this.serverAddress = mongoClient.getAddress(); 
                 LTMClient.register(this);
                 LTMClient.getInstance();
            } catch(TransactionManagerException ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    /**
     * Creates an instance based on a (single) mongodb node (localhost, default port).
     *
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(boolean fromFactory) throws UnknownHostException {        
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress();
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host server to connect to in format host[:port]
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node (default port).
     *
     * @param host    server to connect to in format host[:port]
     * @param options default query options
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, final MongoClientOptions options, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host);
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(host, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node.
     *
     * @param host the database's host address
     * @param port the port on which the database is running
     * @param fromFactory
     * @throws UnknownHostException This exception is no longer thrown, but leaving in throws clause so as not to break source
     *                              compatibility.  The exception will be removed from the declaration in the next major release of the
     *                              driver.
     */
    protected MongoClient(final String host, final int port, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.serverAddress = new ServerAddress(host, port);
                 this.options = new MongoClientOptions.Builder().build();
                 
                 mongoClient = new DeprecatedMongoClient(this.serverAddress);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node
     *
     * @param addr the database address
     * @param fromFactory
     * @throws java.net.UnknownHostException
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final ServerAddress addr, boolean fromFactory) throws UnknownHostException {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongodb node and a list of credentials
     *
     * @param addr the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param fromFactory
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    protected MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final ServerAddress addr, final MongoClientOptions options, boolean fromFactory) {
        this(addr, new ArrayList<MongoCredential>(), options, fromFactory);
    }

    /**
     * Creates a Mongo instance based on a (single) mongo node using a given ServerAddress and default options.
     *
     * @param addr    the database address
     * @param credentialsList the list of credentials used to authenticate all connections
     * @param options default options
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    @SuppressWarnings("deprecation")
    protected MongoClient(final ServerAddress addr, final List<MongoCredential> credentialsList, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 this.serverAddress = addr;
                 mongoClient = new DeprecatedMongoClient(addr, credentialsList, options);
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of mongod
     *              servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final List<ServerAddress> seeds, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds);
                 serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds           Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list
     *                        of mongod servers in the same replica set or a list of mongos servers in the same sharded cluster. \
     * @param credentialsList the list of credentials used to authenticate all connections
     * @see com.mongodb.ServerAddress
     * @since 2.11.0
     */
    protected MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * <p>Creates a Mongo based on a list of replica set members or a list of mongos. It will find all members (the master will be used by
     * default). If you pass in a single server in the list, the driver will still function as if it is a replica set. If you have a
     * standalone server, use the Mongo(ServerAddress) constructor.</p>
     *
     * <p>If this is a list of mongos servers, it will pick the closest (lowest ping time) one to send all requests to, and automatically
     * fail over to the next server if the closest is down.</p>
     *
     * @param seeds   Put as many servers as you can in the list and the system will figure out the rest.  This can either be a list of
     *                mongod servers in the same replica set or a list of mongos servers in the same sharded cluster.
     * @param options default options
     * @see com.mongodb.ServerAddress
     */
    protected MongoClient(final List<ServerAddress> seeds, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, options);
                 this.serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    protected MongoClient(final List<ServerAddress> seeds, final List<MongoCredential> credentialsList, final MongoClientOptions options, boolean fromFactory) {
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = options;
                 mongoClient = new DeprecatedMongoClient(seeds, credentialsList, options);
                 this.serverAddress = mongoClient.getAddress();
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    
    protected MongoClient(final MongoClientURI uri, boolean fromFactory) {       
        while(mongoClient==null) {
            lock.lock();
            try {
                 this.options = new MongoClientOptions.Builder().build();
                 mongoClient = new DeprecatedMongoClient(uri);
                 this.serverAddress = mongoClient.getAddress(); 
            } catch(Exception ex) {
                mongoClient = null;
                throw new MongoException(ex.getMessage(), ex);
            } finally {
                lock.unlock();
            }
        }
    }
    
    /**
     * Gets a database object from this MongoDBAbstract instance.
     *
     * @param dbname the name of the database to retrieve
     * @return a DBAbstract representing the specified database
     */
    public DBWrapper getDBWrapper(String dbname){
        long dbID = this.dbCounter.incrementAndGet();
        DeprecatedDB deprecatedDb = mongoClient.getDB(dbname);
        DBWrapper database = new DBWrapper(dbID, dbname, mongoClient, this, deprecatedDb);
        this.databases.put(dbID, database);
        return database;
    }
    
    /**
     * @param databaseName the name of the database to retrieve
     * @return a {@code MongoDatabase} representing the specified database
     */
    @Override
    public MongoDatabase getDatabase(final String databaseName) {
        long dbID = this.dbCounter.incrementAndGet();
        MongoDatabase deprecatedDb = AbstractMongoClient.mongoClient.getDatabase(databaseName);
        CpMongoDatabaseImpl database = new CpMongoDatabaseImpl(dbID, databaseName, mongoClient, this, deprecatedDb);
        Log.trace("Putting database " + databaseName + " with id: " + dbID);
        this.databases.put(dbID, database);
        Log.trace("Now we have database instances: " + this.databases.size());
        return database;
    }
    
    
    /**
     * Gets a database object from this MongoDBAbstract instance.
     *
     * @param dbname the name of the database to retrieve
     * @return a DBAbstract representing the specified database
     */
    @Override
    public DB getDB(final String dbname){
        long dbID = this.dbCounter.incrementAndGet();
        DeprecatedDB deprecatedDb = mongoClient.getDB(dbname);
        DB database = new DB(dbID, dbname, mongoClient, this, deprecatedDb);
        this.databases.put(dbID, database);
        return database;
    }

    @Override
    public long applyWS(TxnCtx txnctx) throws DataStoreException {
        CoherentMongoTransactionalContext ctx = new CoherentMongoTransactionalContext(txnctx);
        long res; 
        try {
            res = applyMongoWS(ctx);
        } catch(TransactionalMongoException ex) {
            //wrap exception and re-throw
            throw new DataStoreException(ex.getMessage());
        }
        return res;
    }

    @Override
    public void rollback(TxnCtx txnctx) {
        CoherentMongoTransactionalContext ctx = new CoherentMongoTransactionalContext(txnctx);
        rollbackMongo(ctx);
    }

    @Override
    public byte[] getWS(TxnCtx txnctx) throws DataStoreException {
        CoherentMongoTransactionalContext ctx = new CoherentMongoTransactionalContext(txnctx);
        byte [] result;
        try {
            result = getMongoWS(ctx);
        } catch(TransactionalMongoException ex) {
            //wrap exception and re-throw
            throw new DataStoreException(ex.getMessage());
        }
        return result;
    }

    @Override
    public DataStoreId getDataStoreID() {
        return DataStoreId.MONGODB;
    }

    @Override
    public void redoWS(long l, byte[] bytes) throws DataStoreException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        //TODO: THIS CODE SHOULD BE EXECUTED
        /*
        List<byte []> listWritesets = new ArrayList<>();
        listWritesets = ByteOperators.getBytesFromWriteSet(bytes);
        for(byte [] collectionWritesetBytes : listWritesets ) {
            ByteArrayInputStream byteIn = new ByteArrayInputStream(collectionWritesetBytes);
            ObjectInputStream in;
            try {
                in = new ObjectInputStream(byteIn);
                Map<String, Bson> collectionWriteset = (Map<String, Bson>) in.readObject();
                
                String databaseName = null;
                String collectionName = null;
                Map<ObjectId, Bson> writeset = new HashMap<>();
                Iterator<Entry<String, Bson>> it = collectionWriteset.entrySet().iterator();
                while(it.hasNext()) {
                    Entry<String, Bson> entry = it.next();
                    String [] values = entry.getKey().split(MongoMVCCFields.STRING_SEPARATOR);
                    if(databaseName==null) {
                        databaseName = values[0];
                        collectionName = values[1];
                    }
                    
                    ObjectId objId = new ObjectId(values[2]);
                    writeset.put(objId, entry.getValue());
                }
                
                
                MongoDatabase db = this.getDatabase(databaseName);
                CpMongoCollectionImpl<Document> coll = (CpMongoCollectionImpl<Document>) db.getCollection(collectionName);
                coll.recover(l, writeset);
                
            } catch (IOException | ClassNotFoundException ex) {
                throw new DataStoreException(ex.getMessage());
            }
        }
                    
        */
        
    }

    @Override
    public void unleash(TxnCtx txnctx) throws DataStoreException {
        CoherentMongoTransactionalContext ctx = new CoherentMongoTransactionalContext(txnctx);
        try {
            notifyMongo(ctx);
        } catch(TransactionalMongoException ex) {
            //wrap exception and re-throw
            throw new DataStoreException(ex.getMessage());
        }
    }

    @Override
    public void notify(TxnCtx txnctx) throws DataStoreException {
        if(Log.isDebugEnabled())
            Log.debug("MongoClient is invoked to notify for: " + txnctx.toString());
    }

    @Override
    public void garbageCollect(long l) {
        garbageCollectMongo(l);
    }
    

    @Override
    public long applyMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException {
        long tid = mongoTxnctx.getCtxId();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._applyWS(mongoTxnctx);
                this.openTransactionalDatabases.remove(tid);
            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }
        } catch (TransactionManagerException ex) {
            Log.error("Could not applyWS " + " for transaction tid: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage());
        } 
        
        //FIX WHAT TO RETURN
        return tid;
    }

    @Override
    public void rollbackMongo(MongoTransactionalContext mongoTxnctx) {
        Log.info("MongoClient is invoked to rollback for: " + mongoTxnctx.toString());
        Long tid = null;
        
        tid = mongoTxnctx.getCtxId();
        if(this.openTransactionalDatabases.containsKey(tid)) {
            this.openTransactionalDatabases.get(tid)._rollback(mongoTxnctx);
            this.openTransactionalDatabases.remove(tid);
        } else {
            Log.warn("There is no open transaction associated with tid " + tid);
        }
    }

    @Override
    public byte[] getMongoWS(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException {
        byte [] bytes = null;
        DataBaseRecoveryWriteset dataBaseRecoveryWriteset = null;
        
        long tid = mongoTxnctx.getCtxId();
        
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                Abstract_MongoDatabaseImpl database = this.openTransactionalDatabases.get(tid);
                List<CollectionRecoveryWriteset> list = database._getWS();
                if(!list.isEmpty())
                    dataBaseRecoveryWriteset = new DataBaseRecoveryWriteset(database.databaseName, list);

            } else {
                Log.warn("There is no open transaction associated with tid " + tid);
            }

            //if emmpty writeset, then return null for the HTM to continue
            if(dataBaseRecoveryWriteset==null) 
                return null;

            bytes = SerializationUtils.serialize(dataBaseRecoveryWriteset);
        
        } catch (Exception ex) {
            String message = "Could not get writeset for transaction: " + tid + " ... " + ex.getClass().getName() + ": " + ex.getMessage();
            Log.error(message);
            throw new TransactionalMongoException(message);
        }
        
        return bytes;
    }

    @Override
    public void notifyMongo(MongoTransactionalContext mongoTxnctx) throws TransactionalMongoException {
        if(Log.isDebugEnabled())
            Log.debug("Mongoclient is notified to unleash resources for: " + mongoTxnctx.toString());
        
        long tid = mongoTxnctx.getCtxId();
        try {
            if(this.openTransactionalDatabases.containsKey(tid)) {
                this.openTransactionalDatabases.get(tid)._unleash(mongoTxnctx);
                this.openTransactionalDatabases.remove(tid);
            }
        } catch(Exception ex) {
            Log.error("Exception when trying to unleash transactionid: {}. {}: {}", tid, ex.getClass().getName(), ex.getMessage());
            throw new TransactionalMongoException(ex.getMessage());
        }
    }

    @Override
    public void garbageCollectMongo(long l) {
        if(Log.isDebugEnabled())
            Log.debug("Garbage collection was invoked with timestamp: {}", l);
        
        //TODO: Miltos to wire it up with Mongo GC on the server side
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
