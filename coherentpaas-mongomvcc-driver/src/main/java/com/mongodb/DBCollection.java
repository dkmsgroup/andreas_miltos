package com.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.driver.CoherentMongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.exception.CoherentPaaSException;

/**
 *
 * @author idezol
 */
public class DBCollection extends DBCollectionAbstract {
    
    /**
     * constructor of this class
 sets the collection's name and the AbstractDBCollection object of MongoAbstractDB driver
     *
     * @param name
     * @param depracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @dochub MongoMVCCCollection
     */
    protected DBCollection(String name, DeprecatedDBCollection depracedCollection, MongoTransactionalContext mongoTransactionalContext, Abstract_MongoDatabaseImpl mongoMVCCDB) {
        super(name, depracedCollection, mongoTransactionalContext, mongoMVCCDB);
    }
    
    /**
     * constructor of this class
 sets the collection's name and the AbstractDBCollection object of MongoAbstractDB driver
     *
     * @param name
     * @param depracedCollection
     * @param mongoTransactionalContext
     * @param mongoMVCCDB
     * @param checkForDefaultIndex
     * @dochub MongoMVCCCollection
     */
    protected DBCollection(String name, DeprecatedDBCollection depracedCollection, MongoTransactionalContext mongoTransactionalContext, Abstract_MongoDatabaseImpl mongoMVCCDB, boolean checkForDefaultIndex) {
        super(name, depracedCollection, mongoTransactionalContext, mongoMVCCDB, checkForDefaultIndex);
    }

    @Override
    protected MongoCollection<DBObject> translateToMongoCollection() {
        MongoDatabase mongoDatabase = this.abstractDB._depracedMongoClient.getDatabase(this.abstractDB.databaseName);
        MongoCollection<DBObject> mongoCollectionDeprecated = mongoDatabase.getCollection(this.collectionName, DBObject.class);
        CoherentMongoCollectionImpl<DBObject> cpMongoCollection = new CoherentMongoCollectionImpl(collectionName, DBObject.class, mongoCollectionDeprecated, this.mongoTransactionalContext, null, writeset, pendingWritesetsToApply);
        
        return cpMongoCollection;
    }

    @Override
    protected void _getTransactionalContext() throws CoherentPaaSException {
        if(this.mongoTransactionalContext == null) {
            if(!LTMClient.getInstance().isTransactionOpen()) {
                //if not yet open, then set to autocommit
                LTMClient.getInstance().getConnection().startTransaction();
                this.isAutoCommit.set(true);
            }
             CoherentMongoTransactionalContext transactionalCtx = new CoherentMongoTransactionalContext(
                LTMClient.getInstance().getConnection().getTxnCtx());
             this.abstractDB.startTransaction(transactionalCtx);
             this.mongoTransactionalContext = transactionalCtx;
         }
    }

    @Override
    protected void clearTxnCtx() {
        this.mongoTransactionalContext = null;
        if(this.hasUpdates)
            this.hasUpdates=false;
        if(!this.writeset.isEmpty())
            this.writeset.clear();
        if(this.isAutoCommit.get())
            this.isAutoCommit.set(false);
    }

    @Override
    protected void _close() {
        this.mongoTransactionalContext = null;
        this.isClosed.set(true);
        this.writeset.clear(); 
    }
    
}
