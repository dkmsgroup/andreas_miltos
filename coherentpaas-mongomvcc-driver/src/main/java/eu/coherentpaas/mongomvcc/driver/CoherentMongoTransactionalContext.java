package eu.coherentpaas.mongomvcc.driver;

import eu.coherentpaas.mongomvcc.MongoTransactionalContext;
import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.LTMClient;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.concurrent.ThreadSafe;

/**
 *
 * @author idezol
 */
@ThreadSafe
public class CoherentMongoTransactionalContext implements MongoTransactionalContext {
    private final TxnCtx txnCtx;
    private final long tid;
    private final long startTmp;
    private final long cmtTmp;

    public CoherentMongoTransactionalContext(TxnCtx txnCtx) {
        this.txnCtx = txnCtx;
        this.tid = -1;
        this.startTmp = -1;
        this.cmtTmp = -1;
    }

    public CoherentMongoTransactionalContext(long tid, long startTmp, long cmtTmp) {
        this.tid = tid;
        this.startTmp = startTmp;
        this.cmtTmp = cmtTmp;
        this.txnCtx = null;
    }
    
    
    
    @Override
    public long getStartTimestamp() {
        if(this.txnCtx!=null)
            return this.txnCtx.getStartTimestamp();
        else 
            return this.startTmp;
    }

    @Override
    public long getCommitTimestamp() {
        if(this.txnCtx!=null)
            return this.txnCtx.getCommitTimestamp();
        else
            return this.cmtTmp;
    }

    @Override
    public long getCtxId() {
        if(this.txnCtx!=null)
            return this.txnCtx.getCtxId();
        else
            return this.tid;
    }


    @Override
    public String toString() {
        if(this.txnCtx!=null)
            return this.txnCtx.toString();
        else
            return "CoherentMongoTransactionalContext{" + "tid=" + tid + ", startTmp=" + startTmp + ", cmtTmp=" + cmtTmp + '}';
    }

    @Override
    public void checkForConflicts(byte[] key) throws TransactionalMongoException {
        try {
            LTMClient.getInstance().getConnection().hasConflict(key, DataStoreId.MONGODB);
        } catch (TransactionManagerException ex) {
            throw new TransactionalMongoException(ex.getMessage(), ex);
        }
    }
    
    
    
}
