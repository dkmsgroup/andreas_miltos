package eu.coherentpaas.mongomvcc.driver;

import eu.coherentpaas.mongomvcc.exceptions.TransactionalMongoException;
import eu.coherentpaas.transactionmanager.client.TxnCtx;
import eu.coherentpaas.transactionmanager.datastore.DataStoreId;
import eu.coherentpaas.transactionmanager.exception.TransactionManagerException;

/**
 *
 * @author idezol
 */
public class WrapperTransactionContext extends CoherentMongoTransactionalContext {
    private TxnCtx txnCtx;

    public WrapperTransactionContext(TxnCtx txnCtx) {
        super(
                txnCtx.getCtxId(), 
                txnCtx.getStartTimestamp(), 
                txnCtx.getCommitTimestamp());
        
        this.txnCtx = txnCtx;
    }

    public TxnCtx getTxnCtx() {
        return txnCtx;
    }

    public void setTxnCtx(TxnCtx txnCtx) {
        this.txnCtx = txnCtx;
    } 
    
    
    @Override
    public void checkForConflicts(byte [] key) throws TransactionalMongoException {
        try {
            this.txnCtx.hasConflict(key, DataStoreId.MONGODB);
        } catch (TransactionManagerException ex) {
            throw new TransactionalMongoException(ex.getMessage(), ex);
        }
    }
    
}
